# -*- coding: utf-8 -*-

import os
import sys
import time
import argparse
import logging
from logging import INFO
import tkinter
import lookup
from lookup import the_lookup

supported_encodings=['utf-8-sig', 'utf-16','utf-16-le','utf-16-be','utf-32','latin-1']

def create_logger(name,level):
        logger = logging.getLogger(name)
        logger.setLevel(level)
        # fh, ch, bh = get_logging_handlers()
        # logger.bh = bh
        # logger.ch = ch
        # logger.fh = fh
        # logger.addHandler(fh)
        # logger.addHandler(ch)
        # logger.newline = types.MethodType(log_newline, logger)
        return logger
logger = create_logger("EdtPIIDetector",INFO)

configs_mapping = {
    "MetaDataCsvFile": str(),
    "TextFilesFolder": str()
}


version = '0.0.1'

doc = """The utility uses natural language processing and machine learning to 
automatically identify PII candidates from the party information.
"""

trial_exp_date = '2019-08-01-12-00'

footnote = """NOTE: The evaluation period for this utility expires on 1 Feb 2019.
Please email support@discoveredt.com to provide feedback or to obtain 
support.
"""

interactive_msg = """

_________________________________________
             EDT AI Lab: Dev Prototypes        
_________________________________________
                     PII Detector (PIID)           
_________________________________________

Loading environment and modules, please wait...


"""

def enter_exit(status):
        input('\nPress Enter to Exit...')
        sys.exit(status)

def create_parser():
        """Return command-line parser"""
        parser = argparse.ArgumentParser(description=doc,
                                                                         epilog=footnote,
                                                                         prog='EdtPIIDetector')
        
        parser.add_argument('--version', action='version',
                                                version=version)
        parser.add_argument('-c', '--use-configs', action='store_const',
                                                const=True,
                                                help='set this flag to use configs file; '
                                                         'if used, other flags --data-directory'
                                                         ' will be ignored')
        parser.add_argument('-e', '--email-only', action='store_const',
                                                const=True,
                                                help='set this flag to skip filtering data-directory; '
                                                         'if used, the algorithem assums that data-directory'
                                                         ' contains only emails text files '
                                                         'and the metadata file will be ignored')
        parser.add_argument('-f', '--configs-file', action='store',
                                                default='configs/input.json', metavar='filepath',
                                                help='path to configuration file, only used if '
                                                         '--use-configs is set (default: '
                                                         'configs/input.json)')
        parser.add_argument('-d', '--data-directory', action='store', 
                                                metavar='filepath',
                                                help='path to directory that includes all emails text files'
                                                        'where filename is equal to Document ID in the metadata file'
                                                        ' mentioned in -m argument.'
                                                        'ignored if --use-configs is set')
        parser.add_argument('-m', '--metadata-file', action='store', 
                                                metavar='filepath',
                                                help='path to metadata csv file that Document ID and '
                                                'Document Type (or Document Kind). The file is used to '
                                                'select email documents only for processing from'
                                                ' directory mentioned in -d argument.'
                                                'ignored if --use-configs is set')

        return parser


def trial_check():
        logger.info(interactive_msg)
        time.sleep(3)
        _ = os.system('cls')
        if time.strftime("%Y-%m-%d-%H-%M") <= trial_exp_date:
             pass
        else:
             logger.info(
                 "Your EdtAliasDetector trial has expired! Please contact "
                 "info@discoveredt.com for more information.")
             enter_exit(1)


def parse_args(arguments):
        """Parse command-line options"""
        parser = create_parser()
        args = parser.parse_args(arguments)
        inputs_dict = None
        
        if args.use_configs:
            if args.configs_file:
                inputs_dict = get_configs_dict(args.configs_file, configs_mapping)
            else:
                parser.error(
                "--configs-file should be provided if --use-configs flag is set")
        
        elif args.data_directory and (args.metadata_file or args.email_only):
             inputs_dict = {'MetaDataCsvFile': args.metadata_file,
                                            'TextFilesFolder': args.data_directory}
        else:
             parser.error(
                "please use --cofigs-file option along with --use-configs "
                "or set both --data-directory and --metadata-file")
        if args.email_only:
            inputs_dict['EmailsOnly']= args.email_only
        return inputs_dict

def get_inputs_GUI():
        root = tkinter.Tk()
        root.withdraw()
        inputs_directory = tkinter.filedialog.askdirectory(initialdir="./",
                title='Please select a directory')
        metadata_file = tkinter.filedialog.askopenfilename(initialdir=inputs_directory,
                filetypes =(("csv File", "*.csv"),),
                title = "Please choose a csv file containing metadata Document ID and Document Type (or Document Kind).")
        if not inputs_directory or not metadata_file: 
                return None
        inputs_dict = {'MetaDataCsvFile': metadata_file,
                                            'TextFilesFolder': inputs_directory}
        return inputs_dict


def get_emails(dir_path,metadata_f, encoding ='utf-8' ):
    emails = []
    with open(metadata_f,encoding=encoding) as f:
        reader = csv.DictReader(f) 
        for row in reader:
            if row["Document Kind"] == "Email":
                try:
                    validate_file(os.path.join(dir_path,row["Document ID"]+'.txt'),'.txt')
                    emails.append(row["Document ID"]+'.txt')
                    #columns["Document Type"].append(row["Document Type"])
                except Exception as e:
                    logger.info("%s: %s" % (type(e).__name__, e))
    # allFiles = [f for f in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path, f))]
    # available_emails = list(set(emails) & set(allFiles))
    # missing = list(set(emails) - set(allFiles))
    # print("available_emails",len(available_emails))
    # print("missing_emails",len(missing))
    logger.info("Done verifing emails from metadata file:%s\n"
                " %s emails' file found valid at %s",metadata_f,len(emails),dir_path)
    return emails



def get_inputs(inputs_dict):
        
        dataDirectory = inputs_dict['TextFilesFolder']
        MetaDataCsvFile = inputs_dict['MetaDataCsvFile']
        
        validate_dir(dataDirectory)
        logger.debug("directory was validated: \n%s\n" % dataDirectory)

        if 'EmailsOnly' in inputs_dict.keys() and inputs_dict['EmailsOnly']:
            emails_files = [f for f in os.listdir(dataDirectory) if os.path.isfile(os.path.join(dataDirectory, f))]
        else:
                validate_file(MetaDataCsvFile,'.txt')
                
            
                logger.debug("csv file was validated at: \n%s\n" % MetaDataCsvFile)
                emails_files = apply_with_encoding(get_emails,supported_encodings,dataDirectory,MetaDataCsvFile)
        return emails_files,dataDirectory




def main():
        """Command-line entry"""
        trial_check()
        logger.info("Program Start") 
        inputs_dict = None
        if len(sys.argv) > 1:
            inputs_dict = parse_args(sys.argv[1:])
        else:
            inputs_dict = get_inputs_GUI()
        if inputs_dict is None:
                raise RuntimeError(
                        "Failed to get required user inputs needed to run the utility")
        #emails_files, inputs_directory = get_inputs(inputs_dict) #TODO validate input dic values
        dataDirectory = inputs_dict['TextFilesFolder']
        if os.path.isdir(dataDirectory):
            emails_files = [f for f in os.listdir(dataDirectory) if os.path.isfile(os.path.join(dataDirectory, f))]
        else:
            return -1
        #------------------------------------------------------
        #  Create Output directory
        #------------------------------------------------------
        
        outdir = os.path.join("outputs", time.strftime("%Y-%m-%d_%H-%M"))
        if not os.path.isdir(outdir):
                os.makedirs(outdir)
                os.makedirs(os.path.join(outdir, "csv's"))
                 
        logger.info(
                "Creating output files at %s..."
                % outdir
                )
        _ = the_lookup(emails_files,dataDirectory, outdir)
        return 0


if __name__ == '__main__':
     try:
            
            enter_exit(main())
     
     except Exception as e:
            
            if isinstance(e, RuntimeError): 
                logger.error("%s: %s" % (type(e).__name__, e))

                logger.exception(e)
            else:
                 logger.info("%s: %s" % (type(e).__name__, e))
                 logger.debug(e)
            
            enter_exit(1)
