As a heads up – understand there are requests from offshore for PFP equipment, and substantial scaffolding materials required asap for the attached scopes.
We shall ship these on the Far Scotsman and use the Scotsman as floating storage in the field.
Can you please provide delivery status asap

Apologies to all as I never attached – also the files are sitting In; Cygnus Shared/Commissioning/PFP Mar 13
Walter

Frank – I had a wash up with Stork today regarding the requirements for the outstanding PFP scope.
As you will note it is extensive and extremely concerning although scope advised need to be corroborated against job-carded including scaffolding etc.
Can you have someone onshore check  the man-hours and scope. Please note there is no contingency advised.
In addition can you please (Gordon) expedite 50  tonne of scaffolding to kick this off. Stork will call-off but can we follow-up. We should also look to have at least 9 scaffolders available to come to Cygnus on either Thursday of Friday. For every three scaffolders travelling one should complete a PA course.
We will look to get the first up permits required ready for their arrival. As you know the leg application is weather dependant – if held by weather we will utilise the Scaffolders elsewhere or re-locate to an internal PFP scope. Up-man of Fireproofers will follow as soon as we have the scaffolding arranged for them to work on.
We have looked at who we will either send onshore or stop travelling to Cygnus, to date we have identified circa 36 beds.  I will go through this with you tomorrow.
In addition to all of this I feel we need to look at having a Project Engineer or someone of that ilk out here from Stork – the Supervisors out here spend a lot of their time doing time-sheets etc. This work is so critical I feel it needs a Focal point to drive it.
Suggest we arrange a meeting tomorrow afternoon on all of this – 
Can you please forward to all AMEC/Stork people you feel are relevant as I am short of email addresses
Walter

