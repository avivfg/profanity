Hi Frank,
Firstly I would to apologies on behalf of my team if they have turned up un announced and this will certainly not be the case gong forward, I can only think that there has been a breakdown in communication as this request was from Paul Marley on Wednesday to have some disciplined guys down to help. 
The guys were on site to offer up some help to the offshore and onshore teams and I do believe were in communication with the offshore guys trying to locate what was need but I agree that this should have been better organised and maybe with a bit more notice going forward to the guys and hopefully with the arrival of Chris next week as you say this should lead to better clarity and continuity between the teams. 
I will get on to the teams  to see what we can do about the excess materials and get back to you and hopefully get that cleared up next week.
Apologies again for the confusion.

Mark,
I am the HUC Support base today. We seem to have a communication breakdown. Mick Oxley & an E&I AMEC guy have turned up this morning unannounced. Mick has no DWG’ s with him and the E&I guy has a couple of items on job cards he is looking for, 2 of which have already been issued and the others we do not have as they were issued on the HFG reconciliation report at the yard. Apparently these two gents got a late request from AMEC to come in here with limited information and no consultation with the Engie Base team. 
We work in a less formal way that the Fab yards did regarding visits, but if people don’t respect this we will introduce a similar Site Visit Request form requiring adequate advance notification and justification.
Please note that we already have an AMEC Hook up Material Controller (Mike Featherstone) based here as part of our team.  Can AMEC provide a fully identified list of urgent / specific items that they are looking for backed up by the support DWG’s and we can get Mike F to investigate?
For example, the latest FSA report shows circa 80 pipe supports / second fix guides etc as being short (this includes BW); also 18 of these have HAN No’s against them so we presume AMEC have been instructed to fabricate these. We have 14 pallets of various supports / guides etc here -- this list has
been provided to AMEC offshore. We need to know if they are required for Job Cards not listed on the report, or are surplus to requirements? If we are given the applicable job card information (if any) we can identify the relevant ones.
We have multiple pallets of instrument stands as well, some of which have been requested by the platform, shipped offshore and then returned to the Base again. We need to know which, if any, are required for the Project.
We have agreed that AMEC’s Chris Kirk will be here from Wed next. Hopefully his visit will lead to some clarity on the above and other similar issues. We will review at the end of next week to see what progress is being made and what value is being added to the operation here.
Meanwhile we would appreciate if you would communicate the ground rules to your guys. 

