Guys
I would request that these are job carded in order that we verify there are sufficient kits to cover the overall scopes – Marti can you sort out please
Please note that 2 of the 22 type EAJA013000 1 Core 300 joints kits were purchased for the splicing of the power cable within the Umbilical at APU and BWHP – hence I would like clarity of the current HU Jobcard scopes before we ship any of the kits out. Tony retain 2 off of these kits at HUB for the umbilical scopes please

Gordon,
Confirmed QTY as below available, I have downloaded the MSDS from 3m
EAJA013000
RESIN DATE EXPIRY  
EAJC031200
THESE ARE COLD FIX, NO RESIN

Gordon,
Have been advised by Amec (Offshore) that the Joint Kits have not been included on JC BOM’s therefore will not show on FSA.
Marti - Will you arrange for JC’s to be populated with the relevant details.
Offshore requirement / MTO (as advised by Amec / GDF HU Co.) is 16 nr and 2nr accordingly. Amec would like Offshore to check prior to mobilising the Jointer.
Tony – Please pick and ship on planned Sailing circa 17/09/2015 or 21/09/2015.

Tony
Afterthought
Can you get the lads to pull a couple of each kit and check the shelf life of the cold resin and also that we have relevant MSDS sheets for the resins
Thanks
GT

Gary
Please request AMEC to confirm the cable splicing job card refs, as these scopes don’t appear on the full Jobcard FSA.
Part/Tag Number
Store Item Short Desc
Qty
	Yard
Bay
Location
Comments
EAJA013000
THROUGH JOINT; CAST RESIN 1C 300; TAG: EAJA013000
E98
EAJC031200
THROUGH JOINT; CAST RESIN 3C 120; TAG: EAJC031200
B60A
E95
Tony has the original installation qtys that AMEC DD specified i.e. 22 and 4 each respectively and last year when it was agreed that AMEC HU would do the cable splicing we asked if they needed additional kits for cable splicing training purposes and we were advised ‘NO’

Tony,
Can you confirm if any stock available for the Cable Joint Kits;
EAJA103000
EAJC031200

