Must have missed this one, I’ll upload to PaMMS after lunch

Thanks Ross
Marti all the MI series (Miscellaneous Item)  comm codes were included in the overall data upload from Vincent Peck

Hi 
After speaking with Peter Berry the cable we are looking for is 
MI-AO-0110A-070
NESSCO GROUP
7/8" Non- Radiating Coaxial Feeder Cable (Ref. SNS-CFAW-086-Z110-B)
Marti 
If you could let me know when this is in PAMMS so I can get the jobcard sorted

I  don’t have any of the below, from memory this cable was lost on site by Heerema. 
We ordered the attached cable but it is not 7/8”, it is ½” and we have 
Part/Tag Number
Store Item Short Desc
MSDS 
REQD
Stock No
Drum
Number
Qty
RG62 COAXIAL CABLE
RG62A COAXIAL CABLE ML-C-17F
RG62 COAXIAL CABLE
RG62A COAXIAL CABLE ML-C-17F
RG62 COAXIAL CABLE
RG62A COAXIAL CABLE ML-C-17F

Hi Ross
Amec detailed design identified the 7/8 Coax Cables – tag specific and used MI (miscellaneous items) commodity codes instead of 1 common code to cover 7/8” Coax
The 5 codes are noted below along with the corresponding equipment specific tags
MI-AO-0110A-070
NESSCO GROUP
7/8" Non- Radiating Coaxial Feeder Cable (Ref. SNS-CFAW-086-Z110-B)
MI-AO-0110A-026
NESSCO GROUP
7/8" Non-Radiating Coaxial Feeder Cable (Ref. SNS-CFAA-086-Z110-BRA)
MI-AO-0110A-061
NESSCO GROUP
7/8" Non-Radiating Coaxial Feeder Cable (Ref. SNS-CFAP-086-Z110-LA)
MI-AO-0110A-053
NESSCO GROUP
7/8" Radiating Coaxial Feeder Cable (Ref. SNS-CFAP-086-Z110-GA)
MI-AO-0110A-075
NESSCO GROUP
7/8" Radiating Coaxial Feeder Cable (Ref. SNS-CFAW-086-Z110-C)

Hi Gordon
This is not the correct cable supplied by Nessco under that PO. I am looking for COAX 7/8” Non-Radiating Feeder Cable Ref Number RFS/LCF78-50JFNA. Would you please let me know the code for the cable and I will get Marti to create the item in PAMMS.

As below
JAZO99000
Ground and Weatherproofing Kits to suit item JCZ1010000
JCOZ010000
   Low loss coaxial cable for use in external areas. RG6/U, Armoured, Flame Retardant to IEC 60332, Low Smoke Zero Halogen. As Per Specification.
MI-AO-0110A-021
M10 Universal Installation Cleat (Ref. SNS-CFAA-086-Z110-BS)
MI-AO-0110A-028
Hanger for 7/8" Non-Radiating Cable (Ref. SNS-CFAA-086-Z110-BRA)

Morning Tony, Gordon
Ross is looking to job card the cable, etc. noted below.
Can you a) confirm availability & b) let me know if these materials already have codes, if not’ I’ll code them into PaMMS. 

Marti
I am looking for the codes for the cables and accessories that were purchased under PO52021 CF00-32-AO-52-001110.
250m of 7/8” Non Radiating Coax Feeder Cable 
7x High Speed Grounding Kit GKSPEED20-78P
230x Universal Installation Cleat MKD-M10-LN
230x Cable Hanger CLAMP-105

