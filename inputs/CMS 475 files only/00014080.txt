Ian – this was when we had 10 beds on the Ensco 80 and attempted to get various workscopes done on the AWHP despite the limited access etc.
Marti – did these valves get shipped offshore after being delivered to Bob – please advise

Ian,
They were never fitted due to late delivery (after sailaway).
They were shipped to Aberdeen at the request of AMEC HU for shipping off shore, see attached

Tony,
Please can you do a check for me so I can understand why check valves were removed or missing from AWHP.
Valve missing SPZ-087
Isometrics AWHP:
WT-003002-01
WT-004002-01
WT-008002-01
WT-009002-01
WT-010002-01
The TQ states the valves were late so they were never fitted thus the misalignment, however I am of opinion that these were fitted but removed for preservation.

Ian, 
Please find TQ attached, clear piping misalignments.
Need you to advise.
Regards

Hi,
Please find attached the following document, Issued For Response:
AMH-CYG-Q-00209 - 01 - EQ: AWHP MEXXDECK WT 1.1/2" Pipework Misalignment
Responses are required by 16/11/15.

Responses to this distribution must be made within 5 WORKING DAYS of the above email date
DOCUMENT EMAIL TRANSMITTAL
Originator -  Amec
Purpose of Document Issue :-
To addresses                 For Action 
CC'd addresses          For Information only
Documents issued
AMH-CYG-Q-00209 01       EQ: AWHP MEXXDECK WT 1.1/2" PIPEWORK MISALIGNMENT       16.7 mb
 <https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=6616b7f65f9bada98ae81a25d1ee87e1a31f455f> https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=6616b7f65f9bada98ae81a25d1ee87e1a31f455f 
AMH-CYG-Q-00210 01       EQ: VH-023318-01 AP-202P-PS-1007 ALIGNMENT                      7.3 mb
 <https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=2c7516964b4301bb4888f308011a838f5574b830> https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=2c7516964b4301bb4888f308011a838f5574b830 
AMH-CYG-Q-00213  01       EQ: SWLP BIOCIDE INJECTION POINT FLANGES                        7.8 mb
 <https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=6fcaad2971ff9d4ec02b850f7efdf23008a48f25> https://docfusionemea.amec.com/URLServer/?Server=edc05-fusapp1.3000&ID=6fcaad2971ff9d4ec02b850f7efdf23008a48f25 
NOTE
*The above link/s retrieve documents via the internet if you are not on the Amec network, as such it may take some time to download (e.g. 1min per Mb if your connection is slow)
*The URLs auto expire after a 60 day period

