Gents,
We will need 160ltr of the Castrol oil to refill the IPU hydraulic Start System.
This is to be delivered on UQ cellar deck. 
Contact name: Malika Jendoubi/Chris Kelman.

All,
I have 3 x 20 ltr drums of Hyspin AWH-M 32 Oil 20 LTR  at the HU base
Please confirm if they require to be shipped off shore, delivery point, contact name

Yes, as Glenn stated it is a 32 grade hydraulic oil that should be used, and lab tests carried out on a taken sample of oil from the system have also confirmed the oil in use is the Shell Z-TELLUS-T32.  This is from first fill as supplied by OEM.  Moving forward, as we have a contract with Castrol, for any change out of the oil it will be replaced with Castrol’s equivalent which is Castrol Hyspin AWH-M 32.  This will then align with what we have registered in the Castrol lube oil schedule database for the platform.
As mentioned I have raised a TQ to have the ‘lube oil schedule’ doc amended.

Mailka, 
Mark Kemp (cc’ed on this email) raised a TQ when he spotted a mistake in the engineering data provided by Amec 
Mark will confirm and expand on this but oil is the T32, 15W/40 is an engine oil

Hi Glenn / Keith,
Have you had a chance to look at the below? 
Tony,
Can you take a look at the below and see if we have either of these in the commissioning spares transferred from Methill / Heerema please.

Good Morning Gents,
There is some confusion around the type of oil required for the Hydraulic Start System on the Emergency Generator.
-       Turner Oil Schedule states 15W-40 grade oil to be used.
-       IPU Spare List states Z-TELLUS-T32.
(See both documents attached).
Could you please advise which type of oil we need to order as IPU vendors are due offshore in the first week of January?


John
Can you discuss with Craig, and raise a req for the various items as highlighted below to be supplied by Turners
In addition raise req for the Grade 32 Hydraulic Oil (presume Craig this is not Transaqua via Castrol??) when you have the manufacturer / spec confirmed by Craig.  – General question – have we got any concerns regarding cleanliness of hydraulic pipework ie do we require flushing and cleanliness analysis to NACE spec??

Hi Candice.  If you need him then + kit, you had best just request that he attends.  Please put a note in that the day rate requires to be re confirmed.
Day rate does not look right Contract 2015 rate is £979.44/12hr day offshore.  Uplift from 2014 to 15 was +6% - so if you apply the same that would be £1,038.

Gents can you check below first before I re-send the vendor call out, attached FYI 

Good afternoon Candice,
Having engaged IPU, the following is required for the commissioning of the hydraulic start system:
-          The engineer (Paul Smith earmarked) will require a minimum of 3 days to commission, providing that basic storage procedure has been adhered to.
-          Day rate of £1250+vat (2015 standard offshore rate), based on no more than a 12 hour working day
-          To reduce the risk of non-productive time on the platform, a spare bladder assembly is recommended (there are 3 in the unit). The engineer will assess whether they need replacing when there and order additional if required. If not required, the bladder assembly shall be kept as a spare. These are £1264 each +vat. Order is required today to meet the 6th Jan.
-          Filter – 1 off HP filter element - £180 +vat – require ordering today for 6th Jan
-          Filter – 1 off suction filter element - £359 +vat – require ordering today for 6th Jan
-          Hydraulic oil – 32 grade (not supplied by IPU)
-          Recharge pump seals and shock pulsation accumulator cost/lead time to be advised shortly
-          Whilst the pressure switches and gauges should be returned for calibration, for practical reasons the engineer will assess the items and proceed accordingly.
Due to the short time to order the items in preparation in January, we have ordered the bladders and filter elements already.
Please provide the formal request so we can keep the paperwork correct.

Please find attached a Call Out Request for requirement to mobilise Engineer offshore to the Seafox 2 on 05.01.16. Please can you confirm name and availability at your earliest convenience
Please be aware there is a mandatory requirement of 1 Day Pre-mob Training held at Dunston Hall Hotel, Norwich (overnight accommodation will be provided), joining instructions attached. 
Flights to the offshore installation Seafox 2 will depart from Bristow Heliport, Norwich.  
Personnel will also be required to be hold valid offshore certification as follows: 
·         BOSIET Certificate (Basic Offshore Safety Induction and Emergency Training)
·         UKOOA Medical
·         Minimum Industry Safety Training (MIST)
·         CA-EBS (Category A - Emergency Breathing System)
·         Shoulder Width Measurements
Please do not hesitate to contact me if you require any further details or information, I look forward to hearing from you.

