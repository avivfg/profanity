Evening,
Will do

Evening all,
Please be aware that the sample shroud for this OMR is coming back in on the Far Spica tonight.  Container AMF327
Ruth / Jonathan,
Please can you arrange for the Spica to be offloaded tomorrow with container AMF327 being a priority uplift to be taken round to Tony ASAP
We will still go ahead with a Tuesday load out with the intention to release the Far Spica back to Cygnus Tuesday evening
Tony / Setti,
Please can you pull this item from the container once received and arrange collection and delivery to CPS
Apologies we weren’t able to pack this separately but at the time of packing this was the only container we had available to us.
Mike K / Matt,
Can you confirm we can cancel OMR 0497 as mentioned in previous email

Good afternoon all,
Please find attached OMR CYG COMM 0552 - 1mm Stainless steel shroud for fire pump
Delivery Point: Middlesbrough
Offshore Destination: SF2
Recipient:  M. Kelly / S. Clarke
Workscope:  Replacement shroud for fire pump
CoH Critical
Matt / Mike K,
Can you confirm that this is to replace the OMR below and that this can be cancelled
Fire pump A&B Discharge pipe
A60 shrouding for Fire pump A and B discharge pipework
M. Plummer / E. Betts
COMM / 0497
ASAP
Purchase
DE

