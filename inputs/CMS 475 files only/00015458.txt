Correct Moray
That’s the Biffi part -  and no one yet came after it !

Thanks Setti. Is this the Biffi part that I ordered months ago from PV and we talked about last week?  Relates to a site req from Heerema?
If yes it's in no hurry. 

Tony,
Wayne is busy on the quay so I reply on his behalf.
We will deliver the shipment to HUC Base on Monday 18th Jan –  we didn’t get away early enough out of Italy on Thursday when we picked up 
the shipment from Petrovalve.
Will change the date on our track and trace accordingly. 
Setti

Wayne,
This has not been delivered to the HUB yesterday, is it in your warehouse ?

Moray / Dick is this box related to the ESDV Actuator mods? 
If so don’t ship offshore as the actuators are getting backloaded over the weekend

Tony,
Please see attached packing list covering the Terminal Enclosure supplied by Petrovalve – Req Ref: COMM-0125.
The Enclosure will be delivered to HUC Base today – allocated MMT number is 2076.

