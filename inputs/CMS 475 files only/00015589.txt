53554 = Material on 53562 (Not on FSA more details required) 53562 refers to 46639 shipped ABZ 044 & ABZ 042
53589 = Material on 53627 (Not on FSA more details required) 53627 refers to 46647 shipped ABZ 044 & ABZ 050
53635 = Material on 53643 (Not on FSA more details required) 53643 refers to 49751 shipped ABZ 042
The job cards in red have been updated to refer directly to the job cards in yellow, cutting out the spurious 3rd job card.  No idea why this was done this way.

Chris – see comments below

Marti,
As per list below, from the JMS report to FSA all material issued apart from the following,
J/C
54771 Aberdeen Checking (Marti) refers to 3905 shipped ABZ 043
53554 = Material on 53562 (Not on FSA more details required) 53562 refers to 46639 shipped ABZ 044 & ABZ 042
53589 = Material on 53627 (Not on FSA more details required) 53627 refers to 46647 shipped ABZ 044 & ABZ 050
53635 = Material on 53643 (Not on FSA more details required) 53643 refers to 49751 shipped ABZ 042
73148 = No J/C on FSA Make joints – no materials required
75604 = No J/C on FSA Install P-traps – no materials required (vendor supplied)

FYI

FYI – Chris can look at these – jobcards with no materials?


Gordon
Apologies I assumed this had been sent on to yourself.
Piping cards detailed below and E&I jobcards are all for pulling of yard installed cables.
0509-182-FS61-WPK-002
INSTALL PS AP601P-PS-001.
 AWAITING CALL OFF  50709
0509-182-FS46-WPK-002
Install AP601P-PS-0001
 AWAITING CALL OFF  50709
0509-182-FS51-WPK-001
INSTALL ITEMS F1 & R1 ON AP601P-PS-0001 ref. AP-051302-07
 AWAITING CALL OFF  50709
0509-182-FS14-WPK-001
Install AP201P-PS-0019
 Investigating CPS. Not on job card
0509-182-FS52-WPK-001
AMI-CYG-Q-00217  Install Clamped Pipe Shoe To WP-052201-07.
 CHANGED TO CLAMP FROM WELDING
0509-182-FS51-WPK-001
INSTALL ITEM R2 ON AW201P-PS-0062 ref. AP-051300-04
MATL. ON 49751 AS PER JOB CARD  50148 INSTRUCTIONS
0509-182-FS61-WPK-002
INSTALL PS TO SPOOL WT-061200-04-01 & 02.
 MATL ON JOB CARD 46639
0509-182-FS51-WPK-001
INSTALL ITEMS F6, R1 & R3 ON AW201P-PS-0060 ref. AP-051300-04
 MATL ON 46639 AS PER JOB CARD 46639 INSTRUCTIONS
0509-182-FS14-WPK-001
INSTALL AW201P-PS-0001.
 MATL ON 44806 AS PER JOB CARD 54763 INSTRUCTIONS. SHOES ONLY. FUSION MIX UP
0509-182-FS51-WPK-001
INSTALL ITEMS F8 & R1 ON AW201P-PS-0061 ref. AP-051300-04
 MATL ON 46647 AS PER JOB CARD 50121 INSTRUCTIONS
0509-182-FS46-WPK-002
Install AW201P-PS-0060
 MATL. ON 53562 AS PER JOB CARD 53554 INSTRUCTIONS. 46639
0509-182-FS46-WPK-002
Install AW201P-PS-0061
 MATL ON             AS PER JOB CARD 
0509-182-FS45-WPK-001
Install Clamped Pipe Shoes
 53589 INSTRUCTIONS. 46647
0509-182-FS46-WPK-001
INSTALL ITEMS F6, R1 & R3 ON AW201P-PS-0060 ref. GN-046364-04
 MATL ON 46639 AS PER 46787 INSTRUCTIONS
0509-182-FS44-WPK-001
Install PS AW201P-PS-0063 to Spool OD-044300-04-02 (Upper level)
 MATL ON 46663 AS PER 46841 INSTRUCTIONS
0509-182-FS44-WPK-001
Install PS AW201P-PS-0061 to Spool OD-044300-04-02
 MATL ON 46647 AS PER 46817 INSTRUCTIONS
0509-182-FS44-WPK-001
Install PS AW201P-PS-0060 to Spool OD-044300-04-01
 XXXXXX
0509-182-FS46-WPK-001
INSTALL ITEMS F8 & R1 ON AW201P-PS-0061 ref. GN-046364-04
 MATL ON 46647 AS PER 46795 INSTRUCTIONS
0509-182-FS46-WPK-002
Install AW201P-PS-0062
 MATL ON 53643 AS PER 53635 INSTRUCTIONS
0509-182-FS61-WPK-002
INSTALL PS TO WT-061040-07-01 & 02
MATL ON JOB CARDS 46639, 46647, 49751 AS PER 1104 INSTRUCTIONS 
0509-182-FS61-WPK-002
INSTALL HOOK UP PS TO SPOOLS WT-061030-06-01 & 02
 MATL ON JOB CARDS 46639, 46647, 46663 AS PER 1058 INSTRUCTIONS
0509-182-FS34-WPK-001
ERECT & INSTALL AW201P-PS-0060 ref. DH-034003-02
 MATL ON 46639 AS PER 1058 INSTRUCTIONS
0509-182-FS30-WPK-001
C/O Complete valve installation sub-system 035-PU-03
0509-000-0000-WPK-000
NSV - Install P-traps to HXN002,004 & 009 refrigeration pipework

Gents
Can the following Jobcards currently with ‘NO’ Materials required be verified asap, based on the overall report I issued on Monday 22nd Feb. All below are in current 6WLA period so we need clarification on actual material requirement to avoid any potential schedule delays.
To re-iterate from previous correspondence – as there are no materials currently identified against these Jobcard refs – the Jobcards do not appear on the Full or 6WLA FSA Reports 
JOBCARD NUMBER
WORKPACK NUMBER
JOBCARD DESCRIPTION
JOBCARD STATUS
PERCENT COMPLETE
PLANNED START
MATERIALS REQUIRED
0509-180-FS00-WPK-005
APU - Compression Module - Telecoms Cable Installation
A
No
0509-180-FS00-WPK-005
APU - Compression Module - Instrument F&G cable installation
A
No
0509-180-FS00-WPK-005
APU - Compression Module - Instrument F&G cable installation
A
No
0509-180-FS00-WPK-005
APU - Compression Module - Instrument F&G cable installation
A
No
0509-180-FS00-WPK-005
APU - Compression Module - Instrument F&G cable installation
A
No
0509-182-FS25-WPK-001
Re-instate AP-400P-PS-1009
A
No
0509-182-FS25-WPK-001
Re-instate AP-400P-PS-1010
A
No
0509-182-FS25-WPK-001
Re-instate AP-400P-PS-1013
A
No
0509-182-FS25-WPK-001
Re-instate AP-400P-PS-1004
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072202-SB01-PG-01 & 1"-WT-072202-SB01-PG-02
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072206-SB01-PG-02
A
No
0509-182-FS23-WPK-001
TP-30. Reinstatement Scope. Sub-System 023-PU-01.
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072208-SB01-PG-02
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072210-SB01-PG-02
A
No
0509-182-FS30-WPK-001
C/O Complete valve installation sub-system 035-PU-03
P
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072211-SB01-PG-02
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 1"-WT-072212-SB01-PG-02
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 4"-WT-072200-SB01-PG-01
A
No
0509-180-FS00-WPK-052
Install Personnel Protection Guards to Line 4"-WT-072205-SB01-PG-01
A
No
0509-180-FS00-WPK-014
AMH-CYG-Q-00361 Isolate, Disconnect cables, Install New Smoke Detector SDI-311-005-A
A
No
0509-182-FS51-WPK-001
INSTALL ITEM R2 ON AW201P-PS-0062 ref. AP-051300-04
A
No
0509-182-FS45-WPK-001
Reinstate P.S. for Chem.Inj.
A
No
0509-180-FS00-WPK-005
APU- Install Tray & Pull Cables to Dehydration Inlet Cooler Motor B
A
No
0509-182-FS25-WPK-001
Re-instate AP-400P-PS-1005
A
No
0509-180-FS00-WPK-005
APU- Install Tray & Pull Cables to Dehydration Inlet Cooler Motor A
A
No
0509-182-FS61-WPK-002
INSTALL PS TO SPOOL WT-061200-04-01 & 02.
A
No
0509-182-FS51-WPK-001
INSTALL ITEMS F6, R1 & R3 ON AW201P-PS-0060 ref. AP-051300-04
A
No
0509-182-FS14-WPK-001
INSTALL AW201P-PS-0001.
A
No
0509-182-FS51-WPK-001
INSTALL ITEMS F8 & R1 ON AW201P-PS-0061 ref. AP-051300-04
A
No
0509-182-FS46-WPK-002
Install AW201P-PS-0060
A
No
0509-182-FS46-WPK-002
Install AW201P-PS-0061
A
No
0509-182-FS45-WPK-001
Install Clamped Pipe Shoes
A
No
0509-180-FS00-WPK-009
AWHP-Erect lifting frame for to install meters C,D,H,I & J
A
No
0509-182-FS46-WPK-001
INSTALL ITEMS F6, R1 & R3 ON AW201P-PS-0060 ref. GN-046364-04
A
No
0509-182-FS61-WPK-002
INSTALL PS AP601P-PS-001.
A
No
0509-182-FS46-WPK-002
Install AP601P-PS-0001
A
No
0509-182-FS44-WPK-001
Install PS AW201P-PS-0063 to Spool OD-044300-04-02 (Upper level)
A
No
0509-182-FS44-WPK-001
Install PS AW201P-PS-0061 to Spool OD-044300-04-02
A
No
0509-182-FS20-WPK-002
Re-instate AP600P-PS-1716
A
No
0509-182-FS44-WPK-001
Install PS AW201P-PS-0060 to Spool OD-044300-04-01
A
No
0509-182-FS46-WPK-001
INSTALL ITEMS F8 & R1 ON AW201P-PS-0061 ref. GN-046364-04
A
No
0509-182-FS46-WPK-002
Install AW201P-PS-0062
A
No
0509-180-FS00-WPK-029
PU-Vendor to Assist and Supervise the reinstatement of Clean Agent Bottles CFAP-058-SIS002
A
No
0509-182-FS61-WPK-002
INSTALL PS TO WT-061040-07-01 & 02
A
No
0509-182-FS61-WPK-002
INSTALL HOOK UP PS TO SPOOLS WT-061030-06-01 & 02
A
No
0509-180-FS00-WPK-026
Reinstate ladder rack section on CFA2. Remove temporary rack risers
A
No
0509-182-FS20-WPK-002
Re-instate AP600P-PS-1020
A
No
0509-182-FS34-WPK-001
ERECT & INSTALL AW201P-PS-0060 ref. DH-034003-02
A
No
0509-180-FS80-WPK-001
Install vesda tubing from panel IFP014 to APU Upper Switch Room
A
No
0509-182-FS51-WPK-001
INSTALL ITEMS F1 & R1 ON AP601P-PS-0001 ref. AP-051302-07
A
No
0509-180-FS80-WPK-001
Install vesda tubing from panel IFP013 to APU LER / Battery Room
A
No
0509-182-FS14-WPK-001
Install AP201P-PS-0019
A
No
0509-187-FS00-WPK-001
Remove and reinstall gratings panels SNS-CFAW-045-PSV006-A reinstatement.
A
No
0509-187-FS00-WPK-001
AWHP Weather deck - Reinstate Handrail SE Corner
A
No
0509-180-FS00-WPK-026
HAN H1- 00078 Install cabling to  Oil in Water Probe SNS-CFAP-024-AT025
P
No
0509-180-FS00-WPK-014
Re-instatement of Wave Off Lights 099-ECP27-ELW001 & 002
A
No
0509-183-FS75-WPK-005
C/O: Install trace heating tapes -cct -075-ETHB23-L2
No
0509-187-FS00-WPK-001
CFAW - Reinstate steelwork at Slot 08 Valve Access Platform
A
No
0509-180-FS08-WPK-001
Uncoil & install trace heating tapes
A
No
0509-187-FS00-WPK-001
CFAW - Reinstate steelwork Slot 08 Mezzanine Deck
A
No
0509-187-FS00-WPK-001
CFAW - Reinstate steelwork at Slot 03 Valve Access Platform
A
No
0509-187-FS00-WPK-001
CFAW - Reinstate steelwork at Slot 03 Mezzanine Deck
A
No
0509-187-FS00-WPK-001
AWHP Jacket - Reinstate handrail on Jacket Spider deck
A
No

Morning Gordon,
I am about 75% of the way through this list and will have the action closed out today.
I just wanted to keep you in the loop and let you know it was being worked.

Derek
Can you please review.

Mark, Kenny – see below from Gordon.  Can you confirm if job cards are being reviewed?

Hi Marti
Are these NO Materials Jobcards being reviewed by the Jobcard Engineers??

Thanks Marti
I have review the NO Materials reports filtered out a lot of obvious No Materials Jobcards including scaffold and insulation, and as per attached there are 122 Jobcards which by their descriptions indicate materials are required.
Can these be review and checked for actual materials requirements
Tony – Can you check the Spool and Pipe Support tags / refs please

All, please see attached.
All job cards with no materials and <85% complete have been highlighted yellow.

All,
Tony has carried out a detailed review of the APU and AWHP Jobcard Full FSA Report’s based on the report issued 9th Feb, as per attached module specific reports. These reports were issued to Marti under separate and he acknowledged that these would be updated in the FSA data upon his return from holiday 22nd Feb. 
Apart from confirming stock availability against the various ‘BLANKS’ on the Full FSA, and providing manifest info for items previously shipped offshore,  there are still various queries previously raised where we have not received further clarification of incorrect material codes, items being fabricated / supplied via CPS, and re-instatements of existing items offshore etc.
Additionally we are being advised through Paul, of various materials shortfalls on the Daily Action Tracker where if fact there are no materials on the Jobcards – see below – if there are no materials identified these jobcard refs don’t get flagged up on the Full FSA report so therefore not on Tony’s radar.
Can I request that Marti provides on Monday an updated ‘Jobcards with No Materials’ report that can be reviewed by all onshore and offshore parties, and any jobcard workscope descriptions which infer material requirements are flagged up and MTO’s defined asap.
12-Feb-16
JC 51705 TEG INLET COOLER PS MATERIAL STATUS
MARTI SCOTT
15-Feb-16
OPEN
Reinstatement - No materials on JC - Mick to check at MBO
Drawing / ISO to be sent to MBO
12-Feb-16
JC 51713 TEG INLET COOLER PS MATERIAL STATUS
MARTI SCOTT
15-Feb-16
OPEN
Reinstatement - No materials on JC - Mick to check at MBO
Drawing / ISO to be sent to MBO
12-Feb-16
JC 51748 TEG INLET COOLER PS MATERIAL STATUS
MARTI SCOTT
15-Feb-16
OPEN
Reinstatement - No materials on JC - Mick to check at MBO
Drawing / ISO to be sent to MBO
12-Feb-16
JC 51667 TEG INLET COOLER PS MATERIAL STATUS
MARTI SCOTT
15-Feb-16
OPEN
Reinstatement - No materials on JC - Mick to check at MBO
Drawing / ISO to be sent to MBO

