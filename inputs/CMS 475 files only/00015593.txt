Tony,
Please can you confirm if you have in stores the pressure retaining covers for SPC-003 and SPC-005 see attached vendor datasheet page 2 which gives you a clue what they look like.
Can you also run a report identifying the SPC-003 and SPC-005 with the associated line numbers.

Ian,
Just for info neither the carbon steel or duplex fittings have the HD cover item 7 on the carbon steel model , hence I suggest we ask HFG / LV shipping if they have them otherwise they will need to be re ordered
Regarding TP-015 we will be ready this test Monday am can you please review the info and advise if the test can proceed? As discussed I feel with the red line marked up P & ID , and the TQ from HFG we are OK as the item is now SPC-005, and I can confirm looking at the fitting it certainly appears to be duplex. I will get some pictures this afternoon and send them on. 
Thanks for the help 

Mike,
I can confirm from the etching that this is the correct item 50-K03011 which signifies ASTM A350 LF2 Carbon Steel.
Please proceed with the test for TP-014.

Ian,
There is no nameplate on the fitting only some etched numbers, which are now rusty I have cleaned it as well as I could see attached pictures 
I cannot confirm the fitting is LTCS
Can we still proceed with the test

Mike,
Please find attached SPC-003 datasheet and vendor drawing, you will see I have made some corrections to the datasheet but this does not affect the testing just the probe insertion length which I will deal with next week. 
Please can you confirm by visual inspection the following:
·        Nameplate verifies it is SPC-003 as installed
·        Material is LTCS
I am happy with the data I have at hand and if the above confirms what has been installed is correct then please proceed with the testing of TP-014.
I will respond to your query on TP-15 separately.

Ian,
We are starting TP-014 , line number GD-023307-01 , P & ID CFAP-32-AP—62-00020. On the line is a SPC ( coupon sample ) the SPC on piping special data sheet says it’s good for 139 bar design pressure , our test pressure is 208.5barg . I think the SPC is OK based on 1.5 x 139 = 208.5 can you please urgently confirm today?
We have the same SPC 003 on TP-015 but this test pressure is 487.5barg that’s needs your confirmation that the SPC-003 is OK for this test pressure 

