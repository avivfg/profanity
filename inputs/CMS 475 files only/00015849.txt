INFORMATION SERVICES
cid:image003.jpg@01D159E8.09402AE0
Essential Desktop Maintenance – Monday 1st February 20:00
As part of the on-going process to ensure the security of the GaS UK IS environment, we are releasing the following updates to your desktop device: 
Windows 7 Security
Microsoft Silverlight
Microsoft Office 2010 Security
Microsoft Office 2013 Security
Please note: Internet Explorer 11 will not be installed.
The installation process will commence on Monday 1st February at 20:00.
On Tuesday 2nd February, you will receive the following prompt informing you your PC is ready to restart (If restart is still required): 
cid:image001.png@01D15464.CC9BCB40
If the timing is inconvenient, you are able to postpone the PC restart up to a combined period of 12 hours. When the 12 hour limit has been reached, the machine will restart automatically without notification, so please ensure all work is saved to avoid any loss of data. 
There will be no impact to the user experience but please contact the IS Service Desk if you experience any degradation in performance after the updates.
This process may be repeated each evening until your PC is fully patched.
All PC’s that are uncontactable during the maintenance window will install the updates at the next earliest opportunity, so to avoid any inconvenience, we strongly recommend leaving your PC on during Monday’s window. All PC’s that receive the updates after the scheduled window will experience degraded performance during the installation of the updates and before the PC is restarted.
If you have questions related to the updates or subsequently experience any issues please contact the IS Service Desk.

