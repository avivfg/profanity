Marti
Presume this is a CPS Fabrication Scope incl supply of the fixings – please advise asap
Job Card Number
Platform
Const/Comm.Wpk.
Job Card Description
Progress
Code
Code Description
Job Card Qty
Comments
SKYLINE
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_B1_APU_PLANT_ROOM GATE_POST
B1- M16 x 50 long Gr 8-8 bolts c/w nut locknut and washer as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_B2_APU_PLANT_ROOM GATE_POST
B2- M20 x 50 long Gr 8-8 galvi bolts c/w nut locknut and washer as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_B3_APU_PLANT_ROOM GATE_POST
B3- M10 x 40 long Gr 8-8 galvi bolts c/w nut locknut and 2 No washers as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_F1_APU_PLANT_ROOM GATE_POST
F1- 70 x 70 x 8 RSA x 1339 LG gate post - Grade S275 - c/w 125 x 75 x 8 RSA x 140 Length as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_F2_APU_PLANT_ROOM GATE_POST
F2- 70 x 70 x 8 RSA x 1346 LG gate post - Grade S275 - c/w 125 x 75 x 8 RSA x 140 Length as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_F3_APU_PLANT_ROOM GATE_POST
F3- 70 x 10 flat bar x 1105 long hinge bracket - Grade S275 - c/w 2 No 12 DIA rod x 155 lg hinge pins;á 2 No 21-3 x 3-2 CHS x 5 long collars; and 1 No 40 x 10 THK x 45 long spring coil fixing plate as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_F4_APU_PLANT_ROOM GATE_POST
F4- 42-4 x 4 CHS x 3065 long gate frame - c/w 1 No 108 x 6 THK x 122 long lock plate; 1 No 50 x 5 flat bar x 540 long latch bar; 1 No 15 mm dia pin c/w 2 No washers; 1 No 65 x 10 flat bar x 75 long slam plate 1 No 100 x 6 flat bar x 692 long; 1 No 55 x 10 flat bar x 1105 long ; 2 No 21-3 x 3-2 CHS x 30 long sockets; and 1 No 40 x 10 THK x 45 LG spring coil fixing plateá as per 0509-180-A001-SKH-040-001
TBC
APU
0509-187-FS00-WPK-004
AMH-CYG-Q-00254 - Install Safety Gate at Plant Room Access Platform
JC-75795_S1_APU_PLANT_ROOM GATE_POST
S1- Spring coil c/w 2 No coil fixing plates and 4 No M4 bolts as per 0509-180-A001-SKH-040-001
TBC

