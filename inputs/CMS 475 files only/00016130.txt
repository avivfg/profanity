Yes Tony. That's what we did at the fabrication sites.

Frank,
For invoices I will instruct LV to submit any invoices to the HUB base prior to submitting to GDF accounts for payment, we can review, sign and return to LV for submitting.
This will then give accounts confirmation by GDF signature that items/services have been supplied/ provided.  At the moment invoices are been paid on receipt from LV, there is no validation that the items/service has been provided.
This will provide an audit route from request to receipt

Tony,
Following a review of our administration procedures, and our meeting with LV yesterday, the following will apply. (Note: Most of this is also included in the minutes of meeting issued by Fred today).
Organisation
The HUC Support Base org chart is attached. As you are probably aware, each group on the Project has a Line Manager and a Functional Manager. In this case I am your Line Manager and Gordon is your Functional Manager. I am responsible for Base administration, processes and procedures; Gordon is responsible for operational aspects, logistics and materials.  Gordon remains the nominated Company Representative for the LV Contract. If this needs further clarification I can do so, but in practice there won’t be any significant change from the way we operate day-to-day right now.
Contract Instruction and Approval process
·         DoA – You will be formally delegated by Company Rep (GT) to instruct LV in writing to provide services, plant, ad-hoc consumables etc. 
·         All Instructions to LV for these supplies must be backed up by confirmation e-mails from GDF. 
−      Instructions must be numbered sequentially by GDF, and a register maintained. 
−      Please get Vicky to set up a system for numbering and a register. 
−      Urgent instructions can still be given by phone/verbally but MUST be followed up by an email as above. 
−      Instructions must be tracked and closed out on the register when complete. 
−      To be implemented from 13 Jan 2015. 
−        All invoices submitted by LV will have the relevant email instruction attached for traceability and audit purposes. (Ruth Boyle is going to confirm how they want instructions grouped per email so as to line up with their procurement/invoices -- e.g. would not have stationery and crane hire request on the same email!).
−        For audit purposes any physical orders supplied through LV and delivered direct to the GDF HUB must have proof of receipt and include these on the MBO MRR Register. (GT can clarify if necessary)
−        For audit purposes any hire of plant and equipment to support the ongoing operations will be signed off by you or a designated GDF Base team member. (as happens at present) 
−        Timesheets for all personnel in GDF HUB office (i.e. full-time LV personnel and GDF direct personnel (i.e. you and Vicky)) will be authorised by me. If I’m not on site, Vicky can scan and email the timesheets to me.
The above are normal and necessary admin procedures to keep us in line with Company policy and avoid non-conformances in the event of an audit.
If you have any queries please let me know.

