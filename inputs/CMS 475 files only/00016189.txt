John / Mike
Can you clarify please original instruction was 1 of the 4 hubs to go offshore and other 3 previously sent to CPS last year
Different set of requirements being issued by Darren as note below
Just want it clarified so we are not shipping things twice
If 2 of the hubs now to go offshore presume revised mto for additional clamps and seal rings?
If so can we have through Mike a definitive scope and mto instead of conflicting requests

John,
These are CPS ISO’s, the hubs were sent there 22/04/15.
Wayne,
Please action the below

Hi Tony,
Please could you ship below NAV as per Darren Alexanders request.
Ship to: Seafox 7
FAO: JC 8532

John,
There has been constructability changes and the 2 x 12” Hubs required for erection of line 12”-GW-025301-03 aren’t on a Job card.
Can you call off the 12” Hubs required for installation offshore on line number 12”-GW-025301-03.

FYI
Tony, 
In addition to the hub you are sending offshore, we will be needing 3 HUBs at CPS to resolve this. 
This completes the request yesterday for 4 hubs. Please place the order and advise what the lead time would be when you know.
I will issue a HAN to cover the extra CPS fabrication effort and offshore hours.
See page 4 and 5 of the attached (initial intent mark-up), and the offshore survey mark-up, which we will be using as the master (this supersedes initial intent).

John,
I will send the hub I have on NAV.
Ian W,
Do you want me to order another hub ? lead time been duplex will be long

Tony
I require 1 hub to weld onto the continuation offshore. The other end of the pipe is a hub and short length of pipe that needs to be welded to give us clearance of the steelwork, this should be fabricated along with the hook up drop out spool at CPS..

I think I have got to the bottom of this.
CPS are requesting Qty 4 hubs from me for ISO CFAP-38-AK-43-00089-001 (FABRICOM ISO NO)
This was original AMC ISO CFAP-32-AK-43-02429-001, 3 hubs were added to this, these were issued to CPS 23/04/15
John Brodie is indicating below he is only looking for Qty 1 hub which I believe is on the continuation ISO CFAP-32-AK-43-01248-001, This was allocated to HFG in the yard but does not look like it was actioned, hence the spare I have here.
Please quantify what pipe is required and I will send off shore with the hub, unless CPS are going to FAB

Tolu,
Please see attached the marked up drawing from April last year plus the site purchase order for the additional hubs from April last year, I am still looking for the FAN which issued the markups.

Ian,
HU spool. We should have had a FAN for this but it was missed. 
Can you check this and confirm this proposal is acceptable. If not, can you advise what is?
Please give me a call to discuss (if required).
For context see Technical Notes  
CFAP-02-KD-87-00001
CF00-02-EG-87-00002
Regards

Tolu,
           12” isometric as requested.
BR,
Dave

CPS Drawing

Davy
Can you find out the availability of the 12” Galperi (12M 114 ?) hub and short spool for this job.. Don’t believe there is a Job Card for this work yet?
Need these urgently

