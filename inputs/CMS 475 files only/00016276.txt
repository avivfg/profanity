Thanks Tony

Jim,
The bolts for JC 76546 have already been sent, see attached.
JC76651
No seal rings available, some on order
JC72176
Will Issue 1 on NAV
Ph01542
4" ( 4 ) Galperti G-Lok Standard Duty Clamp c/w Bolting AISI 4140 [Cr Mo] HDG - Bolts A320-L7 HDG Nuts 194-4 HDG

Morning Tony
Can you view Mick’s email request below. Please note that these are additional bolts, clamps & seal ring to the job cards that are mentioned below. Could you supply us from stock as these are critical to current work scopes.
Cheers
Jim

Jim
Please contact Tony Buckle at LV Shipping and ask for the following to be shipped NAV
We require 6 x bolts c/w nuts to suit 8” Galperti G-Lok Standard Duty Clamps AISI 4140 (crmo) if bolts are not available then send 2 x 8” Galperti G-Lok Standard Duty Clamps AISI 4140 (crmo) HDG-Bolts A320 L7 HDG Nuts 194-4 HDG JC-76546
Also 2 x size 72 Galperti G-Lok std seal rings AISI 4140 (crmo) PTFE Coated JC-76651
1 off  4” (4) G-Lok Clamp + Bolting a(s)4140 this is for TP-008 JC-72176

