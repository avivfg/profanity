Ed,
Reference this -- at the beginning Gordon Thom has agreed with Tony Buckle at HUB all materials for temporary generators would be marshalled and segregated at Middlesbrough.
Therefore if you contact Tony Buckle he will advise you of status.

Hi Keith,
Can you give me the details for the cable purchased for the 1250kva temp gens on AUQ.
We are planning to call this out this week.

Gents,
To summarise yesterday’s meeting the followings points/actions need to be closed out:
Materials:
·         Construction materials to complete the permanent LV network to be progressed (AMEC HU team).
·         Confirmation in writing of GDF procured materials delivery times (Keith Page)
o    Single core cables
o    Glands
o    Cleats
o    Lugs
·         BSL Swbd – planned to be ex-works 19/12/15 delivery offshore to confirmed (Keith Page)
·         Aggreko generators – FAT planned for 08/12/15, official notification to be issued (Moray Stewart)
o    FAT procedure to be collated by onshore team to ensure that all aspect of the GDF 3rd party equipment inspection procedure is completed to minimise offshore testing and inspection (Keith Page)
Changeover Philosophy:
·         Keith Page and onshore operations electrical team to confirm philosophy of changeover, after Keith has stated that current philosophy (see attached) is not feasible – Urgent clarification on this required (Keith Page)
·         APU generation change out timings to be confirmed (Offshore team)
·         Type of generators for APU application to be confirmed (Rig safe Vs Zone 2) – current commissioning strategy is to isolate temp power generation prior to hydrocarbons being present, no requirement for zone 2 equipment.
·         Scaffold laydown extension design to be confirmed with Stork (Offshore team)
·         Update on actions from issuing of philosophy document 
1.     Review document and provide feedback by 03rd December 2015 (All) – In progress
2.     Do we have more detailed temporary equipment numbers available (KP) – see above
3.     Do we want to include layout drawings from the 3d model (KP) – Ralph Walker is developing the layout to ensure structural integrity, layout drawings to be issued. KP to progress with RW.
4.     Identify expected loads during each step of the changeover/Ensure generation is capable of supplying. (Commissioning/Operations) – This is will part of the normal CoW offshore and will be managed accordingly CLOSED
5.     Identify any steps which may impact project scope, i.e lower lighting levels etc (Commissioning) – This is will part of the normal CoW offshore and will be managed accordingly CLOSED
6.     Communicate to projects when impacts are to be expected to execute this change (Commissioning) – Planning of this activity is ongoing and will determine the impact, though the greater impact is the challenge in the philosophy.
7.     Develop schedule for executing this change/alignment with project execution plan (Project) – as above, ongoing.
8.     It will be necessary to understand the impact of out of sync supplies on the UPS’ (step 3) (Commissioning) – aware of the alarms that will be present during change out CLOSED
9.     Ensure the single line diagrams are correct and displayed during each step of the change (Commissioning/REPs) – This is will part of the normal CoW offshore and will be managed accordingly CLOSED
10.  Outline of next steps for PU temporary generation philosophy (Commissioning/Project) – The commissioning philosophy for PU generation is to run HV temporaries until the point of first hydro-carbons allowing the commissioning of the main generation to be completed on diesel but minimise the run hours (due to the restricted 4months running time as stipulated by Siemens). The temporary generation will then be isolated prior to 1st hydrocarbons, at which point the main generation will be with start-up team for use on diesel. This will remove the need for zone rated temporary equipment. If start-up/operations identify the requirement to keep the temporary generation post 1st hydrocarbon then this needs to be identified as part of the post commissioning generation philosophy, this action is to reside with start-up/operations.
11.  Verify CoH loads profile (Operations/Commissioning/Project) – the base case is that all AUQ loads are available as per the project load schedule, as the majority of these loads will be required to achieve CoH. This is the philosophy we have adopted, if this is aligned with start-up/operations expectations this needs to be highlighted.
12.  Update philosophy document with comments and issue out (JD)

