John,
Update on COMM-0477
The PushBottons now ready for collection from Maclean - can you please let  me know if you want this be airfreighted or via far spica . 

Gary,
Please see attached update on Push Buttons just came in from MacLean.

Gordon,
The Push Buttons being manufactured in Germany and current delivery time is 18th Feb, however, I spoke to Ron on this and he knows
the urgency and going to speak to Germany and let me know if they can better the date and hopefully they will.
Further update tomorrow.

Are there alternative manufacturer push buttons available on a shorter lead time – what currently is the long lead on the units ordered?

Gary,
Just checked with Maclean, only item 2 on the PO / Crimp Lug ready for collection and Push Buttons are on long lead.
Will you let me know please if you want this to be airfreighted still or to be delivered to HUC Base.

Gary,
The Push Buttons becomes available for collection this afternoon, are we airfreighting this to you.
Shipment details as below:
19 x 17  x13 @1kgs
Can you let me know.

Ron,
Please find attached our confirmation GDF SUEZ P.O. No. 4000001212 for Push Buttons and Cable Lugs
Ref. Offshore Material Request CYG/COMM/0477
Maclean Electrical Quotation No. AQ307371 dated 21/01/2016 refers.

