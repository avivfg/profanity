
 PayPal logo <http://images.paypal.com/en_US/i/logo/paypal_logo.gif> 	9 Jan 2015 22:30:54 GMT
Transaction ID:  <https://www.paypal.com/uk/cgi-bin/webscr?cmd=_view-a-trans&id=8NF5231022920812N> 8NF5231022920812N	 
Dear Christopher Kelman,
You sent a payment of £140.84 GBP to Theatre Tickets Direct.	 
It may take a few moments for this transaction to appear in your account.
Merchant
Theatre Tickets Direct
info@theatreticketsdirect.co.uk
+44 2084297456	 Instructions to merchant
You haven't entered any instructions.	 
Delivery address – unconfirmed
Christopher Kelman
16 Chart House
Fleet Avenue
Hartlepool, Cleveland 
TS24 0WB
United Kingdom
	Dispatch details
The seller hasn’t provided any dispatch details yet.	 
Description	 Unit price	 Qty	 Amount	 
	£140.84 GBP	 1	 £140.84 GBP	 
Subtotal	 £140.84 GBP	 
Total	 £140.84 GBP	 
Payment	 £140.84 GBP	 
Charge will appear on your credit card statement as 'PAYPAL *THEATRE TIX'
Payment sent to info@theatreticketsdirect.co.uk	 	
Issues with this transaction?
You have 180 days from the date of the transaction to open a dispute in the Resolution Centre.
  <http://images.paypal.com/en_US/i/icon/icon_help_16x16.gif> Questions? Go to the Help Centre at www.paypal.com/uk/help
Please do not reply to this email. This mailbox is not monitored and you will not receive a response. For help, log in to your PayPal account and click Help on any PayPal page.
You can receive plain text emails instead of HTML emails. To change your Notification preferences, log in to your account, go to your Profile and click My account settings.
Copyright © 1999-2015 PayPal. All rights reserved.
PayPal (Europe) S.à r.l. et Cie, S.C.A.
Société en Commandite par Actions
Registered Office: 5th Floor 22-24 Boulevard Royal L-2449, Luxembourg
RCS Luxembourg B 118 349
PayPal Email ID PP120 - 593fe0ce8a0ee  <https://paypal.112.2o7.net/b/ss/paypalglobal/1/H.22--NS/465074020?c6=57J90544GY165971D&v0=PP120_0_&pe=lnk_o&pev1=email&pev2=D=v0&events=scOpen> 

