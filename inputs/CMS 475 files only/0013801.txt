Cheers for the info Chris, hopefully we can get Norsafe up here straight after the visit to Hartlepool and hopefully get completed within a couple of days although Norsafe are yet to confirm if dates are acceptable.

Hi Gary,
On AWHP we ended up taking 6 days! This however isn’t the norm. The delay came as we realised on Norsafe arrival that the davit static load test hadn’t been completed. We then came to power up the starter cabinet and found problems that have since been rectified on AUQ and BWHP. Lastly the load out grillage was directly below the davit arm which made installing the lifeboat and completing load tests that bit more difficult.
I expect the commissioning and installation work on BWHP to take 1 full day, we are in a better position on BWHP than we were on AWHP. In my call off I have asked for 3 days (18th-20th May), this was to include for inductions (1/2 day) and any problems that may (and always!) occur.

Morning Chris
Do you have any indication of duration of the commissioning activities based upon the previous AWHP performed tests ?, as per the attached call off we have the same scenario as Norsafe’s previous visit to Methil where upon if they complete on time for the BWHP and arrive here on Thursday 21st the activities may or may not run into the weekend period, obviously we are dependent on Norsafe’s agreement that the option to travel straight from Hartlepool to Methil is viable. I surmise that the fact we have 2 off lifeboat davit assemblies to test as opposed to 1 for the AWHP & BWHP will ultimately have a bearing on the matter as regards to expected duration.

Hi Hilary
As previously discussed please find draft copy of vendor call off for Norsafe’s required presence here at Methil for forthcoming AUQ commissioning activities, please note the dates I have included within the call off are based upon Norsafe completing the same requirements at Hartlepool for the BWHP hence ideally they would make provisions to remain in the UK and travel from Hartlepool to Methil although this would be ultimately be dependent on Norsafe agreement therefore I would be grateful if you could contact Milla (Norsafe) regarding this proposal, many thanks.

