Hi Chris,
Please find attached long term preservation procedure.
Hopefully this helps with the preservation of the crane.

Hi Luke,
Can you please take a look at Glenn Johnsons request below?
On the Alpha Wellhead Platform we have tied a temporary power supply into the slip ring from the drill rig. We do not have this option on Bravo Wellhead Platform.

Hi Chris,
BWHP will be installed June 2015 and the seafox 7 will not arrive until around 6 months later
During this time we will have no power and are concerned about preservations of all electrical equipment and need to work a solution
Can you get Liebherr to advise what we should be doing with regards to preservation, specifically preservations that needs power? e.g slew ring heaters and other condensation heaters etc..
If it is not an issue for the 6 months that’s fine juts let me know

