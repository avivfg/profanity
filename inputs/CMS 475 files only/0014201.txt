Hi Hilary
As previously discussed please find attached Liebherr call offs, details as follows :-
·         First call off – 1 off Liebherr site engineer (Luke Dobbings) to perform the flatness survey on the AUQ Pedestal Adaptor on the 17th Feb (1 day duration).
·         Second call off – 2 off Liebherr site engineers ( Joseph Collins & James Ingram) to assist with AUQ Crane installation 23rd Feb – 9th March (14 day duration.
As discussed I have sent Brian Hetherington an e-mail this morning regarding availability of Joseph & James on the 23rd Feb as this date will ultimately be dependent on the guys completing the APU installation activities (currently forecasted to be completed 23rd Feb), It may be the case we get them here for the required dates or worse case they get delayed on the APU.

