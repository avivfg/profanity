Jim 
See attached….the information regarding the bridge guides was communicated to Marcus when he was here on site and I understand he had some dialogue with the SHL people as to the as-built condition of the bridge to make sure everything tied up.

Hi John,
Can you please submit the information required for the UQ bridge landing as per below email as a matter of urgency?

Hello Jim,
We make reference to earlier communications on the above subject, and request GDFS UK to provide the below latest tomorrow, or other DC work (Navisworks), in order to conclude the offshore lifts feasible.
AWHP Topsides
East bridge installation guides dimensions and plan dimensions
East bridge bearing support position
APU Topsides
East Bridge installation guides and docking pins dimensions and plan dimensions
East bridge bearing support position
West Bridge installation guides and docking pins dimensions and plan dimensions
West bridge bearing support position
APU Compression module installation guides and stabbing pin dimensions and plan dimensions
GDFS advise to cut the C1 and C2 lift points as per design elevation, or that SHL has to take any CM out-of-levelness into account.
APU Compression Module
CM installation bumpers and installation buckets dimensions and plan dimensions
AQU Topsides
West bridge installation guide dimensions and plan dimensions
Bridges
Length between bridge bearing supports
Docking bucket dimensions + location of bucket relative to bridge bearing support points
Installation bumpers dimensions
Thank you for your attention / assistance.

