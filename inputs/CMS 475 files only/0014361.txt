Chris
Please find attached email wherein we previously asked this question of Liebherr.
Let me know if you need any more.
BRgds
Andy Daley.

Gents,
Sorry for going over old ground on a subject that had us all pulling our hair out but….
We came to the conclusion that the lift pressure of the accumulator bottle PSV’s was 100barg. Well this is what I think is the case – it is hard to tell from the poor quality scans that came attached to the TQ response.
The tag plate on the blocks suggests 210barg. The T in the below number represents “T... = pressure-set and lead-sealed by TÜV”. 
Broady are asking for extra info on the PSV’s – if I send the attached datasheet and tell him the model number I’m 110% sure he will ask the question…. Why are we saying set the PSV’s to 100barg when they were previously set at 210barg.
Did Liebherr come back with an explanation to the mismatch?

Good Morning Malika
Thanks for the mail. Do you have any literature of the two PSVs used in the Hydraulic Accumulators  please ?
We will do the tag plates and will despatch them along with the valves. I guess these are for those already on site.

Hi Janaka,
I hope you are well!
You will receive soon two new PSVs for recalibration.
The Set Pressure is 100barg. They are used on our hydraulic oil accumulators for our crane.
Could I please request that you seal the valves with an anti-tamper tag once calibrated?
Could we as well get 39 new tags as follow:
SNS-CFAA-62-PSV001 to 013
SNS-CFAP-62-PSV001 to 013
SNS-CFAW-62-PSV001 to 013
Thank you for your help.

