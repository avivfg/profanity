Good Morning Milla
I hope you are well?
I have been reviewing the documentation is preparation for the lifeboat testing offshore. I have noticed from the reports that UQ lifeboats dynamic load tests were not completed in the yard and these tests will need to completed offshore.
For this we will need to obtain 7.5 tonnes of test weight.
Can you please advise how is the best way to do this? Is it with sand bags inside? Or waterbags? Bearing in mind that waterbags would be difficult with the boat installed.
Also I have reviewed all the handover documentation and the FAT reports. Do tests such as lifeboat function testing and release mechanisms etc all require retesting?


Hi Chris!
I’m well, thanks! And you’re not forgotten J
Attached is the check-lists for AUQ davits. 
Have a nice evening!

With attachment*

Hi Ludmilla,
Trust you are well?
I’m currently offshore working on the Cygnus AUQ Lifeboats and as part of the permit we need to have the AUQ Commissioning Checklist (see BWHP example attached).
Embarrassingly we seem to have ‘misplaced’ this checklist and I was wondering if you had a copy that Torgje will have taken back with him?
The checklists I am looking for are for lifeboats SNS-CFAA-057-RSC002 and SNS-CFAA-057-RSC003.

