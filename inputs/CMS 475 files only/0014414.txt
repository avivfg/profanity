Hi Chris
Yes, it is a part of Type Approval for the delivered lifeboats and davits. The LSA certificates issued for them are not valid without stamp and signature of Class upon commissioning testing. This also implies that the warranty shall be withdrawn.

Good morning Ludmilla,
Is it defined in a specific performance standard that a Class representative must witness the offshore commissioning of lifeboat and davit?
The word “may” in the below sentence “Failing to do so may cause the withdrawal of warranty” is a bit dubious. If we did not have a Class representative present during  the commissioning can you confirm if the warranty would be maintained or withdrawn?

Hi Craig
Further to the below email, I’d like to inform that we always request our customers to forward to us the LSA certificates signed by Class upon the completion of commissioning testing. Failing to do so may cause the withdrawal of warranty right by Norsafe. 
Should you have any queries on this matter, please don’t hesitate to take a contact.

Good evening Craig
Please see my answers in your email below.
In addition, attached is the LSA certificates for lifeboats and davits, which Class representative shall sign. Will there be any Class to witness the commissioning?

Good Morning Milla
I hope you are well?
I have been reviewing the documentation is preparation for the lifeboat testing offshore. I have noticed from the reports that UQ lifeboats dynamic load tests were not completed in the yard and these tests will need to completed offshore.
For this we will need to obtain 7.5 tonnes of test weight.
Can you please advise how is the best way to do this? Is it with sand bags inside? Or waterbags? Bearing in mind that waterbags would be difficult with the boat installed. – Norsafe: waterbags are preferrable. However sand bags can also be used, remember 20 kg limit.
Also I have reviewed all the handover documentation and the FAT reports. Do tests such as lifeboat function testing and release mechanisms etc all require retesting? - Norsafe: Yes. These should be done during commissioning to be in compliance with SOLAS.


Hi Chris!
I’m well, thanks! And you’re not forgotten J
Attached is the check-lists for AUQ davits. 
Have a nice evening!

With attachment*

Hi Ludmilla,
Trust you are well?
I’m currently offshore working on the Cygnus AUQ Lifeboats and as part of the permit we need to have the AUQ Commissioning Checklist (see BWHP example attached).
Embarrassingly we seem to have ‘misplaced’ this checklist and I was wondering if you had a copy that Torgje will have taken back with him?
The checklists I am looking for are for lifeboats SNS-CFAA-057-RSC002 and SNS-CFAA-057-RSC003.

