You are a gent. Thank you.

Chris,
AWHP mech scope is down to start 29th April for 3 days, DC starting 2nd May for 6 days.
BWHP DC starting 18th June for 6 days.
Cheers
PJ

Hi Peter,
Could you please send me latest commissioning dates for AWHP and BWHP. Dave said he pushed them out last week.

Hi Chris,
Thanks for sharing your plans, even though they are preliminary. It is easier for our job-planners to move the jobs once they are established rather than to allocate personnel on a short-notice.
I look forward to your updates.

Hi Ludmilla,
Sorry for the delayed response. I am just catching up on emails.
Our preliminary dates are as follows:
AWHP – 9th April
BWHP – 17th June
I empathise preliminary as the AWHP date is very unrealistic. Unfortunately I can only work to the planned dates, but my opinion is that we are more likely to commission BWHP before AWHP.
I will update you early next month with the latest planned dates.

Dear Cygnus
Please advise your (preliminary) schedule for the remaining commissioning at AWHP and BWHP. As I informed before, our service-engineers have a tight travel schedule, especially in the summer period. Therefore your early advice will help us to allocate the right personnel and better plan the call-outs.
Look forward to hearing from you!

