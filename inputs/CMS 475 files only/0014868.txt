Hi Glenn,
Before we talk on the phone tomorrow here is some additional info:
The pump flanges have been stamped with DN40 PN6/10 and have raised faces. I have pulled out the dimensions for both a PN6 and PN10 DIN flange which don’t correspond with what we have on the pump. What we have is a hybrid! A basic internet search in absence of access to the DIN standards did not reveal any commonly used PN6/10 flange or dimensions of such a flange.
Both pump flanges mate with DIN 2527 DN40 PN16 flat face blind flanges. The flange on the suction end has been drilled and tapped to accommodate a 1.5” BSP male adaptor. The flange on the discharge end (the heater inlet flange) has  had a through hole drilled in it before being welded to an elbow. I have tabled the key dimensions for each flange type below:
Dimensions for DN 40 Flanges
DIN2631 PN6
DIN2632 PN10
PN6/10 (on skid)
DIN2527 PN16
PCD
100mm
110mm
Both!
110mm
Flange Thickness
11mm
13mm
12mm
16mm
Raised Face Height
3mm
3mm
3mm
N/A (Flat Face)
Raised Face Length
80mm
88mm
84mm
N/A (Flat Face)
Outer Diameter
130mm
150mm
150mm
150mm
Bolt Hole Diameter
14mm
18mm
Both!
18mm
So effectively IPS (the supplier of the coolant system to Turners) provided a pump that can be connected to either a PN6 or PN10 flange.
From the 17 concessions submitted by Turner none grant permission to use DIN standard flanges over the project standard ASME B16.5. Is the use of DIN flanges acceptable? If not we’ve pretty much got to scrap the cooling system which seems a bit extreme.
Parker came back with a response to the adaptor query – what they have in stock is the exact same as what we already have. You were right about the 1.5” dowty seals, very hard to find. I’ll leave this with the buyers to try and source.
I’ve still to confirm CNAF spec on board as the container has been stacked with no access at the moment. Scaffold access is currently being erected.
Ed Betts will issue the attached TQ listing all the issues with the proposed solutions:
1)     Engineering to go back to Turners and request an identical replacement pump be supplied with the only change being the use of standard PN16 flanges (or PN10 if PN16 is not available).
2)     Engineering to source either 2 x 1.5” female to male BSP short thread adaptors or 2 x 1.5” 4mm thk dowty seal washers.
3)     CNAF to be used for gaskets when refitting  new pump and non-return valve.

Chris, 
will discuss with you when back in the office on Monday but I am not in favour of leaving the pump and we should pursue if this is  warranty issue or we need to replace / repair the pump. If the PCD is the same then then the holes have probably been slotted to except an ANSI 16.5 gasket, I have see it before where slots are ground into the outer ring of the gasket which again is not ideal but much better than slotting the bolt holes on the flange. 
The fitting solution seems fine although If BSP then it will be a dowty seal washer so getting a thicker one may be a  issue 
Again happy to put a NAF gasket on a full face flange in cooking water service, please send over Spec / thickness of material you have in stores

Hi Glenn,
I’ve been asked to run with resolving the pump issues around the emergency generator cooling system. Can you look at the below proposals and let me know if you agree or disagree?
The PCD’s across all flanges surround the pump/heater are 110mm. Therefore I can’t understand why Turners / IPS (pump manufacturer) would have opened out the bolt holes on the pump flanges. Having said that what they have done is a proper job and has been finished well. I don’t believe this would compromise the integrity of the flange/pump. My proposed solution for the pump is therefore: NO ACTION REQUIRED.
The 1½” BSP adaptors on the pump suction flange and downstream NRV flange protrude through the flange 3mm. The male length of the adaptor is 19.5mm. I have emailed Parker to enquire the length of the male thread on their 1½” adaptors. If they are between 12-16mm my proposed solution would be to procure replacements and change them out with those currently used in the Em Gen Package. Failing that we could install a 3mm thick washer between adaptor nut and flange.
The NRV mating surfaces are flat, as are the flanges that connect to the pump. I propose we install full face NAF gaskets between each joint. We have the gasket material in the offshore stores along with cutting tools.

Chris,
Can you keep updated with the progress on this.
It is worth speaking to Matt Plummer before he goes off. We had a brief meeting about it yesterday. The issues are going to be passed back to Bob Craigie to close out.

Craig, 
Can you do me a favour and put the pics into a file in the path as below
This is the only path I have access to
<image002.png>

 <file:///\\gdfbrit.local\dfs$\shared\Cygnus%20Shared\DCC%20Distribution\Glenn-Johnson\Emergency%20Generator%20UQ> W:\Cygnus Shared\DCC Distribution\Glenn-Johnson\Emergency Generator UQ
Glenn,All
We have punched out the Mechanical aspects of the Emergency Generator package (Po no 4100001151)
Our mechanical technicians reported suspected undersized gaskets  located  in the suction and delivery flanges of P007 and also in a sandwiched NRV (HV1028)
When we have further investigated we have  uncovered that 1.5”  150 Gaskets were  installed in DN Flanges. See attached photos for illustration. 
There have been PNS tags made on these joints in question which suggests to me that these have been remade during construction at Methyl.
Looking at the flange faces lt looks like there is imprint of a NAF type gasket that may have been previously installed prior to these incorrect ones being installed. Can you confirm that we can go back to a NAF Gasket and confirm the spec?
Equally as disturbing as the gasket issue in the modified flange on Pump P007. See picture.
There is also a proud fitting on one of the NRV Flanges. Poor to say the least.
Can you please advise on how we should address these issues?

