Hi Mike,
In my handover from Malika she mentioned we would now be keeping the isolation valves that were fitted to the impulse lines and fit them with interlocks.
Firstly – can you please revise your response to that of Rev 4 which says to remove them.
The details from both the Parker isolation vales are identical and read as follows:
HPBYKMIZAHL
6Mo – PTFE
PTFE SEAT
204ºC (400F) MAX.
MODEL A1: 12mm TUBE
I have attached photographs for visual aid.

Hi Mike,
I should have made a comment on the drawing for clarity.
The instrument 052-PZT057 is not fed from the 12mm impulse line. It is being fed from HV1117. The impulse line is fed from HV1116. To summarise; the pipe fitters at BiFab fitted the impulse line and pressure transmitter to the wrong valves!
There is no explanation as to why the extra isolation valves are in place. I will raise this on a TQ directed to you. Your response will generate a HAN and AMEC HU will need to re-route the pipework. In the same TQ I will suggest swapping the PZT and impulse line back to their originally planned positions.

Chris,
I am reviewing the batch of Commissioning Master P&IDs namely CFAA-32-AP-00016-002 and I have included the mark-up that you completed on your walk through together with the associated Isometrics for reference.
I have noted the following important issues that are not acceptable from a safety perspective which are at variance with the intended design:
1)      the relocation of the high pressure trip transmitter 052-PZT057 from the process line 3”-WP-052-233-SB01-NI to a new location taken from the 12 mm instrument tubing impulse line feeding 052-PSV059A/B
2)      Installation of two new manual valves on the impulse lines supplying 052-PSV059A/B.
The problem that this gives is that 
a)      Maintaining 052-PZT057 requires you to isolate the upstream SBB valve thereby isolating BOTH impulse lines to 052-PSV059A/B, thereby losing your overpressure protection from both PSVs.
b)      The two new manual valves located on each impulse line risks isolating both PSVs without being locked open
Therefore I would advise that the HU should revert back to the original design.  I note that you have not deleted the SBB, 052-HV118 so it should be a very easy fix to relocate the PZT back to the intended original position (but the two additional manual valves need to both need to be removed).

