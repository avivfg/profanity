Good morning Tag Coordinator,
Please see below a request for 5 additional tag numbers for the required modifications to the RO package. Tag Request Form attached. Please advise as soon as possible to avoid any delay with Parker issuing the revised P&ID’s.
Glenn,
The additional PSV’s will be added to the live Master PSV Register held by Commissioning. How will this be captured in the Operations PSV Register?
Mike,
Are Parker going to also update the associated instrument and valve indexes?

Hello Malika, 
We are finalizing the P&ID updates, and need to allocate new tag numbers for the following items; 
*	Pressure Control Valve, Qty 1  (shown as PCVXXX in attached) 
*	Pressure Safety Valve, Qty 2    (shown as PSVXXX in attached) 
*	Pressure Gauge, Qty 2               (shown as PGXXX in attached) 
Please let me know if you need more information to allocate these numbers. 

