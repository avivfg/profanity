Hi Alan,
Could you please raise permits for the following activities:
(All of equal priority so it doesn’t matter which you do first)
On AWHP
·         Draining and top up of Liebherr Motor Oil 10W-40 from the crane engine crank case.
·         Draining and top up of Liebherr Gear Hypoid 90 EP from the crane winch gearbox.
·         Top up of Liebherr Anti-corrosion Antifreeze for the engine coolant.
On APU
·         Draining and top up of Liebherr Motor Oil 10W-40 from the crane engine crank case.
·         Draining and top up of Liebherr Gear Hypoid 90 EP from the crane winch gearbox.
·         Top up of Liebherr Anti-corrosion Antifreeze for the engine coolant.
·         Top up of Liebherr Hydraulic 37 for the hydraulic system.
I have attached the COSHH sheets as well as the TBRA used to complete the similar exercise on the AUQ crane.
The oil and antifreeze required were delivered a while back as part of requisitions CYG-COMM-00337 & CYG/COMM/0280. If you don’t know where it is then Cliff will.
We may not have any Liebherr Hydraulic 37 left over offshore. If not then let me know and I call another drum out from the beach.

