Chris
At the moment we have a commissioning query CYG-CYG-K-00241 (See attached) outstanding on the Crane F&G Logic so we are not in a position to finalise this work until this query has been answered and we know the resolution to the issue identified on CYG-CYG-K-00241.
The answer to query CYG-CYG-K-00241 may require the mobilisation of a programming resource for the crane F&G System if the 24VDC Trip has to be implemented in the Fire and Gas Controller on the Crane
So at the moment the completion of the Crane F&G Testing is on hold pending the answer to commissioning query CYG-CYG-K-00241.
I am expediting the answer to TQ CYG-CYG-K-00241 so you do not need to do anything on this and I will advise you when we are in a position to complete the commissioning of the Crane Fire and Gas System and I will mobilise all resources needed to do this

Gents,
We are hoping to have Joe Collins (Liebherr Engineer who installed/tested the F&G) on site Friday 22nd to Sunday 24th April. We need the BWHP crane moved in the morning to allow the installation of the spreader bar and returned to its boom rest in the evening.
The installation is weather dependent so could be on any 3 of these days. He will not be busy for all 3 days, in fact will have very little to do. Will you have techs available over this period to iron out the issues with the F&G?

Steve
Thank you for that
We will let you know when we have finished the Fire and Gas testing we need to do

Graham
The ACM crane is still on temporary supply and will remain until further notice. Power will be available for F&G pre-comm workscope.

Steve/Craig
To during the testing of the APU and BWHP Crane do to date we have been unable to complete all of the Instrument/Fire and Gas  Pre-Commissioning scope.
We anticipate that we will be able to complete this in the next 2-3 weeks.
Therefore please can you ensure that power remains available for powering the APU and BWHP Crane Fire and Gas Panel until I inform you that we have completed this scope

