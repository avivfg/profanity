Morning Gary,
See below email from Dennis Grice relating to the fire pumps. I have let him know any AUQ related should be sent or copied to yourself.
On BWHP I am going to wait for the drawings to go Code 1 then hand them over to Heerema’s electrical sub-contractor to re-wire in the instruments. If SPP haven’t been at Methil yet perhaps this is something that they can add to their existing carry over work scope rather than have you request BiFab do the work.
Dennis also mentioned that there are some free issued sounders and beacons to be installed inside of the enclosures with which project cables are to be terminated into. I am awaiting further details on this which Dennis will copy you on.

Many thanks Steve,
By copy, Peter, before SPP resubmit officially CFAA-32-AM00160A-E02-00001-003 R6, please review and advise if the wiring is now reflecting your concerns are resolved.
By copy, Steve Hugill, Chris Kelman, Terry Newby - I believe that the wiring needs to be reconfigured on all three FW pumps in accordance with revised wiring diagram. History contained within. Other diagrams to be updated are: CFAA-32-AM00160A-E02-00002-003 R6 & CFBW... -E02-00001-003 R6. 

Dennis
So sorry I haven’t responded sooner. I have spent a considerable time away from the office in the last 2 months. I have amended the first of the package interconnections to address the required connections for the Gas & flame detectors. A copy is attached of the relevant sheet.
I shall proceed now with the amendments to the interconnection diagrams for the other two packages. Once completed I shall issue into our system for formal submission.

Good morning 
May we have a response please.

Morning Steve,
May we have a response please.

Hi Steve,
Many thanks for your quick reply. 
Sorry for the misunderstanding. We were not aware that we needed to reply and took the “The Interconnection diagram will require updating to reflect the actual connections.” as an statement that would be carried out as the wiring diagrams have to reflect the actual instruments installed.
We await your further response when your back in the office.

Dennis
I am out of the office until Friday.
The detector are a multi-function device which enables the monitoring to be provided in many methods. SPP requested how you wished to monitor these and I was not provided with a definitive answer. I detailed on the Interconnection diagram a commonly used method. There was no comments made by your instrumentation or F&G departments, therefore I could only assume this was acceptable to you. The detectors have been connected as marked on the Interconnection diagrams.
I shall review the method detailed in your e-mail and advise the implications.

Good morning Steve, 
I hope you are well. As Katie is on holiday I have sent this query direct to you and cc’d Katie, Richard and Laura. I hope that this is ok.
Re SPP response to AMC-SPP-E-00137, dated 30/01/14
Flame detectors
Can you please confirm that the below mods to the wiring diagrams (advised in your below email) have been carried out. Our instrument group state:
From package interconnects E02 sht3 they appear to be wired as Detronics flame detectors however SPP inst data sheets show them  using Spectrex 40/40 flame detectors.
From the control system point of view.
Spectrex 40/40 flame detectors are wired:-
Terminal 1 24v
Terminal 2 0v 
Terminal 9 sig
Gas Detectors
Wiring diagrams mods also need to be carried out for these also. Our instrument group state: 
From Drager literature PIR7000/334 gas detectors should be wired:-
Terminal 1 24v
Terminal 2 0v 
Terminal 4 sig

Dennis
It is confirmed that the device is Sharpeye model 40/40I
I have taken a couple of photographs of the terminations of the flame detector in question. The Interconnection diagram will require updating to reflect the actual connections.

Good morning Alan, Steve,
Please see the below e-mail from our instrument group and provide your response by return.
Many thanks in advance for a quick reply.
By copy, Tony, From the correspondence submitted by SPP on this subject, the 40/401 is being provided by SPP. Please see attached e-mail.

Dennis
We seem to have a conflict on the Firewater Pump wiring of the flame detectors.  The vendor datasheets and instrument index suggest that the flame detectors are Spectrex 40/40 devices however the wiring diagrams provided by the vendor show the flame detector wiring connected to terminals 1, 9 and 19 (see first picture below).  Our problem is that when we look at the instruction manual for this device there is no terminal 19, only goes up to 12 (see second picture below).  The wiring diagram they have shown seems to match the Det-Tronics X3301 (last picture below).  
Could you please raise this issue with the Supplier, they need to confirm:
a)      that the flame detectors are Spectrex but the wiring diagram they have provided is incorrect and needs to be resubmitted, or 
b)      they have actually supplied Det-Tronics devices as per their wiring diagram but the datasheets and C14 instrument index is incorrect.
c)       Some other reason
I think the answer will be a) but we need the Vendor to confirm this so that we can get our wiring to the F&G system correct and drawings updated.
First Picture - Snap shot of vendor drawing showing connection to terminals 1, 9 and 19
Second Picture - the Spectrex 40/40 (attached page 39), only 12 terminals
Last Picture - The following is from the X3301 datasheets (attached page 12), which seems to match the wiring diagram above.

