Gents….update to weekly reports 
The AWHP barge is still coming tomorrow 7th

Guys
All change barge will still come in tomorrow as original plan, GAC will re issue confirmation shortly

Colin,
As just discussed by phone new ETA at B3 is now 7-3/16.00. En Avant 1 will increase speed and will make sure to be there on time.
Please don´t cancel berthing or order services again for 7-3/16.00hrs.
Thanks in advance. Have a nice eve.

Please always reply to agency.leith@gac.com
Re      : En Avant 1 / UR6 barge   
Voy   :  38248 
At      : Burntisland
Cargo: Towing UR6 Barge
CANCELLED BERTHING
Good Afternoon,
ETA of tug and barge                                                     14:00 Hours 08th March 2014
On this basis and due to falling tides the berthing of teh tug & barge will not be able to take place until PM 13th when there is going to be enough water available to allow the port tugs to be able to enter Burntisland West Dock.
Please note that all berthing arrangements are cancelled at present.
Will revert with new berthing prospects on Monday.
Apologies for any incovnience caused. 

