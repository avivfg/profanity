Frank
Thanks for this.
This ties in well with our conclusion from today’s SIMOPS session which is
Wye (OISD 3rd May)
SSIV
AWHJ 
AWH deck.
Chris will work it in more detail – but based on below, it will probably be required.

Heads up on Bifab fyi

Frank 
We went through the AWHP jacket here with Bifab again this morning and arrived at the following  
1.       Barge Arrival at Methil 26th April
2.       Prep Barge for Loadout 27th – 28th April
3.       Jacket ready to Loadout 30th April
4.       Loadout and Seafasten 30th April - 2 nd May
5.       Jacket Ready to Sail 3rd May
The above is based upon favourable wind conditions for high level working however bearing in mind that we have now lost three and a half days “up top” since last Wednesday we could be looking at 7th -10th May sailaway - factoring in downtime.  
What is the word on the Stanislav

