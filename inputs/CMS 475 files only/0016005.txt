Just a few things to add Olivier:
-       The importance of issuing the DP&B in late June/early July which requires Project and Asset input to a very tight schedule. A plan is being issued for review.
-       There was a lot of conversation about the progress of the AWHP jacket and the max/min potential for HLV stand-by for this campaign and associated contingency plans
-       There was a request for a note to go to OCM reps over DPM activity charging. A draft has been started.

All,
Feed-back from this morning meeting that may be raised at OCM tomorrow
1.       2014 campaign : 
a.       BiFab jacket : Working at night and at height safely – thanks Frank for your answers
b.      cost provision for HLV stand-by…which we do not hold anymore since we split the 2014 campaigns….
2.       Facilities TCM for 2015 campaign : after issue of AUQ / Hereema revised schedule. Chris/Frank – do we have the official due date ?
3.       Bravo Accommodation Approval : Partners may hold approval till we get complete picture on BiFab/Hereema schedule (refer to 1), HUC job cards. Partners let us Tender but committing is another matter…..
4.       DPM cost recharge (+2.1 M£) : “Change” to the current scheme but admit that our manmonth rate lower than Centrica + common practice in the industry. Questioned the added value of DPM….provided a few examples as “good” seller…. 
5.       BWHP template : I thought this item was closed at previous OCMs….
6.       Drilling Procurement Cost Commitment : seems low to Partners…may give the perception of slow actual progress
Will be in Tender Board for AWHP RTA cost increase (starting 3pm)
Text me if you need me later at OCM.
Clive/Iain B. – feel free to add comments from your view on this morning meeting.

