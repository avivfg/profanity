Good Evening all,
We currently have a fault regarding our barrier system, on leaving the car park they are not properly registering motorbikes trying to exit. This has resulted in a near miss as barriers are not lifting or coming down to soon.
This has been reported and we are awaiting engineer arrival, can I please ask for your safety that you exit to the left of the barrier.
This has not affected cars so please exit car park as normal.

