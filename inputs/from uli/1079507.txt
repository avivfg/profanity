Hi Daryl,
Please find attached a document to be issued for comment at A01.  If possible, can the document be issued with the PDF P&ID’s also?  They are copied into the document but they are not very clear so it would be useful if the proper PDF’s could be issued too. 
Also attached is the wet signed front sheet.
Can you issue to those currently marked on the DDR for Procedures, in addition to the following people for comment:
·         John Wilkinson
·         Graham Sonley
·         John Davis
·         Chris Johnson
·         Craig Trees
·         Craig Wheatley
·         Rory Newton
·         Chris Hamilton
·         Matt Plummer
·         Michael McGurk
·         Duncan Hutton
·         Tracy Brogan
·         Glenn Johnson
·         Gordon Watt
·         John Buchanan
·         Steve Milne
·         Derek Petrie
·         Ewan Mullin
·         Nick Jackson
·         Michel Smeets
·         Andrew Tyrell
·         Lee Boulter
·         John Moffat
·         Malika Jendoubi
·         Mike Carr

