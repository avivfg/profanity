Hi Team,
Please can you help – I’d like to send an email from the project to the statutory consultee for our chemical permits, namely Cefas (Centre for Environment, Fisheries & Aquaculture Science) but when I try to generate a number in CDMS, they are not presently listed. Can you have them added to the drop-down recipient list? If this is going to take some time, could you send me the relevant number? Many thanks!

