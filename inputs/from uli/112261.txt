Ralph
Yes, I would hope that Heerema would have repaired it anyway..optimistic?
I would have to revise the HAN already issued for the PFP jacket paint rework if not already performed, so I guess we need to find out the as-left status.

Bob
any benefit in slapping some sort of paint system over the studs – particularly the bottom where they’re welded onto the top of the node as that’s where any protective coating on the stud will have been damaged by welding?

Ralph,
I’ve been trying to get hold of Richard Russel to confirm, but no joy so far.
The ITC makes no reference to stud material, therefore it will just be a standard stud; it won’t be stainless. 
Regards,
Mike  

Mike – were the M12 stud bolts welded onto the top deck of the comp module stainless, galv’d or what?

