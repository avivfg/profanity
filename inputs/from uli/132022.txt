IDC Transmittal SHL-CYG-I-02905 - CF00-40-IB-71-00001 - A34 - Seaway Heavy Lifting - Master Document Register - SHL-CYG-T-00885 is currently issued to you for Comment, please complete your comments by the date shown on the IDC General page. This has also been added to your assignment page within the CDMS.
Please click on the links below to access the Workflow and your Personal Assignments:
Personal Assignments <http://cdms.gdfsuezep.co.uk/cdms/llisapi.dll?func=personal.assignments> 
PLEASE DO NOT REPLY TO THIS EMAIL. ANY EMAILS SENT TO THIS ADDRESS CANNOT BE ANSWERED. 
GDF SUEZ E&P UK Ltd (Company Number 3386464), registered in England and Wales with a registered office address at: 40 Holborn Viaduct, London, EC1N 2PB. 

