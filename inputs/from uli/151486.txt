Hi Jim
I am not sure that modifying the sleeve arrangement to take an 800Te tool would simply involve thickening the catcher plate to take the larger gripper…..some modifications to the guide cone, sleeve, associated stiffening and leg connection would also have to be looked at to take the increased load. A change of some magnitude would undoubtedly affect the current schedule completion date. My thoughts early on as to leaving modification work to be carried out later stage were put down on attached email.

Hi John,
See thread of emails regarding the levelling of the BWH Jacket if required.
The decision was taken to leave the jacket with the assumption that the extra stiffeners for the design modifications to the pile sleeve could be added at any time before load out.
As it stands if we have to level the Bravo Jacket that would mean that we would have to put the 2 x 400t levelling tools one leg/corner pin it and then move the two levelling tools to the opposite side to pick up the other corner level and pin that, quite a bit of additional work and waiting time for the HLV. (risk)
All based on a good chance that the levelling tools will not be required; this risk could have been avoided if the plates were modified to take the 800t tools
What are your thoughts?

Frank,
The SNCL check on the modifications to the Bravo jacket pile catchers and cones to take the levelling tool has been concluded.  The clarification that the 40mm plate pile catcher will only work for the 400 tonne jack is the biggest factor.  Even with this reduction in load there is still a need to add an extra ring stiffener and extra vertical stiffeners to each pile sleeve to take the load from the jack.
I would like to suggest a plan B:-
We now have the results of the 2014 Bathy survey which shows that the seabed at Alpha has not moved since the 2013 survey.  It is therefore reasonable to make a first assumption that the Bravo site has also not moved.  The Bravo location was shown to be level in the 2013 survey.  There is therefore a good chance that the levelling tools would not be required.  A further Bathy survey of Bravo will be required prior to the 2015 installation of Bravo.
The designed modifications to the pile sleeve are all additions that could be added at any time before loadout.
Therefore I suggest that the extra stiffeners are not added at this time, but we should ensure that the survey at Bravo is complete in time to allow a decision as to the requirement for levelling and in time to fit the stiffeners if they are required.

Raad,

Paul,
You are correct in thinking that it is 400t per catcher, therefore if two leveling tools were to be used (one tool per catcher, two catchers per leg)the capacity would be 800t. This would be acceptable provided that the jacks on the tools are interlinked to ensure even load distribution.
However, if the 800t capacity is required for one catcher then the structure would need modifying / strengthening.
I hope that gives you enough information for this afternoons meeting. If not please let me know.

Raad,
I did not attend the last T&I meeting but just received the “minutes” of the last meeting.
I would appreciate your clarification on the minute below.
Piling levelling & Finger plate requirement.
1 a) Skirt Pile levelling tool: GDF require Lavalin to confirm if additional strengthening of Pile guide is required for 800t tooling. If 800Te tool is required do we need to modify the structure? 
1 b) Pile guides/finger plates may need strengthening. GDF/BiFab to confirm
Bi-Fab/SNC
Open
End of Oct
SNC to discuss with Bi-Fab and JD
Confirmation required that we only had 400t capacity in the Jacket and weather this is 400t per pile or per leg?.
I presume that the 400t capacity related to one catcher and cone assembly, so if SHL were using 2 tools it could be 800t per leg.
Please advise so that I can confirm at this afternoon’s meeting.

Paul,
As stated in the document “BWHP Pile Catcher Design Report to allow use of levelling tool” (document number: CFBW-61-AS-21-00023 Rev C01), GDF established that a 400tn levelling tool would be used and the catcher and cone assemblies were found to be able to withstand the loads due to a 400tn jacket levelling gripper tool. 
I hope that helps.

Raad,
I have been made aware that as a result of the SHL comments on pile sleeve drawing CFBW-61-AS-23-00341-002 F01, copy attached, the pile catcher plates were modified to suit.
At a T&I meeting today I have been asked to confirm what capacity of levelling tool were the pile sleeve/catchers designed for.  
The comments response sheet refers to a SHL document EO-810 dated 7th February 2014. I cannot find this in our system but you may have been provided it and this may provide the answer.
I have requested a copy from GDF, but hopefully you will be able to confirm what levelling tool capacity the pile sleeve/catcher/bottle assembly has been designed, for fairly quickly.

