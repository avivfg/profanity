Brian, as discussed.  I’ve attached a copy of the original RTA in native format – might be of assistance.
Transport:
The contractual conditions that is in place for Cygnus Subsea is Ex Works because at the time of awarding the contracts it was not known where they equipment would require to be issued too.  Therefore there has been a requirement for Freight Forwarding services.  The project Freight Forwarder has performed this service for subsea effectively and efficiently providing collection and delivery within the timeframes set out to support the activates.
Storage:
A number of pieces of Subsea equipment (pulled bends, spare pipe for pulled bends and flanges) have been stored with the project Freight Forwarder in Middlesbrough.  Some have now been issued, and the balance remains in storage with them (spares)  Our intention is for them to remain in storage until after first gas.  At the time of delivery of these items the GaS UK warehouse in Aberdeen was not set up therefore we required a secure and known facility to store the goods until required.  Some of the items are large (pulled bends) and the GaS UK warehouse in Aberdeen is not really suitable for storing them because it is more of a transit facility.
Hope this helps Brian.  If you need something else/more just shout.

