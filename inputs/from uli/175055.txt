Dear Moray,
It appears to be for the same valve as your previous Item 160. Hence you can consider the same price and delivery as provided earlier:
Item
Qty
Size
Rating
Item Description
Pipe
Query
GDF response 17/02/15
Price (GBP)
No 
Spec
Each
Total
6" NB
Plug Valve, CL2500 Hubbed End Regular Pattern Duplex ASTM A182 F51 Gear Operated VDS VPZDG01
DG01
6" was supplied to Cygnus project as double PV only. Pls. clarify if single or double PV is required.
Single plug only please.
Total:
Ex Works delivery will commence in 42 weeks and be completed in 46 weeks
Hope this helps.

Fillip, would this be another one for you?  If yes, please can you provide a budget Price and Delivery?

Dear Moray,
Can you please clarify the following points on manual valves so that we quote for the required materials.
Item
No 
Size
Rating
Item Description
Pipe
Spec
Query
1 1/2" NB
Double Isolation Plug Valve Class 2500 Flanged RTJ Duplex A182 F51 Gear Op VDS VPCDG02
DG01
Pls. clarify if this is to be supplied along with Bleed valve or not
1 1/2" NB
Double Isolation Plug Valve Class Flanged RTJ Duplex A182 F51 Gear Op.  VPCDG02
DG01
Pls. clarify if this is to be supplied along with Bleed valve or not
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1/2" was supplied to Cygnus project as single valve only. Pls. clarify if single or double PV is required.
1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1/2" was supplied to Cygnus project as single valve only. Pls. clarify if single or double PV is required.
1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  c/w Bleed Valve Assembly VDS VPCDG02
DG01
1/2" was supplied to Cygnus project as single valve only. Pls. clarify if single or double PV is required.
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  VPCDG02
DG01
Pls. clarify if this is to be supplied along with Bleed valve or not
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  VPCDG02
DG01
Pls. clarify if this is to be supplied along with Bleed valve or not
1 1/2" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Gear Op.  VPCDG02
DG01
Pls. clarify if this is to be supplied along with Bleed valve or not
1" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Lever Op. c/w Bleed Valve Assembly VDS VPCDG02
DG01
1" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Lever Op. c/w Bleed Valve Assembly VDS VPCDG02
DG01
1" NB
Double Isolation Plug Valve Flanged RTJ Duplex A182 F51 Lever Op. c/w Bleed Valve Assembly VDS VPCDG02
DG01
6" NB
Plug Valve, CL2500 Hubbed End Regular Pattern Duplex ASTM A182 F51 Gear Operated VDS VPZDG01
DG01
6" was supplied to Cygnus project as double PV only. Pls. clarify if single or double PV is required.

Dear Moray
Thank you for the enquiry. I will look into it and will submit our offer by We next week the latest.
Our reference is Q1502045-FK.

Slight correction Fillip, contract no. should read 41000011036.

Hello Fillip.
Your company has recently supplied the “Double Isolation Plug Valves” for the Cygnus project under Contract 4100001147.  We are in the process of evaluating a potential additional piece of work that would use the same products and would like you to provide a Price and Delivery quotation for the items listed on the attached spreadsheet.  The Specifications, QA/QC and Documentation remain unchanged and your quotation requires to be based on the already approved/agreed documents that you complied with for the Cygnus project.  
At this time the additional phase of work is not approved and we are preparing an outline cost and schedule for feasibly/evaluation purposed.
It is noted that some surplus items from Cygnus may be used in support of Cepheus.
Please respond by COB 18/02/15 or earlier?
Any questions, please revert.  
Your points of contact for Cygnus remains unchanged, however they are not involved in this activity and I would be obliged if you do not contact them in relation to this enquiry.

