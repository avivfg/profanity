All,
Please see attached the approved Potential Change Notification (PCN) form for “Coating spec change at Jacket leg” that has been passed through the Management of Change Procedure (CF00-02-ZA-67-00003) and ACCEPTED for implementation.
IM.CYG,
Please store the attached in the corresponding folder in CDMS.

