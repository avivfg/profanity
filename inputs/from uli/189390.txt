John, Martin, 
Please find attached our draft Method statement and the transport manual for the Meri vessel in relation to ex-works and shipping activities of the UQ Helideck from Teesside to Bifab prior to our meeting on Thursday 08th January in Bifab.
I will send the rest of the documents and drawings by separate email to make sure you receive them all safely.
Notes: 
§  This is a complete Method Statement and Risk Assessment in relation to all ex-works activities, i.e. Topping out, jacking and weighing operations 
which already took place. However as we have provided the similar documents for the BW Helideck to the project, I send you the full RAMS 
for the UQ Helideck as well.
§  The vessel transport manual is currently being reviewed by Noble Denton for their comment / approval, therefore it might have to be revised 
depending on their comment(s).
§  for your information following weighing of the UQ Helideck on the 19th December and declared total weight of 81.89 tonnes , as agreed with the project 
we did send the Lifting Frame to the manufacturer for the equipment to be load tested and re-certified to its new SWL of 85tonnes.  I will send you the new certificates along with the revised drawing for the lifting frame demonstrating the revised SWL as soon as we have received them. 

