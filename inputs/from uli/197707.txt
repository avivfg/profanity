Hi Dave, Setti,
To confirm, dossier has been updated to include the drum numbers on the certs and has been passed to the warehouse for shipping with the goods.
Cables are now ready for collection.
Please contact Reece White (in copy) to arrange collection time.

Folks,
 Please find attached approved data dossier and IRC also available in Drop Box
Note Prior to issue please incorporate IRC and ensure reel traceability is transferred onto cable certs
Cheers
D

Hi Dave,
Revised NORFI attached - working from two sites it was bound to happen at some point!  
Apologies for the inconvenience.

Liz
Please revise Norfi as 18 taken, 127 is the r01 latest DD no 
Please revise and resubmit if you speak to Brain he will confirm
Cheers

Dear Dave,
Please find attached our Inspection Notification NORFI-GDF-00018 for cables on Release 9 (GDF PO 4200051030), which will be ready for visual inspection at Noskab, Aberdeen on Monday 12th January 2015.
Please confirm whether you wish to inspect on this date and advise details of your inspector.
Alternatively should you wish to waive inspection please advise - I have also attached a copy of the packing list.  The R01 dossier has also been uploaded into the Drop Box.
R01-00125-01 - PL08

