Hi Ibrahim,
I’m ok with the 10% allocation to HSE in the tender evaluations (see Brian’s email below).

Thoughts ref the technical scoring criteria:
I think the weighting for item 6 Proposed Product and Technical Solution (including equipment weight, footprints, operating reliability and fuel efficiencies) should be increased to 25%, with item 8 - Proposed storage facilities reduced to 5%, and item 9 – Proposed schedule delivery reduced to 15%. 

  Gents
This is the type of scoring we need to use .....
We just need to agree how we do the 100% carve up...
Let me have your thoughts but I would say 
HSE                             10%
TECHNICAL                 45%
COMMERCIAL            35%
QUALITY                     10% 

