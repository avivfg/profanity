Hi Dick,
Sorry forgot the attachment on my original email please see attached for information.

Hi Dick,
I can confirm that we have a pool of Technicians that are all certificated to work offshore and if this service is required we can accommodate as required. We are currently waiting on fixed dates to complete the surveys for the Platforms as listed below and if necessary we can do the installation work at the offshore location for all platforms.
Bravo WHP 
Heerema, Victoria Dock, Bond St, Hartlepool  
Site Survey - End Feb early March 15
Sign Delivery - End April 15 (wk 18) / first wk May (wk 19)
Alpha PU & ACM 
Heerema, Greenland Road, Hartlepool 
Site Survey - End Feb early March 15
Sign Delivery - End April 15 (wk 18) / first wk May (wk 19)
Alpha UQ  
Burntisland Fabrications Ltd (BiFab), C/O, Fife Energy Park, Wellesley Rd, Methil
Site Survey - Early March 15
Sign Delivery - By 15th April 15 (Wk 16)
I have attached a cost breakdown for the charges for supplying Offshore certificated Technicians for your information. If you need any additional information please let me know.

Hi Chris,
Request from Richard Everton, GDF Suez regarding Installation.

Gail,
Could you please confirm that Seaward Safety could undertake the installation of Safety Signs offshore – i.e. that you have suitable personnel with a current survival course certificate etc.  

