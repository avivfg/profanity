Hi George,
Noted – please find attached revised front sheets 6,7 and 8.
Any problems, please let us know.

Geoff,
I believe we can now progress VO 6, 7 and 8, however, the front sheet values for each will have to be revised and re-submitted.
To summarise:
VO 4 approved giving revised Contract Value: £8,057,847.00
VO 5 was not accepted/approved.
VO 6 start value is £8,057,847.00 + increase of £ 75,373.00 = £8,133,220.00
VO 7 start value is £8,133,220.00 + increase of £8,677.00 = £8,141,897.00
VO 8 star value is £8,141,897.00 + increase of £23,800.00 = £8,165,697.00
Please revise the front sheets and re-submit to me urgently. Can I have these sent to me for start of business Thursday 08th May 9.00am? I need to get these on the signature trail urgently.
Many thanks in advance for your assistance.

George
   I had hoped to get the final RA report today but it now looks like this will not be available until next week. I can say that the indications are that we can accommodate your engineers request to extend the shutdown time delay on detectors going into fault out to 2 hours. We will confirm this next week.
In the meantime, I have arranged for the detector brackets to be sent to site and attach the Work Package document for welding in the brackets. Our site team will take the lead in getting the brackets installed but will require assistance from Heerema with the welding and provision of access platforms as we discussed at site. Siemens are ok for this work to go ahead in advance of the VOR being signed off so, as long as GdF-Suez are in agreement, we can get the mechanical part of the work completed before installation work gets underway.

