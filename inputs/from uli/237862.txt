Approved C&P

Dear All
We would like to request approval, for the following purchases on the Procurement Card for the Heerema site commissioning team” 
“Good/services required:”
Description
Power plugs 2.5mm, 2.1mm & 2.0mm
Supplier
RS Components
Justification
Required for the project to replace the plug on a battery which is currently broken.
Cost
Requestor
Chris Johnson
Budget Holder/Approval
Frank Stafford
Cost Code
O.SN011.C.CN.B00100

