I was advised to pass these onto Brian Doyle.

All,
Further to earlier e-mails the ALPHA 055-Z001-A & Z001-B keys should have been sent to:
Delivery Destination 
Heerema Hartlepool Limited 
Bond Street, Victoria Dock 
Hartlepoolartlepool, TS24 0JS 
England 
FAO: Mr.Callum Morris
As per the attached e-mail sent to SPP by Ray Southwood. Brian/Callum please confirm.

Gents
When the keys are available for the UQ Fire Water Pumps, the address to send them to is :
Burnt Island Fabrications.
C/O Gary Maguire.
GDF Offices.
Fife Energy Park, Fife.
Wellesley Road.
Buckhaven.
Fife.
KY8 3RA
Thank you all for your assistance.

Hi Chris
The below emails refer to both Alpha skids
If they were not rec’d by Callum Morris and Bryan Doyle please let me know and I’ll check with my colleague Tim

Tim
Would you arrange please – keys are with you
Bri

Brian, 
Please address the package to Callum Morris who will pass the keys for the SPP Pump enclosures to Brian Doyle (Heerema) for safe storage.
If you could make sure the Courier is Next Day delivery (maybe too late today) as the Heerema Site is closed from 10.00am Friday 20th December 2013. 
Delivery Destination 
Heerema Hartlepool Limited 
Bond Street, Victoria Dock 
Hartlepoolartlepool, TS24 0JS 
England 
FAO: Mr.Callum Morris

Hi Brian,
I received 2 keys for the fire pump enclosures which were in a bag labelled “Cygnus Bravo Enclousre”. Are these the same keys to be used on the Alpha Fire Pump?

Hi Dennis
Enclosure keys, please see attached emails 

Good morning Brian, Alan,
Please see the below e-mail from site. Can you respond re the Alpha A & B keys please.
Also, the above jogged my memory - I don’t recollect receiving a response re Bravo re another site query. Please see attached.
Your response by return would be appreciated please.

Dennis,
Re- below, any idea where the door keys are ?
Appreciate any help you can offer.

Mike
Another example of missing door keys. The Expeditor’s initials are EA2? 
Could you please pass on the below message to whoever this is.
PO CF00-32-AM-70-00160A

John
As previously discussed could we enquire as to the whereabouts of the keys for the SPP LTD supplied AUQ Firewater pumps ??, there are a total of 3 personnel access doors per pump (SNS-CFAA-055-Z001-A & B) so grand total 6 doors !!, I’m not sure if there is only 1 key per unit which fits all doors or it may be the case 6 keys will be required (1 per door) ??, many thanks !!!

