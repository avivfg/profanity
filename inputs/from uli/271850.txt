Sounds good, is the crane to get you outta bed each morning?
Seriously though, great organisation, we owe you. Looking forward to a few bevvies come Thursday night to catch up.
TMC

Update for ya my son.
There are heavy lifts going on Friday so access to BWHP may not be possible.
Safety brief in the morning then split into two groups. One group doing harness rescue training – dangling people off scaffold. At the same time the second group will be suiting up and going into a blast chamber to experience how it feels, this won’t take as long as the harness so at the same time we will either tour BWHP or watch the crane getting installed.
Lunch at Pizza Hut.
Afternoon will be tour of APU and Compression Module.

Dear All,
Please can you fill in the attached Site Visit Request form ASAP and return to Naomi. We have to get these OK’d by the Site Manager before the trip.
A quick update on logistics will be sent round tomorrow!

Dear All,
You all have a space on the trip! If you feel at any time that you cannot make the event please inform me ASAP as this is a small group and big drop outs will likely lead to cancellation as it will be too late to find replacements!
That said, I’m sure we will have a great trip!

