Thanks Chris

Mac,
All the P&ID’s (attached) can be found in CDMS via:
SNS > Supplier > CF – Cygnus > 4100001011 - Heerema Fabrication Group - Cygnus - Material Equipment and Fabrication  
If you set CDMS to 125 items per page you will find all CFBW drawings on pages 9 & 10.

Hi Mac,
Andy Ward from Heerema is the man who will have all this however he is off site today. Due back on Monday. Same with Martins fridge query.
Andy – it’s just been told I’ll be signing off on the architectural ITR-A’s. When do you expect these to start filtering through? Where can I get access to the Barrier drawings / documents for review before sign off?

Hi Chris,
                Can you point me in the direction (or supply) the Domestic (Hot & Cold) & Waste Water P & ID’s for BWHP ?

