email sent yesterday from sparrows to Gordon Thom

Hi Gordon,
As discussed on Thursday, can you please confirm when the adhoc crane operators for the HUC is required?
I.e. the crane operator residents will begin in approximately August.
Therefore for your requirement you mentioned 3-off adhoc crane operators required at a time:
Dayshift:              Crane operator resident               Adhoc crane operator
Nightshift:          Adhoc crane operator                    Adhoc crane operator
Can you please confirm which date this starts? so that I can get this included in a budget to Glenn.

