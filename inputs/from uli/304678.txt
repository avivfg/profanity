Hilary,
They know to arrive promptly at 07:30 right?
See below:

Morning All,
Please see attached Method Statement, Risk Assessment etc from Blackburn Starling.
Paul/Chris, - note Paul (Blackburn Starling) response below, regarding torque the bus-bars on Bravo switchboards this work to be completed following the APU, LV Switchboard installation, as per the site visit on the 8th Sept?
Response from Paul, (Blackburn Starling) - is in the plan and subject to how we get on with APU.

Hilary – Please see attached.

Thanks Paul,
Any chance you could forward a Method statement and Risk Assessment, and a rough plan of scheduled hours and work to be carried out whilst on site.
Can you also confirm if you require temporary power and the type of connections (make & model) you may need?
Heerema will request all certification of any RAMS / Lifting gear you may use, can you also provide this.

Hiliary – That is in the plan and subject to how we get on with APU

Morning Paul,
How are you fixed to include the Call Out request attached – this work to be completed following the APU, LV Switchboard installation, as per the site visit on the 8th Sept?

Hello Paul,
Following on from our conversation, please see attached a Call Out form, for installation and testing of the BWHP LV Switchboards.
The date is requested for 21/09/14 however ideally the site were hoping you could move over to BWHP following completion of APU LV Switchboard installation.
I will leave it with you and hope to hear from you soon.

Hi Hilary,
Please see attached signed call out form for Blackburn Starling to assist with installation and testing of the BWHP LV Switchboards.
I have put down the request date as 21/09/14 however ideally want them to move over to BWHP following completion of APU LV Switchboard installation.

Gordon/Hilary,
I have reviewed the Erection & Installation Procedure as well as the Mandatory Design Requirements and there is no mention of it being compulsory for Blackburn Starling to install or supervise the installation.
The Installation & Commissioning Manual for the BWHP Switchboard doesn’t even exist however the extract from the APU Switchboards reference the below documents.
I was not partial to the conversations held with Peter Franklin however know from talking to him he didn’t see why Blackburn Starling would need to be involved in the installation of these units. It’s what Heerema do!
The planned installation date for these units is 20/08/14.
On that basis I see no reason why we would stop HFG from progressing with the job…. unless you have something in writing that says any different?

Hi Hilary, we are going to install the LV Switchboards ourselves as waiting beyond 8th Sept will cause delays to the Heerema schedule.
Whilst the guys are on site it makes sense for them to torque the bus-bars on Bravo switchboards also. Do you have the Word version of the Blackburn Starling call out and I will amend it to cover Bravo also?

Chris,
I’ve attached the Call out request originally created by Peter Franklin for BS to Switchboard on the PU only.
You will need to create another Call out Request to Blackburn Starling for the Bravo.

