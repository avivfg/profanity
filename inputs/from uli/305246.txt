Max,
We are now in the process of confirming the booking of a dedicated Vessel for the shipment of the MEG Package to Hartlepool.
The proposed FCA date for the MEG is now 17 March 2014.
What this means is that GDF have instructed AMEC to request COMART to submit a new VOR (please revise your current VOR-018) to show 17 March 2014 as the revised FCA Contract Delivery Date, with no cost implication.
Our intention is now to accept the MEG Package fully completed including Final Documentation including Data Dossier (R04), Dispatch Dossier (R01), fully tested and packed for shipment at O.C.R Premises. 
Reference is made to the last comment in MoM 968 para 13.1 in which it is stated that:
“COMART again confirmed that to complete all of the above activities, the FCA Date is forecast as the 08 March 2014”.
We believe, it is in everybody’s interest that COMART are allowed the additional time we now have available, to complete the workscope to the satisfaction of both parties.  
We trust that COMART will accept the Project proposal in the “good will” that it is intended. 
Next Progress Meeting is scheduled for Feb 11 as previously agreed.

