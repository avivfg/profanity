As discussed. Procedures highlighted in yellow on attached spreadsheet.
Fusion can be accessed using:
Link : https://docfusionlon.amec.com/Fusion/Core/Controls/UI/Web/ApplicationPage.aspx
Server : London Projects
User Name : chris.kelman
Password : Ljunberg8!
Group : GDFSuez
The guys will be here on Monday.

Chris
Procedures need to be looked by Jeff that will also dictate the type of welding machine.
Ray I assume consumables will be provided by Bendalls

Chris,
Further to my conversation with Norman Addison at Bendalls , he has confirmed that both listed person will arrive at Heerema Hartlepool site on Monday 14th July at 7.30 to be Inducted.
All Welding Procedures are Code 1 (see attached)
Please could you arrange for a 240volt Welding Machine available for Monday Morning 14th July 2014.  
1)      Mr Chris Worth (Senior Inspector) 
2)       Mr Norman Coombe (Welding Forman) 

Richard , 
Further to our telephone call this morning and to provide you with the background and to avoid any confusion with both parties. 
To clarify we have two different issues here on two separate items.
Remedial work on item 1 below.
1)      As per your email below “overlaying the groove inner face with alloy 625 then re-machining the groove to the correct dimensions” is for the HP Flare KO Drum 030-V001.
The second item which GDF require Bendalls involvement is:
2)      Internal and external inspection of damaged nozzle K9 of 035-V002 which was damaged during a Heerema lifting operation.
We have raised an Over Short and Damage (OS&D) for the latter issue which is now with AMEC. I am not sure if AMEC have even made Bendalls aware of this issue however it makes sense to coordinate the visits. 
As Bendalls will be working within a confined space I would like the attendees to complete the full level 1 induction which as you know is held on either a Monday or Thursday mornings.

