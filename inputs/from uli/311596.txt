Hi Janaka,
We have found a temperature/pressure relief valve on our safety showers which was missed from our PSV register.
I have attached the technical datasheet for it. Is this something you could calibrate or would you recommend purchasing a replacement?
Do you have means of testing temperature relief valves at the Broady facilities?

