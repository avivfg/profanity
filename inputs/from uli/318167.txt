Hi Stevie,
Another update as we have just received torque values from the Methanol Injection Skid vendor, both lids are 331Nm (lubricated) – see attached data sheets.
Torque table updated.

Hi Stevie,
I have attached the latest torque table which includes 5 new joints to be remade all within the Cellar Deck Deluge Skid. I have put together a slide show to help indicate where the joints are however feel free to give me a call when you plan on doing them and I can point each out if required.
I am really struggling with Amec/the vendor to get torque values for the strainer lids however I have received a value of 350Nm For SPS-009 (the Closed Drains Pump Strainers).

Hi Steve,
Please see attached the latest Packaged Equipment Torque Register. Items 23-30 cannot be completed just yet however I have included them as an advanced notice.
The additions to the list from last time are as follows:
Ø  The four instruments on the Vent/Closed Drains Drum (Cellar Deck) level gauges have now been remade with the correct sized bolts. The have insulation blankets tied round them that you will need to remove – when they are removed could you please take care not to misplace/misuse them and let me know as soon as the torqueing has been completed so I can arrange for someone to re-instate them.
Ø  The blind on the flushing point of Deluge Skid (Weather Deck)

Right Steve, 3rd time lucky…..
The joints in all four instruments on the degasser have now been swapped out for the correct duplex type – I’ve checked them myself this time.
I have still to replace the bolts on the methanol tank level gauge spool piece so that is still on hold.
There are 3 more joints that require torqueing to be added to the list:
·         The Fire Pump Diesel Day Tank Manway (inside fire pump enclosure)
·         The blind underneath the Fire Pump Diesel Day Tank level gauge where the float has been installed (inside fire pump enclosure).
·         The flanged connection between the silencer and the backup habitation generator.

Hi Steve,
I have another batch of joints to be remade as detailed below:
Ø  The three other instruments on the Produced Water Degasser (Mezz Deck) have now been remade with the correct sized bolts and a new 1 x 2” 4.5mm Nom Thk 300# spiral wound gasket, SS outer and SS inner ring with compressed non-asbestos filler.
Ø  The Deluge Skid (Cellar Deck) RO (SNS-CFBW-055-RO102) has been replaced and the joint has been remade with 2 new 6” 150# spiral wound gaskets, ASTM A182 Gr. F53 (Super Duplex) inner and outer rings, non asbestos fibre inhibited graphite filler. The joint is inside the deluge enclosure.
The two strainers (Cellar Deck) (SNS-CFBW-035-SPS009) upstream of the Closed Drains Pump inlet have had the internals removed for inspection and replaced with new RTJ.

Hi Steve,
Could you please torque and tag the following bolted connections (to be added to an extra section of the PNS flange management register as they are not covered on design iso’s):
·         The instrument connection on the north facing stand pipe of the Produced Water Degasser (Mezz Deck). It has been remade with:
Ø  1 x 2” 4.5mm Nom Thk 300# spiral wound gasket, SS outer and SS inner ring with compressed non-asbestos filler.
·         The spool connection on the corner of the Methanol Storage Tank (Cellar Deck). It has been remade with:
Ø  1 x 2” 4.5mm Nom Thk 300# spiral wound gasket, SS outer and SS inner ring with compressed non-asbestos filler.
·         The flanged connection containing a RO on wet seal pipework off Service Water Transfer Pump A & B (Mezz Deck). They have been remade with:
Ø  A ¾” 4.5mm Nom Thk 300# spiral wound gaskets, SS outer and SS inner ring with compressed non-asbestos filler either side of the RO.
Note: Joints not shown on model – approx. location highlighted below – you can’t miss them.
·         The flanged connection containing a RO on wet seal pipework off Diesel Transfer Pump A & B (Cellar Deck). They have been remade with:
Ø  A ¾” 4.5mm Nom Thk 300# spiral wound gaskets, SS outer and SS inner ring with compressed non-asbestos filler either side of the RO.
Note: Joints not shown on model – approx. location highlighted below – you can’t miss them.
·         The flanged connection containing a RO on wet seal pipework off Closed Drains Booster Pumps A & B (Cellar Deck). They have been remade with:
Ø  A ¾” 4.5mm Nom Thk 300# spiral wound gaskets, SS outer and SS inner ring with compressed non-asbestos filler either side of the RO.
Note: Joints not shown on model – approx. location highlighted below – you can’t miss them.

Chris / Paul
Here is the email address for John Richardson (Cordell Project Manager) & Steve Sanderson (PNS Flange Management Engineer). 
These are the guys that will accommodate with any Vendor torqueing activities or commissioning activities that are required as discussed earlier. Steve will deal with the Flange management and Torqueing that is required, but as PNS are Cordell’s subcontractor John needs to be kept in the loop so the hours get booked against the attached VOD.
Any problems give me a call.

