Olivier, 
I have spoken with Simon about his availability for the QRA and he has given me options on the following dates:
·         17-25th November
·         8-12th December
·         (Possibly – TBC) 15-19th December
So my questions are what do we think we can get done at each of these times and which is preferable?  … Bearing in mind that it will take a minimum of 3 working weeks to get the answers and internal agreement (and that is without Paris).

Folks,
Not sure what the outcome of the discussions were yesterday, but speaking to Steve Thomas, it will be a phased approach with APU Topsides Schedule / Planning Package issued first, then BWH Topsides Schedule / Planning Package (still to discuss with their sub-contractors to fully verify details / handovers), then ACM Schedule / Planning Package. All of these Schedules MUST have full MC content or we cannot verify the Onshore Commissioning duration from the GDF detailed onshore commissioning schedules (Vicky). Only on the receipt of this can we start analysing, discussing and agreeing schedules as both accurate and achievable. Thereafter, our GDF Control, Detailed and Summary Schedules need to be updated.
And then there is BiFab AUQ Topsides………………………….
This is not a 5 minute job. I’d suggest by the end of October (earliest) we have all the Heerema detail for analysis. 2 weeks for review, debate and agreement………….and we need to produce the Monthly Reports in the first 2 weeks of November as an added distraction. By End of November it will all be agreed and reflected in the GDF Schedules. The Dashboard and S-Curves will all be updated.
Basically QRA will need to be in December for me and is subject to receipt of all data from the Fabricators. On receipt of data from Fabricators (all Topsides), +1 month for the review / agreement and update of GDF Schedules and Planning Data. Then there is the time required to update and run the risk model by Jason / Simon.
Planners – Any thoughts on the above timelines?

Folks,
Please postpone QRA in view of on-going discussions with Hereema
·         Bayerngas just agreed by email
·         Centrica agreed in principles
The next question we would have to answer is when we could execute the QRA.
Jason,
Please build a revised timeline on the basis that it should take 1 month to receive/validate the Hereema revised schedule.
We need to allow time then for building the control schedule, input data, etc….

