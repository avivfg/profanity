Hi Chris,
That’s the booking moved to the 13th for you, just let us know when you’d like to have it changed to suit your schedule and we’ll get it done for you.

Hi Kim,
If it’s easier doing it this way, then please book me onto the 1440 on 13th February (if available) and we can change it nearer the time.

Good morning,
I’m just off the phone with Gray Dawes who have informed me that your ticket is actually a fully flexible one which means that we can book you onto any other flight time/day free of charge.  We can move the ticket onto anything for the time being and can then change it to whatever you need it to be once you know what your schedule will be?
It may be easier than going for the refund and then re-booking?
They have also informed me that although the 1440 is still showing up on the system, it won’t allow any booking on it!

Hi,
I will be in Hartlepool on the 12th February, however might not come back to Aberdeen until 13th February. Think it is best if we can get a full refund, then I can re-book nearer the time to suit meetings at the yard and arrange travel around the agendas when known.
I do note that they can’t offer me the 1440 anymore and have cancelled it, however the alternatives they can offer are the 1100 or………………the 1440! (MME ABZ 1440 1540   - FlightT3 586)
Anyway, if we get the refund and re-book as appropriate nearer the time. Appreciate your help on this.

See below email from Gray dawes.
Please advise how you would like to proceed.

Good Morning,
Can you please advise Mr Lochead that his flight with Eastern Airways ( as below)  has been cancelled by the Airline and is no longer operating at 14.40
Passenger Names:
MR CHRISTOPHER LOCHHEAD
Flights
Segment
Airline
Flt Num
Class
Depart

Arrive
Stops
Seats
Aircraft Type
Operated By
Flt Time
Eastern Airways Ltd
T3586
ECONOMY
Thu 12-Feb-15 14:40
Durham Tees Valley Arpt, Durham
Dyce Airport, Aberdeen
Thu 12-Feb-15 15:40
	British Aerospace Jetstream 41
	1 h 00 m
The alternatives offered are for different flight times or a full refund if no alternatives are suitable. If he does still wish to travel on 12 Feb the alternative flights are
MME ABZ 1000 1100  - Flight T3 582 
or
MME ABZ 1440 1540   - FlightT3 586      
Please advise how you wish to proceed with this booking.    
Regards,

