Hi Trevor,
Just been to a Workshop and it looks as we will only drill 3 wells at Alpha, so the drill rig moves Alpha to Bravo at end September 2015, long before 1st Gas on Alpha; 1st Alpha gas is December 2015 with no carryover or March 2016 with carryover.
Assumption is correct at present.

Hi Chris… is the red-text assumption correct?  I need to ensure that associated regulatory submissions do not slip between the cracks. Many thanks!
Cygnus Project
GDF Suez
Ref
Title
Description
Status
Risk
Progress
Legislation
Format
Body
Application Preparation Days
Review Period Contingency Days
Approval Days
Target Submission Date
Days to go
Key Stage
Key Stage Date
Submitted Date
Accepted Date
Expiry Date
Responsible Person
Accountable (Position)
Before / During / After
Consulted (Position or Organisation)
Informed (Position or Organisation)
LP Checked
Notes
Notification of Combined Operations (Drill Rig at Cygnus Alpha)
Notify HSE of combined operations
(Current basecase schedule has drill rig departed Alpha prior to first gas therefore this would not be required.  Tracker items deleted and this item has been kept as a marker) 
Application not started
High
SI 2005/3117 - The Offshore Installations (Safety Case) Regulations Reg . 10
Prescribed Content
HSE
5 days until Start of Preparation
CYFA28230 2015 Offshore Construction (Topsides/Jackets - Start)
Fraser Easton
Project Manager
Before Commencing combined operations
N/A
N/A

