OK Frank, just for clarity, this is not for tomorrow, it is part of the weekly reporting.
We already report on Piping and E&I……….the Structural (value?) and Equipment Metrics are the additional requests to add to the weekly report.

Chris, All,
DPM have developed an unhelpful habit of issuing last minute demands just before the DPM Meeting. We've had others today already. Same in previous months.
Suggest we don't react until we've had a chance to consider whether the additional metrics will add value, can be easily obtained and don't create unnecessary extra work.
Suggested answer tomorrow: We will review the request, consider it on its merits and then respond.

Folks,
See e-mail below I have just received from Charles Muzelle regarding weekly metrics DPM want to see for each BiFab and Heerema scope in the Weekly Planning Report.
With regards to Piping, Electrical and Instrumentation Metrics (I’ve combined E&I) I have made a proposal that we will start reporting on this week, see below (Annie has now refreshed the Weekly Report to reflect Piping and E&I metrics).
With regards to Equipment and Structural Metrics, I have spoken with John Dingwall and the proposal below on these metrics can be delivered each week from BiFab, hopefully the Heerema lads have similar metrics available at hand? Let’s start these metrics (Structural & Equipment) from next week when you have had a chance to get the base figures.
I am assuming that we are going to comply with the DPM request for additional metrics? If we are, the following is the proposal for all metrics (per scope) to be delivered as part of Planning Team Weekly Report:

Jean-François,
Early August wPlease find heerattached the wxeekly report issued by Chris team and showing the weekly follow up for piping. We checked with Chris (see attached email) if it was possible to have on top:
1/ For Bifab AUQ the same weekly quantity tracking that the one you are following with Heerema (ie per trade).
2/ For Heerema and Bifab the planned quantity per week (on top of the actual you are already reporting).
3/ For Heerema and Bifab the weekly average actual head count vs the average planned head count (for directs only)
The third point is integrated now, but the two others calls for a lot of work and may the report heavier. Chris has however add a “macro” weekly qty for each (prefab and erection):
-           F’cast Spool Delivery For Week 
-           F’cast Spool Erection For Week
(in this respect you can see for instance that on PU deck the weekly fabricated spool is below the planned, but that the weekly erected spools are above).
I think this management report for piping is sufficient.
Saying this we don’t report the other trades.
Chris,
Could you think about adding some high levels Weekly Qty tracking (the most representative) for the other trades, for instance:
1/ Instrum:
1.1   Cable pulling (lm)
1.2   Instruments installation (no) incl. analysers (but not sure as it may be peanuts compared to the rest)
1.3   Cabinets/panels installation (no)
1.4   Connections (no)
Don’t think it is necessary to follow telecom, PA, JB etc. and the tests
2/ Elec:
1.1   Power Cables installation (lm) 
1.2   Electrical Equipment installation (no)
1.3   Connections (no)
Don’t think it is necessary to follow grounding, lighting, tracing, CP, outlets, JB etc. and the tests
3/ Steel Structure:
3.1 Main Steel Structure installation (T)
3.2 Secondary&tertiary Steel Structure erection (T)
Don’t think it is necessary to follow grating, cladding etc.
4/ Equipment:
4.1 Static Equipment Erection (no)
4.2 Machinery erection (no)
4.3 Package erection
Don’t think it is necessary to follow internals installation and other equipment
Is it necessary to follow painting/TSA? I don’t know how many direct hours it represents in comparison to the rest.
I Don’t think it is necessary to follow insulation and fireproofing.
These are suggestions, do not hesitate to comment

Folks,
Attached is the usual non sanitised full version of the Cygnus Planning Team Weekly Report including (in addition to the Focus Areas) the Areas of Concern and Scope Statistics. This report is distributed to you as a smaller more focussed team who are directly involved in the main Project events at present, with the summary version issued to a wider audience to improve communication regarding Project status.
Cygnus Planning Team Weekly Report is attached for information, reflecting bullet points on current focus and concerns within each area (Amec / BiFab / Heerema / SHL / Isleburn / Saipem / Drilling / Subsurface / Commissioning / Hook Up Etc.)
Please note that this is an ‘internal project’ report, reflecting the Planning Team’s view on both Planning and Performance status to give you a flavour of what is going within each scope.

