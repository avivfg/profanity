Phil
I’m the approver. This looks fine; you just need to issue it via doc control.

Morning
This is the revision C02 of the boundary drawing for checking and approval.
I have drafted the revised overboard drains, who is the approver?

Hi Phil 
Here is the revised drawing you requested from me for you to check.
Would you let me know whose initial should go in the approval box … I’ve used Martin’s but I’m not sure this is correct.
Would you also copy Fiona into your reply.

Afternoon Alison
Attached revision C01 of the completions boundary drawing together with mark up of engineering drawing CFBW-32-AP-62-00044 sheet 01.
Please revise completions boundary drawing CFBW-02-KC-62-00044-001   to reflect the additional drain lines within sub system 036-BWHP-04.

