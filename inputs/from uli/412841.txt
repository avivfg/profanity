Dave
Have you had an update on this as I Haven’t heard anything more on the investigation starting. Reason being we can potentially engage a resource to look at the IS side if we are clear on what’s required.

Bonjour Dave,
BWHP has been installed ~200m from its planned location.
Looks like SHL is organising an IT investigation to find the root cause.
Gas UK IT group will have to be involved (either directly or via a third party to be sub-contracted).
Have a think of it (internally first) and let’s see how it develops in the coming days

Ryan
We have a bit of time to work to get the right people. I think we need to get one of our decent IT guys to recommend an " expert" dependent on the manufacturer of the computer blah blah. 
Let's talk later. 

Any thoughts Olivier as to who is best placed to help us with this from an IT side?
Ryan 
Begin forwarded message:

Hello Ryan,
In response to your below message, please be advised of the following from the Investigation Team.
I understand that the IT will conclude their on-board investigation tomorrow AM, all parties have collected the same data and are ready to travel home after 1200hrs 24-July 2015. Our Travel Dept will liaise with your Rachel for the travel arrangements.
Subsequently they will start to report the facts, establish root causes and contributing factors, a draft report should become available somewhere next week.
On this subject, I received the following email from DOF just now.
Quote
As a result of the initial findings and as a precautionary step we would like to propose the primary navigation computer on board Oleg gets quarantined and the second (standby) computer is switched over to be the primary online computer. The hard drive form the primary will be imaged so that there is a reference file for future as part of the investigation.
The primary computer will then to be returned to Aberdeen for further investigation and analysis by an independent third party.
A new project will be set up on the secondary online computer where all offsets, corrections and DGPS/USBL comparisons can be input and completed to ensure a clean navigation system. This will be set up prior to the next scope.
Are you also in agreement with the above and can we progress and go ahead with this course of action?
Unquote
SHL are in agreement with the above, subject to the condition that the primary / quarantined navigation computer will not leave before its replacement safely arrived on board, and is proven operational. Furthermore we consider it of paramount importance that this primary computer is investigated by an true independent third party, and as such we propose you to select this.
Finally we believe it is necessary to agree the scope of work for this computer investigation, which we will request DOF to provide for review and approval by the IT Departments of GDFS UK and SHL.
Please advise / confirm the way forward.
Thank you for your attention / assistance.

Eppo
For my own info:
Do you have a timeline for the investigation and is there any information that can be shared now?

