I’m chasing Liz for a response… we now know some of the answers but not all

Hi Liz,
As you may know, John Rogers has asked me to get involved part-time with the running of the HU base at LV Middlesbrough.  
I’m trying to get my head round the cost status. Maybe you can help … I’m assuming it falls under your wing as part of HUC?
1)      Is there a Budget and forecast for the hire and running of the Base? 
2)      Is there a budget and org chart for the Site Team?
3)      Are Requisitions / PAF’s in place for the Site Team, whether employed through GDF or LV?
4)      Is the cost of the Base (incl Team) included in the monthly Cost Report and Cost Review meeting?
5)      Anything else?
Any help appreciated!

