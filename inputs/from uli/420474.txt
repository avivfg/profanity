Dave, John,
LOI is in line with Friday meeting debriefing + protect well our rights during the interim period – thanks Dave
While we shift Ready for Sailaway by 4 weeks (15-May to 15-June), we grant a greater move of Mechanical completion dates for BWHP & ACM by accepting April 6th. See attached the letters we sent when we moved the contractual dates for all call-offs. 
April 6th is fine for APU. If the negotiation is still open, I would try to get an earlier date for ACM & BWHP to protect our commissioning schedule.
LOI effective date : not clear to me. Is it date when HFG return acceptance on LOI ?
I would put a specific date for HFG to return & acceptance

Ruud,
Please find attached a first draft of the LOI to progress the agreement reached in principle last Friday. Items 1-6 are structural in nature and the key principles of the agreement are set out in item 7.
I plan to get some time with Kate tomorrow if possible to review the principle of incentivising the parent company rather than Heerema Hartlepool and secondly the utilisation of a Collateral Agreement to implement the incentive scheme as this LOI sets out. This structure is designed to mitigate the risk of multiple claims by HFG under the proposed Lump Sum contract in order to protect their entitlement to the incentive in the event of us impacting or appearing to impact their ability to deliver. There is still a possibility that GasUK will be late with some deliverables and I think it best if we dealt with such a scenario from the position of benevolent flexibility on our part rather than through a formal contract claim for an Extension of Time. To date HFG have been obsessed with issuing contractual letters for every minor item and in general have been found to be baseless. This is a behaviour we do not want to encourage by the introduction of a new incentive.

