
Dear sir(s),
We refer to your email dated 20-06-2014.
From our studies we concluded that lining up the centers of the PU deck and the Seafox 2 results in the most favourable bridges installation clearances. Reference is made to attached sketch.
If you have any questions on above, please don’t hesitate to contact undersigned.

Hi Jim,
Based on your email below, SHL will perform the following studies:
-       AQU topsides installation with the Seafox 2 in position at the South West corner of PU
-       APU/AQU bridge installation from platform North with the Seafox 2 in position at the South West corner of PU
-       APU/AQU bridge installation from platform South with the Seafox 2 in position at the South West corner of PU.
We will revert with our conclusions upon completion of the studies.

Hi Eppo,
Please see attached proposal for Seafox 2 position at South West corner of PU.

Jim,
Further to yesterday’s discussions please find attached current proposal for Seafox 2 position at South West corner of PU.
The anchors will be recovered once the vessel has moved in from stand off to on station position.
Will this position give SHL an issue for the installation of UQ and West Bridge if we go on the basis of a split installation?

Hi Gordon,
Can you please give me a call?

