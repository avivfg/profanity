Pour info…

TfL advice during RMT strike action on London Underground (LU)
Update at midday:
LU is operating 50% of Tube services today – see the line-by-line information below
Two thirds of stations are open and there is a service operating on nine out of 11 lines, as planned
Around 87% of Oyster cards that would normally be seen on the TfL network have been seen this morning
A record number of buses are on London’s streets – 7,961 in total, 266 more than usual
Barclays Cycle Hire journeys are up 70% so far today
Many thousands of staff and volunteer  Travel Ambassadors from TfL support functions are out on the network working to help customers and road users
The situation may change throughout the day. TfL is urging all customers to check before they travel at www.tfl.gov.uk <http://www.tfl.gov.uk/>  and by following @TfLTravelAlerts, @TfLTrafficNews and @TfLBusAlerts on Twitter.
Bakerloo line @bakerlooline
Trains are running: 
-Queen’s Park - Elephant & Castle (every 5 minutes)
These trains will not stop at Edgware Road, Embankment, Kilburn Park, Lambeth North, Maida Vale, Paddington, Piccadilly Circus or Regent’s Park stations. It is possible that some other stations may also need to close at times.
Central line @centralline
No service is operating through the central area.  
Trains are running:
-Epping – Leytonstone (every 15 minutes)
-Hainault – Marble Arch (every 20 minutes)
-West Ruislip – North Acton (every 15 minutes)
-Ealing Broadway – White City (every 10 minutes)
These trains will not stop at Redbridge, Bethnal Green, Chancery Lane, St Paul’s or Wanstead stations. It is possible that some other stations may also need to close at times.
Circle line @circleline
See District line, Hammersmith & City line and Metropolitan line below.
District line  @districtline
Trains are running:
-Upminster – Wimbledon (every 6 minutes)
-Ealing Broadway - High Street Kensington (every 15 minutes) 
These trains will not stop at Sloane Square or Fulham Broadway stations. It is possible that some other stations may also need to close at times.
Hammersmith & City line  @hamandcityline
Trains are running:
-Hammersmith - Aldgate (every 10 minutes)
These trains will not stop at Barbican or Great Portland Street stations. It is possible that some other stations may also need to close at times.
Jubilee line  @jubileeline
Trains are running:
 -Wembley Park - Stratford (every 10 minutes)
-Stanmore – Wembley Park (every 20 minutes) 
These trains will not stop at Bermondsey, Canada Water, St. John’s Wood and Swiss Cottage stations. It is possible that some other stations may also need to close at times.
Metropolitan line  @metline
Trains are running:
-Harrow-on-the-Hill – Aldgate (every 8 minutes)
-Uxbridge - Harrow on-the-hill (every 20 minutes)
These trains will not stop at Barbican, Euston Square, Ladbroke Grove  or Great Portland Street stations. It is possible that some other stations may also need to close at times.
Northern line  @northernline
Trains are running over the whole line with good service via both the Bank and Charing Cross branches in both directions.
These trains will not stop at Angel, Borough, Chalk Farm, Embankment, Leicester Square, Mornington Crescent and Warren Street stations. It is possible that some other stations may also need to close at times.
Piccadilly line @piccadillyline
No service is  operating through the central area.  
Trains are running:
-between Acton Town and Heathrow Terminals 1,2,3 around every 10  minutes in both directions 
These trains will not stop at Heathrow Terminal 4, Heathrow Terminal 5 or Hounslow West stations. It is possible that some other stations may also need to close at times.
NB: Heathrow Express and Connect
A reduced Heathrow Express service will run on Tuesday 29 April and Wednesday 30 April due to separate strike action by the RMT. Heathrow Express plans to run two trains an hour between Heathrow and Paddington and back.
Check www.heathrowexpress.com <http://www.heathrowexpress.com/>  for more information.
Heathrow Connect services will not be operating due to this separate strike action by the RMT.
Victoria line @victorialine
Trains are running between Brixton – Walthamstow Central (every four minutes) 
These trains will not stop at Pimlico, Warren Street or Tottenham Hale stations. It is possible that some other stations may also need to close at times.
Waterloo & City line  @wlooandcityline
No service

