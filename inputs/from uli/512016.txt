FYI

Dear All,
Please find attached, update from the Fabrication Coordination Meeting between GDF/Amec & Bi-Fab.
I’ve highlighted in red, any additional comments added as a result of this week’s meeting (24/04/14)
Please can all “Responsible Discipline TA’s” & “Sub-Contractors” close out any issues shown in the Matrix before next week’s meeting on 31/04/14, specifically those denoted as “HIGH PRIORITY”
Thank you in advance.

