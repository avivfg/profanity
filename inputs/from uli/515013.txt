Fabien,
My comments on how we can realistically support the offshore campaign:
To support the 7 days a week working the following AMEC personnel:
·        Gary McCarthy
·        Chris Aldridge
·        Steve Sinclair
As you are aware this team has been on the project throughout FEED stage, Detailed Design stage and the Construction stage the knowledge they have will enable a quick turnaround of queries and full support during the offshore stage.
Once this team has completed the construction support phase they will then relocate back to AMEC Farringdon to initially start looking at the As-built documentation from construction TQ’s and FAN’s.
Once all platforms are installed the Hook-up and carry-over job carded work will commence, this will lead to a number of technical queries the amount is unknown. Therefore during this phase the proposal would be for the team to be spending 50% on as-built and 50% on technical queries.
We would propose to support the offshore phase during the weekend by being on call with the use a single mobile phone with a rota of being on call one weekend/ month.
All new study work would be carried-out using Fabricom in the exact same way as I have used them throughout the last 12 months via the RFS procedure.

Ladies, Gents
The meeting today has been longer than expected, but as explained this is the beginning of the Work on that subject, and we will continue to work together to finalise it.
The presentation I show you is available on the network:
https://portal.gdfsuezep.co.uk/cproj/Cygnus/Engineering/Forms/FolderView.aspx?RootFolder=%2fcproj%2fCygnus%2fEngineering%2fOffshore+Period
So please as discuss, could you 
·        revisit this slide pack, 
·        revert to me with your comments (this week)
·        Do not hesitate to advise proposal to fill the GAP with the current organisation in place.
Next Monday we will set together the action plan up to mid-June general Engineering Workshop
I am at your disposal to discuss about it, my doors is always open.

Ladies, Gents
I would like to discuss in more detail about the summer plan during the next Monday’s meeting. Let’s plan 1h for that purpose.
Please could you ensure that you will be available, at least by Phone/VC ?
The action we have as Engineering to prepare the summer period is:
“Ensure 7 day TA cover during offshore phase. TA (both engineering and operational) support requirements during offshore phase to be established.”
No surprise as we already discuss about it.
Currently the agreement I have with OPS team, is that you are in Front Line as Project TA, and they are in Back-up when you are off or not available.
Aberdeen TA will have to approve all deviation/Concession after Sailaway.
So agenda for the meeting:
-        Summer schedule: The Offshore campaign schedule is now almost completed, and will a good basis of discussion. I will present you quickly the current plan
-        Requirement per discipline
-        Our Schedule / “On Duty” planning
-        Day to day work. Coordination
-        Q&A
The final deliverable will be the updated of the TQ process (dead line early June), so this is probably just the first meeting on the subject…
In order to prepare the discussion, and the schedule, could you have a discussion with your counterpart in Aberdeen to share you availability? They will certainly also populate the Excel table.
I thank you for your support

All,
2015 offshore campaign has already started and activities will be ramping-up till the core campaign start Mid-July.
It is vital that all functions ensure proper coverage during 2015 offshore campaign :
·        All team member needs to update the holiday planner (Holliday Planner <https://portal.gdfsuezep.co.uk/cproj/Cygnus/_layouts/GDFSuezUK.Portal.DocumentID/GetDocumentFromID.aspx?ID=CPROJCYGNUS-26-5918&v=10&a=Default>  ). Isobel/Lauren can assist as required.
·        All manager needs to check / monitor regularly that their respective team is fully resources to provide necessary 2015 campaign support (in particular during core campaign & holiday season)
Thanks for those who are already using it

