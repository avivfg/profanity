Bonjour Maryse
Pour le vivier…
Je ne connais pas cette personne, mais il a l’air motive. Ses points forts : il parle Allemand… (Dexpro ?)
A bientôt
Cordialement

Bonjour Fabien, 
Je comprends votre démarche et je vous remercie de m'avoir contacté sur mon mail professionnel.
Je suis manager d'un groupe de technicien chez ERDF GrDF (distribution d'électricité et de gaz en France) sur Paris depuis maintenant 4 ans. 
Suite à mon expérience managériale je souhaiterai continuer à évoluer dans le secteur énergétique en restant dans des activités techniques opérationnelles et en m'orientant vers l'international. 
Malheureusement ERDF et GrDF ne prévoient pas d'étendre leurs activités à l'étranger dans les prochaines années.
Les activités d'exploration et de production de pétrole et de gaz s'inscrivent dans le cadre de mon projet professionnel en me permettant de travailler dans un domaine technique opérationnel et multiculturel sur des projets d'envergure. 
Le domaine de la production et de l'exploitation de champs m'intéresse plus particulièrement et me semble le plus approprié à mon profil d'ingénieur manager. 
J'aimerai encadrer des hommes ou mener un projet sur des activités industrielles de productions ou d'exploitation.
De réels enjeux d'efficacité et de performance sont présents et me motivent dans cette branche: développer les réserves d'ENGIE tout en maîtrisant les coûts, la sécurité et l'impact environnemental des activités. 
Je sais que le secteur fait actuellement face à des difficultés compte tenu du prix très faible du baril. Pour autant je reste persuadé que des opportunités existent au sein du groupe.
Malgré ce contexte, êtes vous actuellement à la recherche de compétences? 
Je vois d'après votre profil que vous travaillez sur le projet Cygnus, étes vous en charge de l'installation des plateformes? 
A toute fin utile je vous joins mon CV en français et en anglais: 
Cordialement,
Sébastien Bouchy
Adjoint au chef d'Agence technique clientèle
Agence Butte Aux Cailles
ERDF-GrDF
Tel fixe: 01 45 65 56 70
Portable: 06 99 50 67 26

Bonjour Sebastien 
J’ai bien reçu votre demande de contact sur Linked-In. 
Je n’ai pas répondu directement à la demande, puisque je n’accepte sur mon réseau que les personnes avec qui j’ai travaillé… 
Mais, si vous souhaitez discuter de l’E&P chez ENGIE, pas de souci. 
A noter cependant que la période actuelle n’est pas très propice à rejoindre l’E&P. Les prix bas du Baril et du Gaz nous place plutôt dans une période de recentrage. Mais cela n’est que du court terme (j’espère…) 
Bref, si vous pouviez m’envoyer un CV et vos motivations en quelques points concernant l’E&P. On pourra discuter sur ces bases… 
Bien cordialement 

