
Dear all,
Following the request some time ago, I have compared figures from Cygnus AWHP with information provided by Centrica for York platform and with information from one of our dutch platform.
The results are presented in the attached table.
This table show that Cygnus AWHP is indeed higher than other platforms in terms of structural / mechanical weight ratio, construction manhours per ton and engineering manhours per ton.
As said in earlier emails, this is a simple comparison for three installations which have similarities (NUI, wellheads, a few equipment). A more detailed benchmark with a more complete database per platform type will be worked in 2015.

