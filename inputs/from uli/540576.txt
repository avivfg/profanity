Thx Ian. What are the numbers?

> Frank,
> I can confirm that Heerema site is still within the original budget and at present do not foresee us over spending.

> Ian, Jim,
> A comment has been made in London that the site teams were only budgeted until end July -- the implication being that we've overspent the budget....
> My understanding is that the spend and demob extensions to date are within the original site team budgets in both cases....and that we still have a 'saving' of some sort in hand.
> Please confirm the position ASAP.

