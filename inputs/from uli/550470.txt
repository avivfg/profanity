I always considered we’d need 4 support guys from AMEC…..and told Radley so…2 pipers, 1 struc and 1 elec.
How do we go about getting the additional piper? Or can we use one of the follow-on guys.

Mike
During last week’s presentation with Amec regarding FOE one of the topics discussed was the piping designers’ resource and if Steve Sinclair could manage all the TQ’s coming in.
It is felt from site (Paul S and myself) that the amount of TQ’s coming in presently on PU alone are quite a lot without the BWHP which currently has only just started and ACM to start and as such we may have delays with TQ’s. (Steve S is working additional hours to keep up at this time)
We discussed the possibility of a second piping designer being made available from Amec as part of the FOE team who could assist with TQ’s as well as carry on with the FOE. Both Paul S and I agree this would be the best way to go.
This has been brought up with Fabien who says there is no budget available for an additional piping designer. (One is available and willing to come to site)
Could you if you are in agreement engage with Frank to see if any pressure can be put on this from above to make this happen?

