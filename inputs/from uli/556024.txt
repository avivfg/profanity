Thanks Frank, 
Appreciate you clarifying the below

Thanks for the note Ruth.
After speaking with those involved, I can confirm that inputting of manifest details to e-cargo is the responsibility of GDF Materials/Logistics personnel in Aberdeen (Jim/Emily). 
The lapse last weekend should not have happened. 
I am assured that appropriate arrangements have now been made to ensure that this does not recur.
If LV experience any further problems in this area, please inform myself and Tony Buckle.

Hi Frank, 
As discussed this morning I think that there were some frustrations at the weekend due to people not knowing who was responsible for producing manifests etc.
Tony obviously creates his packing manifest which is sent via e-mail to Aberdeen Office / Cygnus Materials Lead – this information is then fed into e-cargo by Petersons I believe
LV create our own basic manifest to assist the quayside in loading / discharging – giving them only the information that they require to complete this task.
If the vessel calls into Aberdeen then Petersons would normally create a manifest for the port call to include any containers sent up from / loaded at Middlesbrough – I assume that as the vessel didn’t call into Aberdeen this week that the assumption was that they didn’t need to complete this task.
If the manifest needs to be completed in e-cargo then either we would need to be given access to this system or Petersons need to be made aware that even when the vessel calls into Middlesbrough then they have to complete the manifest.  As mentioned either way works for us but just need to know for future port calls the procedure we are working to so as to avoid confusion.
Hope the above is clear – if not give me a call

