Any update

Hi Richard, 
Just wanted to clarify.  I am shipping this to address below, correct? 
Kindly, 

Apologies – documents now attached. 

Frances, 
Please find attached our confirmation GDF SUEZ P.O. No. 4000001042 for a Replacement (Operational Spare) Chlorination Dosing Pump for the Reverse Osmosis Package Ref. Offshore Request CYG.COMM/0364.   Your emailed quotation and clarification of Parker Hannifin Part No. 70-0810 refers. 
Please accept my apologies for the delay of a few days in placing this Purchase Order – this is because of a high number of requisitions received from offshore for urgently required Hook-Up material. 
This particular part is going back into the GDF SUEZ Operational Stock at Waterloo Quay in Aberdeen so could we ask you to mark it and the accompanying documentation the Material Master No.41002339. 

Hi Richard, 
PN: 70-0810 
Your cost:  $  4,142.53 
Thank you so much for your patience and Happy Holidays. 
Kindly, 

Frances, 
Thanks very much – great support. 

Hi Richard, 
Per our engineering team the correct PN for the dosing pump is 70-0810.  It is currently being quoted by our pricing team.  I will update you with a quote tomorrow 12/17. 
Kindly, 

Frances, 
I referred your query regarding the Part No. to Offshore and they have sent me the attached.  Perhaps Amir or Oscar can assist on the technical side in sorting out the Part No. required. 

Hi Dick, 
See Malika’s response below 

Hi, 
The item we need is Dosing Pump, Chlorination, .155 TUBE, 4-20mA, Supplier’s Part No. 70-0810, Real manufacturer’s Part No. A2V2X-MNF-3. 
See item 7 in the attached spare list. 

Hi Malika, 
See below, Can you advise please 

John, 
Parker Hannifin in the U.S.A. do not recognize the part no. quoted on our requisition – can you please check and advise. 

Hi Richard, 
Please double check the part number.   
The part number you provided below is not in our system. 
Kindly, 

Frances, 
Could you please send me a quotation for a spare Dosing Pump (Parker Part No 41002339 ) and as per attached Offshore Requisition. 

Dick, 
Please find attached OMR CYG / COMM / 0364 for Purchase action. 
This is to replenish Ops Spare of the Chlorination Pump for the RO Package, to be delivered to the Aberdeen Operations Base. 

