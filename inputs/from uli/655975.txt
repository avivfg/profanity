All,
An update on the temp Nav aids.
Item
Position
Status
Action
AUQ spider Deck NW
No Access due to incomplete handrails
Rope access to assist in status survey
APU spider deck N
Non-functioning, no access due to caisson installation
Remove unit once accessible
AWH Cellar deck NW
Functioning 
No action offshore
AWH Cellar deck NE
Functioning 
No action offshore
AWH Cellar deck SW
Functioning 
No action offshore
AWH Cellar deck SE
Functioning 
No action offshore
APU spider deck S
Functioning
To be relocated to SE cellar deck
AUQ spider Deck SW
Non-functioning. Lights flashes upon power cycle but does not function when photo cell is covered
Remove unit for repair.
I will raise the request to AMEC HU to assist with the retrieval of units  2 & 8, along with the relocation of unit 7.
Whilst accessing unit 8, we will test unit 1 for functionality.

The relocation of the sub light was supposed to be actioned about 10 days ago and I am still awaiting confirmation if the propsed temp white light unit fir awhp is acceptable 
Can we clarify tomorrow morning during 0900 conf call

Hello Ian,
Matt Plummer will be dealing with this one.

Please see attached the notification of the survey of aids to navigation.  There are still some follow up actions to address the issues we have currently with our temporary lights across the complex:
·         Moving the spare secondary red light from the south side of the APU to the north side of the APU to replace the defective red light on the north side.  Please provide a status update.
·         The secondary red lights on the AUQ were confirmed as not functioning.  What actions are being taken to bring these back into service and when is this likely to be completed?  If this is not going to be in the next two weeks I will need to apply for a dispensation for these lights.
·         Gordon Thom to contact Nick Dodson of Trinity House to discuss the requirement and potential dispensation for a white light on the AWHP.
·         Confirm that the four red secondary lights on the AWHP are functioning.
It is important that these actions are followed up to bring is into compliance with our consent to locate prior to the survey.  I would therefore be grateful if you could provide a response to the points above as soon as possible. 

Dears Sirs,
PLEASE ENSURE THIS INFORMATION IS CIRCCULATED ACCORDINGLY WITHIN YOUR ORGANISATATION
Please be advised that the Trinity House Lighthouse Services intend to commence the annual inspection of aids to navigation on offshore structures and wellheads on or around Monday 16th November 2015.
I would be grateful if you could alert your installations to these forthcoming inspections.

