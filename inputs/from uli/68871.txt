Jim
FYI
It is likely that a lot of the sign qtys on the AUQ Jobcards may have already been installed at site hence I expect Tony will not be able to satisfy the jobcard line item qtys 100% - subject to what has been installed.
I had previously asked Methil for a as installed survey of the signs installed on the AUQ, surprise surprise no response.
Going forward and as per attached – detailed sign installation surveys were carried out at Hartlepool with marked up plot plans per module.

Tony
Please see the attached call off for Job cards 22039 & 22047. Can you send on the NAV please. These are safety signs that are similar to previous call off 00025. Can you ensure that when all these safety signs are manifested, they have the appropriate job card included please.
Cheers
Jim

