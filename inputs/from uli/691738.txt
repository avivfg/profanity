Tony
Please raise req for 60 off 1 ¼” x 175mm studbolts to satisfy the following Jobcards
Dick as a heads up we have never purchased this length of 1 ¼” dia studbolts previously so no PB code – can you please get lead time from Steadfast
AUQ
0509-182-FS00-WPK-002
Install 24" Top Hat 048-P001-A.
AUQ-C3-3 S1
1 1/4IN 175LONG STUD BOLTS ASTM A193 B7 COMPLETE WITH NUT ASTM A194 2H
AUQ
0509-182-FS00-WPK-002
Install 24" Top Hat 048-P001-B.
AUQ-C3-3 S1
1 1/4IN 175LONG STUD BOLTS ASTM A193 B7 COMPLETE WITH NUT ASTM A194 2H
APU
0509-182-FS31-WPK-001
Remove Lift blind & install top hat Prod.Water Caisson  
AUP-C2 S2
1 1/4in X 175 LONG STUDBOLT (ASTM A193 B7) COMPLETE WITH NUTS (ASTM A194 2H)

Gents,
The Bolts are too long to be used in lieu therefore can we arrange purchase with best expedient lead time.
Please ship the Gaskets NAV to SF2.

Guys the bolts are required for the Top Hat Covers of the Caissons i.e. not piping flange connections
There are 70 off 1 ¼ x 215mm studbolts in stock – would these over length bolts be acceptable in lieu of 175mm long bolts??
PB02093
1.1/4" Dia * 215.0mm Lg Studbolt [AS] ASTM A320-L7 HDG, Nuts ASTM A194-4 HDG

24” SP WD gaskets only available, No bolts

Hi Gary
Yes they are permanent and should be gdf supply
Tony
We can double check tomorrow against piping stocks we had this issue during the module builds where the design struccies did not use the piping codes for bolts and gaskets on the caisson scopes

Tony,
Did you have a look at availability for these items? can you confirm availability.
Gordon – these items were not included on the HC BOM’s therefore not included on FSA which was only identified after receipt of items for this scope . Am I correct in assuming that as these are permanent installed items, these should be supplied by GDF either from Stock or if not available would require purchase by GDF ? 
See attached copy email, Amec are trying to purchase ?

Marti
Can you view the attachment and advise of the availability of this material. This work scope is now at the stage where these bolts are required urgently. Can we have then on the NAV please.
Jim

