No - I have problems with Chris – did a good job on BritSats – just haven’t seen him for a few years 

Is he OK ? I do believe that another pair of hands will be an asset Gordon but it needs to be the right person and if he is not then I will get someone who is.

Yes – from a few years ago on BritSats – He was back to back with Jim McGee offshore as Materials Lead, then he took opportunity to go over to Calgary with Phil Cairns and others

I think so mate, I will get you his CV. Do you know him ?

Chris Kirk – ex Calgary??

Hi Frank,
Apologies I got the name wrong, its Chris Kirk. Language barrier as Marti is a Geordie. 

Gordon, Tony,
Frank Stafford has confirmed that an AMEC materials controller (Chris Kerr) will be based short-term at HUB Middlesbrough from  Monday next to identify the materials that remain unidentified from the sites, e.g. pipe supports with no numbers as noted below. 
He also needs to address the issue previously identified by you -- i.e. Job Cards which obviously require materials, but have no materials listed.
The above exercise will assist the HUB team in filling the information gaps which currently exist and are causing extra work. 
We need to get to the point ASAP where all Job Cards correctly identify the materials required, and all materials items carry identification numbers/references.

Gordon,
Spoke to Mick Oxley. He found what he was looking for (just like Bono…). 
A few pipe supports. Problem seems to be pipe supports shipped here in bulk did not carry ID. He found them using the drawings he brought with him.
Seems to me we need to have someone go round with Job Pack and drawing numbers and identify the rest of them.
Whether it should have been done at site … now history.
Will leave to you and Tony to plan the next step? Let me know if you need my input.

Gents
All I can say is one maybe two items for reinstatement materials might go missing but the amount of anomalies we are getting from offshore seems too big and as we know Heerema installed and removed these items they must be available somewhere.
AmecFW had a task to tag in the yard for job carding now for whatever reason, last minuet rush / removal from APU prior to sail away or whatever and I don’t care, we need to locate them and get them offshore if it has been determined 100% that they are not already there and if not we will need to refabricate ASAP.
The goal to get the job over the line and we have discussed many times to place a member of AmecFW in Middlesbrough for reason such as this, Mark was requested to come up with proposal for this, did you do that Mark?
In the meantime let’s look today with Mick to see if we can establish if what we want is at HUC base or not.

Gordon/Tony
I’m not going to get involved in a ‘you done this we done that etc etc’ email spat…it’s not going to get the materials offshore and I apologise if my email has been interpreted the wrong way as it was not intended to criticise anyone. It was merely offer of assistance to locate materials that cannot be found.
However I am going to state some facts.
·        Reinstatement materials required offshore.
·        Offshore have tried to locate offshore but have come to the conclusion that they are not there.
·        Materials requested from HU base onshore however cannot be located there either.
·        Offshore have offered to go into HU base to try and locate…this request has now been deemed as unjust and counter productive?
I do not know where we go from here as every time we send a material request we are then subject to emails picking holes in jobcarding, FSA etc etc.
My suggestion is that we send a resource into the HU base with whom the HU team can communicate with on a daily basis and should be able alleviate any issues with jobcards, FSA etc, etc.
Please advise when, if in agreement, we can mobilise a resource to the HU base.

We have very little structural items left here now and the below term as cannot locate is unjustified as Gordon has mentioned.
70% of the structural items (and a large amount of the piping items) transferred from site were transferred with no JC numbers on or advised as JC TBA, also they were not itemised and were given a one line description which makes identifying items difficult.
Example of AMEC packing list for items transferred, these should have been fully identified by DWG / items / spool / Part No
Job Card 
ITEM/ Description
QTY
2 NO VALVES 
FRP RED GRATING bag of fittings 
2"-11/2"-3/4 SPOOLS 
1/2 spool c/w stud bolts 
uv steriliser gauge 
spool c/w stud bolts 
1/2" spool c/w studs
TBC
10" fire water spool
TBC
A12012 S-3700 ITEM 1+2 
TBC
FLOWLINE A12012,S/N 5761
TBC
FLOWLINE A12012,S/N 5762
TBC
FLOWLINE X 2 NO IDENTIFICATION
TBC
DUMMY SPOOLS X 2
Outside of the above Job card numbers have been changed since items were transferred, this has all been picked up by me by spending many hrs going through your FSA report, also on the FSA items are showing as
Issued – when they have been back loaded
Not issued – when they have been issued
Material location for install items as been HU Base supply when they should be CPS supply
Material shown as Re-instatement items as CPS supply when they should be HUB supply
Carry over items shown as CPS supply when they should be HUB supply
Incorrect codes for materials
Quantity Discrepancies, especially on E&I tray / rack / lighting  – which Gordon has requested to be clarified numerous times
Clarity given for RED items on the FSA not been identified
Amec made the decision to remove the HU team from the HFG yard when the JC scope was only 50-60% complete and left only 2 MC’s behind who had to carry out site transfers to the HU base, as a consequence JC items were mopped up and transferred
with the surplus from the site – over 200 + articulated vehicles.
We will endeavour to work alongside Mick Oxley and provide any help required as we did previously to resolve any material issues

Kenny, thanks for the response.
Please clarify your comments regarding ".........the personnel on site cannot locate them" as I feel these are unwarranted and counter productive.
As we are all aware, the structural items transferred from Hartlepool were supposed to have been all marked up and identified with jobcard refs but this was not, in certain instances the case, hence the current situation.
Taking aside the various comments / inferences and in order to move forward on a One Team basis, Mr Oxley will be provided with the necessary support tomorrow by Tony and his Team and I expect the majority of the current queries on shortfall materials for the 6WLA will be resolved or clarified.
Can you please ensure Mr Oxley has appropriate PPE with him for the visit.

Gordon
Mick has demobbed today after a 2 week trip and has kindly offered to spend a few hours tomorrow at the warehouse looking for reinstatement materials which are not offshore and should be sitting in the warehouse however the personnel onsite cannot locate them. He will not be going through the full FSA at this time but if he does find the material it does highlight that we should mobilise a resource to the warehouse to complete a check on the full FSA.
Let’s see how Mick gets on tomorrow and then we can discuss a further visit to complete a check on the full FSA.
Regards
Kenny

This is ok providing Mick has a detailed scope to review as far as jobcard materials are concerned and completes for the full FSA
The previous visit last year was for approx 2 hours and was incomplete

Gordon
Tried call you however went onto voicemail.
Mick Oxley can visit the Middlesbrough warehouse tomorrow to identify reinstatement materials which are required offshore and showing unavailable on FSA
Can you confirm this is ok?
Thanks

