Ok thanks for the update
Setti – please arrange uplift the 2 x pallets on Tuesday for delivery into Keppel on Thursday 23rd, and once confirmed via David the 2 or 3 boxes on Thursday for either pm delivery Friday / am delivery Saturday

All,
We are just shrink wrapping the pallets now,  of which there are two.
These will be ready for collection from Tomorrow Morning, we will then have a further 2 or 3 boxes of isolation certs which will be ready for collection sometime Thursday. 
So:
Tuesday collection – 2 x Pallets
Thursday collection – 2 or 3 boxes 
The last message I got from Commissioning was to forget about shipping with them as they would not have anything ready for tomorrow.

No
David – where are you with all the Ops Docs and other stuff for Seafox 2 – can these isolation folders be included in the main consignment, which hopefully will be tied in with the commissioning docs ex Hartlepool

Gordon,
So, collection from David Thursday 23rd July and delivery to Keppel yard Tuesday pm 28th July.
Is this correct? 

Setti – please arrange Tuesday pm

David,
Please see attached commercial invoice / packing list covering the Isolation certificates which we are to deliver to Seafox 2.
Allocated MMT number is 1001 –  you need to add the dims and weight of the each box to the attached document once information is available. 
Gordon, when the boxes need to be delivered to Keppel yard? 

Yes no probs the same way we have been having to send blank certs back to Methil and Teesside - we shall request LV to courier to Keppel.
Presume tgese will be fao Grant Bateman?
Setti on Monday please send David the profirma shipping docs and delivery address for Keppel. I
Sent from my iPhone apologies for any typo's

Gordon,
We will have two boxes of folders c/w isolation certificates etc which we require in the SF2 permit office but these boxes will not be ready until Thursday.
Is there a way we can ship them after Tuesdays collection, so that they reach the SF2 in time…?

