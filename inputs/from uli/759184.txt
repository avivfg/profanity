Gents,
Please add / comment / suggest anything you think I’ve missed or you think is inaccurate.
Attendees:-
Steve Newmarch             (GdF)
Ian Taggart                          (GdF)
Steve Humble                   (HFG)
Lee Tyas                               (HFG)
Jim Kerr                                (HFG)
Neal Waby                          (HFG) (Part time)
Absentees:-
Graham Windebank        (HFG)
·         Universal definitive list of Surplus / Shortages received. Expediting Report needs explanation and quantities to ascertain whether the shortages are not covered by the existing expedited materials.
·         12” 34.93mm wt Pipe has been received at Site and Fabricators. Item 56 on the Shortage list is now in error and can be removed.
·         Materials for HU is now ongoing. Batch one has been sent, batch two is being loaded onto transit, batch three will follow.
·         All Material transfers from / to RBC are now complete.
·         10 welders were requisitioned to be sent to RBC to assist in the remaining fabrication. 5 welders are still in attendance. RBC have cancelled the requisition of welders for nightshift to carry out the fill and cap, they have started 4 Mig welders of their own which will suffice.
·         Liam Buckle is to receive this weeks Erection Material Shortage Report from GDF (Fabricom) for comment / material allocation, still not received, Neal Wabey to supply.
·         12” Barred Tee is still on HOLD pending further information.
·         There are 9 Bridge Isometrics still outstanding and 1 Bravo Isometric still to be received by HFG.
·         There are modifications to 37 ACM Support Drawings to be produced by AMEC, Issued via CCN and FCN.
·         Request for GDF to Respond to A12012-TQ-L-0656, Supports in the LER / Switch Rooms that are lacking Unistrut Supports in that area. Response has been recorded, will be with HFG DO later this week. In essence it is not practical to support from the internal or external corrugated sheet walls. HVAC Unistrut Support needs to be found. Supports welded to the steel floor is acceptable with the correct foot plates. Individual cases should continue to be queried.
·         Pipe Testing is to begin in earnest 3rd week in November, however there are a number of Tests completed, and a number of Tests that can be done now, that could be used to pave the way for Re-instatement, ITA Reporting methods and Paperwork.
·         HFG enquired in the meeting regarding the TSA Coating of the In-Deck Drains Tanks. Is there  a large overlap where dissimilar welding occurs, particularly around the Piping Nozzles. GDF to respond, and compare what occurred on Alpha Well Head Platform.
·         HFG DO are still to Report why they have received 1710 Isometrics but the Isometric Spool Tracker only has 1708.
·         End of Report

Gents,
Please add / comment / suggest anything you think I’ve missed or you think is inaccurate.
Attendees:-
Steve Newmarch             (GdF)
Ian Taggart                          (GdF)
Steve Humble                   (HFG)
Lee Tyas                               (HFG)
Tony Buckle                        (AMEC)
Jim Kerr                                (HFG)
Neal Waby                          (HFG)
Absentees:-
Graham Windebank        (HFG)
.      Jim Kerr (HFG) is to provide Tony Buckle (AMEC) a universal definitive list of Surplus and Shortages from all Fabricators by Wednesday morning at the latest. COMPLETE
.      HFG have changed the Task for Bagging of known Surplus Materials, the Materials known to be reallocated are to be removed by 4th November at the latest. True Surplus Materials will remain on RBC shelves until reconciliation at the end of the RBC contract, although Lee is attempting to get these materials back to HHL yard sooner. COMPLETE
.      Two of the random lengths of 12”nb Pipe being delivered on Wednesday is to be redirected to RBC by Thursday 30th October. Tony Buckle is to inform Lee Tyas of the correct delivery address. COMPLETE
.      10 Welders were requisitioned to provide shortfall of manpower at RBC, the number sent has increased to 6, however one handed in his notice. In addition, 5 welders are to be sent for the task of filling and capping the heavy wall large bore pipes on night shift. This is to commence Tuesday  29th October. COMPLETE
.      Manpower allocated to Stores at Greenland is:-                
o   11 Store men; (increased by 2)
o   4 Office Staff; COMPLETE
.      Current Shortage Reports from Fabricators are:-               
o   RBC = 15 Isometric with Genuine Shortages for PU; COMPLETE
.      Current Shortage Reports for Erection Materials is:-
o   176 Isometric Shortage (Info Supplied by Fabricom). Tony Buckle (AMEC) was to receive a list and advise, Tony Buckle is still waiting for the list.
.      Lee Tyas requested a comprehensive list and ‘On Site’ dates for Materials and Items Procured by GDF (AMEC) for PU and ACM, Tony Buckle to provide.
.      The 12” Barred Tee is on HOLD pending further information. The 24” Barred Tee is not included in the HOLD request.
.      HFG Drawing Office (DO) provided a list of Support Drawings still on HOLD due to FAN’s. The DO is now to investigate whether these FAN’s have been cleared. COMPLETE
.      HFG DO is still to investigate why the DO has received 1710 Isometric whilst the Isometric Tracker has 1708 Isometrics to Fabricate.
.      A meeting is required so that HFG DO can discuss with GDF the philosophy of the 2nd Fix drawing rectification exercise. HFG DO are claiming they are finding more and more problems, Task definition and boundaries need to be redefined. The Meeting is to be scheduled when Ian Watson is on Site later this week. COMPLETE
.      HFG DO are to highlight to GDF (AMEC) problems identified for the Flare Stack Supports. Details of the Supports, not in contact with Steelwork, is to be defined. COMPLETE
.      GDF are concerned that the Isometric Tracker is not being updated correctly. It is known that Spools are being delivered to Site, but the Tracker still shows zero progress. Lee Tyas to investigate. COMPLETE
.      GDF highlighted in the meeting that a ROSP card was submitted for a Spool that had fallen from one level of Pipe Rack to a lower Pipe Rack. GDF then suggested that there is an increasing amount of Spools seemingly abandoned on Scaffolding. Examples of which have been submitted to GDF Engineers, but the Spools have not been moved. Lee Tyas responded by saying that HFG will remove spools if stored unsafely but otherwise will not be wasting time and effort clearing Spools back to the compound. COMPLETE
.      Supports for the small bore Piping in the LER / Switch Rooms is to commence by the end of this week. GDF Engineer is to assist the HFG Engineer with a walk round the rooms to ascertain whether there is ample Unistrut for the task, and determine best solution. COMPLETE
.       Pipe Testing is to begin in 3rd week in November. Some Test Loops are ready now and a trail of systems can be done prior to the November commence date to pave the way for ITA reporting methods and paperwork. 

Gents,
Please add / comment / suggest anything you think I’ve missed or you think is inaccurate.
Attendees:-
Steve Newmarch             (GdF)
Ian Taggart                          (GdF)
Steve Humble                   (HFG)
Lee Tyas                               (HFG)
Tony Buckle                        (AMEC)
Jim Kerr                                (HFG)
Absentees:-
Neal Waby                          (HFG)
Graham Windebank        (HFG)
.      Jim Kerr (HFG) is to provide Tony Buckle (AMEC) a definitive list of Surplus and Shortages from RBC by Wednesday morning at the latest. COMPLETE
.      Bagging of known Surplus Materials has begun at RBC ahead of the definitive list, all materials to be removed by 4th November at the latest. COMPLETE
.      Lee Tyas is to Site Visit RBC on Wednesday to oversee the allocation of Materials. COMPLETE
.      10 Welders were requisitioned to provide shortfall of manpower at RBC, 4 have been sent, it is not known this time whether the other 6 will be sent. COMPLETE
.      Manpower allocated to Stores at Greenland is:-                
o   9 Store men;
o   4 Office Staff; COMPLETE
.      Current Shortage Reports from Fabricators are:-               
o   JN = 23 Isometrics with Shortages for ACM;
o   JN = 3 Isometrics with Shortages for PU;
o   RBC = 18 Isometric with Shortages for PU;
o   RBC = 19 Isometrics with Lost Materials for PU;
o   RBC = TBC Isometrics with Shortages for ACM; COMPLETE
.      Current Shortage Reports for Erection Materials is:-
o   161 Isometric Shortage (Info Supplied by Fabricom). Tony Buckle (AMEC) to receive the list and advise. 
.      Lee Tyas requested ‘On Site’ dates for Flanges and Fittings that are outstanding for PU and ACM, Tony Buckle to provide.
.      A ‘HOLD’ form is to be Issued from HFG Drawing Office as requested by Steve Newmarch (GdF) ASAP to prevent any further welding of the ‘Barred Tee’.
.      HFG Drawing Office (DO) is to consolidate Isometric lists to ascertain why 1707 Isometrics have been Issued by AMEC and 1710 have been received (see DO weekly Report #58).

