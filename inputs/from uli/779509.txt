Vicky
Correct, all the equipment is required for the operational phase. The davits as you say, should be fitted onshore if possible.
The trolleys, electric tugs and spider cranes (if delivery & time permit) will go to the yard for trialling and training purposes.

Proph / Mac
I believe that of all the items on the following requisition the only ones to be installed/trial fitted onshore are the :
-          Trolleys – will be trial fitted at the vendors and then brought to the yards for testing prior to Sailaway. 
-          Davits? I believe these are like Jib Cranes – do they need to be fitted onshore? Will they be used during the HUC phase?
The rest of it looks like they will only be required in the operational phase…can you confirm?
Thanks
V

Vic
Rev F04 attached, issued today.

Hi Stuart ,
Please could you scan me a copy of the Mechanical handling Requisition – lost the hard copy you gave me last week 
Many thanks,

