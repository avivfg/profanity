Thanks Matthew and Brendan for all your hard work over the weekend.
Gabrielle Koonin | GDF SUEZ E&P UK | 40 Holborn Viaduct | London | EC1N 2PB | Tel: +44 (0)20 3122 1402 | Mobile: +44 (0)7515 199 961 | Email:  <mailto:gabrielle.koonin@gdfsuezep.co.uk> gabrielle.koonin@gdfsuezep.co.uk | Web: www.gdfsuezep.co.uk

Thanks for the move and the update Matthew.
I will let the guys know about the printers…

And to update the update…: J
I spoke with AMEC support this morning and they’ve patched each of the desks now. Each moved user will now have LAN data and Voice. I spoke with Duncan Hutton this morning and confirmed this. 
The printer is online but requires additional network configuration before it can be accessed; rest assured it’s being treated as a priority. Until that time, the moved users can use either of the two printers on the lower mezzanine, with a communication to be sent out to that effect, 

Thanks for the Update Mat

Evening All, 
The office move is completed with all desks setup. 
There are outstanding issues with the network (phone and data) which I will I’ll be discussing with AMEC network support in the morning. 
All laptops will be able to connect to GaS UK Internal Wifi automatically so the guys can continue working. LAN data and voice will be looked into as a priority. 

