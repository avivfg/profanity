We are pleased to inform you that your incident 135174 has been Resolved and your Service should now be operating normally.  
If you believe this is incorrect, please contact the Service Desk. If we do not hear from you within 3 working days, this Incident will be Closed.
A summary of your call:
Incident: 135174
Description: Citrix receiver not logging in please investigate

