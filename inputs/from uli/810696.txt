Ian
How are you doing with the PU “structural” TQ-1181 about bungs / bunds on PU weather deck??
Cheers
R

FOR LONDON ONLY
Summary of OVERDUE TQs below
2 Overdue Electrical TQs
10 Overdue Instrument TQs - THESE INSTRUMENT TQs DATE AS FAR BACK AS FEBRUARY AND REQUIRE IMMEDIATE ATTENTION!
1 Overdue Structural TQ
A12010 – General
0 – Overdue 
0 – Due next week
A12011 – AWHP
0 - Outstanding
A12012 – APU
8 – Overdue – 2 ELECTRICAL, 5 INSTRUMENT, 1 STRUCTURAL
0 – Due this week
6 - Due next week
A12012 – Bridges
0 - Outstanding
A12014 – BWHP
5 – Overdue – 5 INSTRUMENT
0 – Due this week
0 - Due next week
A12015 – ACM
0 – Overdue 
0 – Due next week
Please find the TQ register by following the link below: 
https://portal.gdfsuezep.co.uk/documents/repository/_layouts/GDFSuezUK.Portal.DocumentID/GetDocumentFromID.aspx?ID=11111-8-70923 <https://portal.gdfsuezep.co.uk/documents/repository/_layouts/GDFSuezUK.Portal.DocumentID/GetDocumentFromID.aspx?ID=11111-8-70923&v=154&a=Default> &v=154&a=Default

All
Summary of OVERDUE TQs below
6 Overdue Electrical TQs
20 Overdue Instrument TQs - THESE INSTRUMENT TQs DATE  AS FAR BACK AS FEBRUARY AND REQUIRE IMMEDIATE ATTENTION!
0 Overdue Piping TQ.
1 Overdue Structural TQ
1 Coatings
1 HVAC
Can you all please review the attachment and if you are not in possession of the TQs for your discipline please request them from Paul Marley (PU and ACM) or Paul Sayer (BWHP, General and Bridges)?
A12010 – General
0 – Overdue 
0 – Due next week
A12011 – AWHP
0 - Outstanding
A12012 – APU
13 – Overdue – 3 ELECTRICAL, 9 INSTRUMENT, 1 STRUCTURAL
10 – Due this week
11 - Due next week
A12012 – Bridges
0 - Outstanding
A12014 – BWHP
16 – Overdue – 11 INSTRUMENT, 3 ELECTRICAL, 1 HVAC & 1 COATINGS
13  – Due this week
2 - Due next week
A12015 – ACM
0 – Overdue 
1 – Due next week

