Ian,
No worries I’ll get in touch. Thanks again for booking the trip – was a great time. Really enjoyed it.

Mike,
Looks like the lesson still needs to be paid for.

Hi Ian,
Hope you had a safe trip home, good to have a chat on the way down the hill and i will definitely put you on my mailing list for Freedom Velo!
Please find attached an invoice for Mike Carr. Unfortunately the need to take payment got overlooked by Emily in the confusion as to whether Mike was having further lessons or not. 
Anyway, would you please be as kind as to pass this on to Mike and ask him to contact us by phone or mail with his credit card details to enable us to take payment for his ski lesson with ESF.
Alternatively if Mike supplies his phone number i will happily call him to take down the payment details.

