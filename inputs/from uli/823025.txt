Paul / Ian
There is an additional piece to this you may or may not know about.
During Georges review, he identified that there is also an issue w.r.t. the relative weir heights.
The ‘tube’ extension mentioned below is the fix to this second issue (not directly related to the level instrument issue).
i.e. the tank may not have worked at all without this second fix.
Ray Hillard has been dealing with this also.

Ian,
This is taking too long can you chase George to get an answer.

Paul,
Update on this issue.

Ian,
I did speak to Instruments and we are still waiting for the vendor to advise solution (hopefully extend tube on site to cater for addition of reducer above 6” flange of N4A).

Simon/George,
How was this left we talked about the reducing flange but that would mean the instrument would be short and George was going to speak with instruments I think. 
Any update?

Ian/Chris ,
Did this get resolved.?????

fyi

Mike / George
Cheers for looking at this.
My challenge is, how much range do we loose if we keep the existing instrument?
Given that this is the oil/water interface level instrument and there should always be a level of water up to the water outlet siphon breaking hole, do we really need a new instrument?
Also, I will follow up with Ian W to see if there is a smarter way we can mate the 4” instrument flange to a 6” tank flange without the use of a short spool.

Chris,
Please find attached the comment from George Curati which seems a sensible way forward. Not sure if George has spoken to you directly, but I imagine it may be fairly straight forward to obtain a longer instrument probe and use the existing 6” boroscope nozzle (accepting that future inspections would require temporaneously removing the probe when an inspection is required.
If we are agreed then we can get the P&IDs amended.

Mike,
The tank drg is by Structural and they made a mistake. The tank is made and probably internally coated so a new nozzle is not the best option.  We could use the 6” boroscope nozzle with a reducer for the 4” instrument. Unfortunately this means changing the level instrument probe for a longer one due to the addition of the reducer.

George,
Do you have any comments on Chris’s note?
I have internet access but I struggle to have access to the Vendor Drawings while I work remotely.

Gents,
During writing of the Commissioning Procedures for the BWHP Non-Hazardous Open Drains System, Craig Tees identified a probable design / fabrication issue which needs investigation and resolution.
The BWHP Non-Haz Open Drains Tank is an in-deck tank which has two compartments; one for oil and one for water.
The water ‘underflows’ and discharges in to the sea. The oil ‘overflows’ into the oil compartment.
There is no oil pump-out, manual intervention is required.
Process Engineering specified a level instrument for each compartment so that the liquid levels can be monitored to ensure correct operation.
Looking at project drawing  CFBW-32-AJ-23-00046 (tank GA) it would seem that there are two nozzles located in the wrong compartment.
Nozzles N5 and K2 should be on the water side. They look to be currently located in the oil side.
Nozzle N5 is the ‘utility’ connection – we can probably live with this as-is.
Nozzle K2 should be the water side (interface) liquid level – K2 is shown to be located in the oil compartment (where there should be no water).
This latter item is problematic – the instrument will not function as designed.
I think this needs a dose of looking at to see if this in fact reflects the current design / situation. 
If it does, we have to change something.
My first thoughts would be to either: 
·         Provide a new 4” nozzle on the top of the water side of the tank to accommodate LI-006.
Or
·         Install LI-006 on nozzle N4A via a suitable adaptor (6” nozzle – 4” instrument)
Either way, if the current GA is correct, we need a modification.
I am not sure who the owner of this issue should be, so I have included a few on the distribution.
I will probably copy this e-mail content into a TQ to formally record it.

