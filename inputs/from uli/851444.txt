Hi Scott,
We tend to work to the Galperti recommended bolt load, and not their torque values. The reason is that there are various calculations out there that can be used to derive a torque value from a required load. The Galperti documentation does not say which torque calculation it uses, and I’ve a feeling it could be a simplistic one or very generic one (it may even use ‘K’ factors instead of COF’s). The one that we use is a long form equation that we have used for many years and are comfortable with. The important aspect is the bolt load though.
I suspect that PNS has just followed the torque value Galperti say, irrespective of the bolt load relationship.
I’ve had conversations with Galperti in the past and they don’t appear too concerned about torque value descrepancies as the clamps tend not rely upon bolt load for sealing performance.
For the 6” XF52  -  4 x 1-1/4” bolts,  rec tension – 103kN, max tension – 152kN  (Bolt Stress, rec - 169 N/mm2, max -  249 N/mm)
And for the 24” X24  -  4 x 2-1/4” bolts,  rec tension – 398kN, max tension – 583kN (Bolt Stress, rec - 179 N/mm2, max -  262 N/mm)
Updated table attached…

Hi David,
I have been recently reviewing the torque figures currently being used for Galperti hubs on the Cygnus project, these were left to myself by Mark Beadnell whom I believe was in communication with yourself. I have found a few emails from Mark to yourself discussing figures but there are some gaps and I was wondering if you could just review the information I have at present to make sure the figures we are currently using on the project are correct.
I have been digging through Marks files and have found a sheet with the figures he is using,  a Galperti Clamps reference document and also the figures previously being used by PNS. I have gathered this information into a spreadsheet and highlighted the column headers in Blue for your reference (attached).
My main query is with the figures that have a small difference in the bolt stresses but yet the torque figures are quite different e.g. the 2” and 3” clamps. There are also 2 clamps that I could not find torque values for (6” XF52 & 24” X24).
I have attached also attached the Galperti reference document from which I got the ‘recommended’ figures and a word file with an example of the 2” clamp figures.
If you could have a quick review of the figures and let me know what you think that would be great. 

