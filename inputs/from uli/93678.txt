Mustafa,
Bob Conder is currently in discussion with both Ian and Chris Hamilton regarding the requirements as per the design drawings. As soon as hear anything I'll let you know.

Hi Tam,
Have there been discussions on the e-mail below since it was sent out?
Do I need to involve AMEC for any of this scope? I.e. do we need any jobcards?
Also, have you had a chance to look at any of the other ITCs I had sent previously for APU/ACM?

Ian,
Yeah I can remember the email and have looked at it in detail, however it's not clear from the P&ID's whether the vessels actually need Insulation and the extent, however we can pick this up on Monday to discuss. Would probably be best to get Chris into the conversation as time is marching on and we need to decide what scope these fall into.

Tam,
Please find attached an email from last year which identifies the equipment that requires insulation, I am sure they all require 50mm thick insulation but we will check on Monday.

Ian,
I've had a chance to look over Russell's report regarding the vessel Insulation requirements as per the ITC and have the following questions.
024-V003- Not currently in the scope. What's the requirement Insulation wise i.e. the extent and what scope should it appear in.
035-V002- Not currently in the scope. What's the requirement Insulation wise i.e. the extent. On Bravo.
020-V005-  Currently in the Winterisation scope.
020-V007- Not currently in the scope. What's the requirement Insulation wise i.e. the extent and what scope should it appear in.
026A-V001- Not currently in the scope. What's the requirement Insulation wise i.e. the extent and what scope should it appear in.
026B-V001- Not currently in the scope. What's the requirement Insulation wise i.e. the extent and what scope should it appear in.
026B-V002- Not currently in the scope. What's the requirement Insulation wise i.e. the extent and what scope should it appear in.
020-V004- - Not currently in the scope. What's the requirement Insulation wise i.e. the extent. On Bravo
014-EPH001- Currently in the Bravo scope.
014-EPH002 - 1st Gas scope.
020-EPH002 - 1st Gas scope.
029-EPH001 - 1st Gas scope.
Please let me know as soon as possible so these can be added to the relevant scope.

Hi Mustafa, here is the first of seven up-dates as to the status of the ITC's. Three of the vessels are on the Bravo ...which might have saved me a couple of hours of searching in the freezing winds and rain if I had l known ...I don't like to complain though :). 
This is for CYG-HFG-L-00959
Let me know if you require any further information? 

Hi Russell,
I have added them to the following folder in the W Shared drive, let me know if you can access this?
W:\Cygnus Shared\Commissioning\HookUp\CarryOver\Painting and Insulation ITCs for Russell\APU

Morning Mustafa, the links don't appear to work at this end, could you try them again at your end and pass them on again please? 

Russell,
Please see below a list of ITC's that were issued to Hereema. 
These can be found in the below link supplied by Mustafa. Can you print these out and survey these and report the status. You'll need to speak to Ops about raising a permit to allow you to take photos, otherwise you can get a loan of the intrisicly safe camera from the safety guys. It's for joint use between all.
Give Mustafa a shout if you've any issues with the link.

Hi Tam,
Hope all is well offshore.
I was wondering if you can please provide me with an update on the following ITCs for APU and ACM:
APU:
1-     CYG-HFG-L-00523
2-     CYG-HFG-L-00542
3-     CYG-HFG-L-00959
ACM:
1-     CYG-HFG-L-00388
2-     CYG-HFG-L-00419
3-     CYG-HFG-L-00420
4-     CYG-HFG-L-00504
They can be found in the following folder within their platform subfolder:
\\gdfbrit.local\dfs$\Shared\Cygnus Shared\Construction\HAN\Carryover ITC's<file:///\\gdfbrit.local\dfs$\Shared\Cygnus%20Shared\Construction\HAN\Carryover%20ITC's>

