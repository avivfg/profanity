import enchant
import json
import string
import nltk
import random
from nltk.tokenize import regexp_tokenize
from nltk.corpus import stopwords, words
import re
from fuzzywuzzy import fuzz,process
import difflib
import time
import csv
import os
import sys
from collections import Counter


spell_checker = enchant.Dict("en_US")
the_ABC = 'abcdefghijklmnopqrstuvwxyz'



def create_bow(bow_path):
    file = open(bow_path,"r") # @aviv
    profane_phrases = file.read().replace('\r\n','\n').split('\n') # getting the BOW
    profanities_by_size = {}
    for i in range(len(profane_phrases)):
        p_phrase = profane_phrases[i]
        separated_p_i = regexp_tokenize(p_phrase, pattern='^\S+^|\S*[^. -\]\)\?,!~`$]')
        filtered_p_i = [w for w in separated_p_i if not w in stopwords.words('english')]   
        p_phrase_size = len(filtered_p_i)  
        if not p_phrase_size in profanities_by_size:
            profanities_by_size[p_phrase_size] = []
        profanities_by_size[p_phrase_size].append(filtered_p_i) 
        #profane_phrases[i] = filtered_p_i # concider not taking off all the stopwords
    max_len_p_phrase = max([key for key in profanities_by_size])
    return max_len_p_phrase, profanities_by_size

def extra_csvs(b_terms_total_occur, profane_amount_in_docs, b_terms_mem,output_dir):

    # an ranked (by amount of profanities) email bodies csv.
    output_file = open(output_dir+"/ranked_email_bodies.csv","w")
    csv_file = csv.writer(output_file)
    csv_file.writerow(["Doc ID","Occurrences"])
    for doc_name in sorted(profane_amount_in_docs, key=profane_amount_in_docs.get, reverse=True):
        body_value = profane_amount_in_docs[doc_name]
        if body_value == 0:
            break
        csv_file.writerow([doc_name,body_value])
    output_file.close()

    # an ranked (by amount of occurrences) profanities csv.
    output_file = open(output_dir+"/ranked_profanities.csv","w")
    csv_file = csv.writer(output_file)
    csv_file.writerow(["Profane word","Occurrences","Random Examples"])
    for b_term in sorted(b_terms_total_occur, key=b_terms_total_occur.get, reverse=True):
        absolute_profanity_value = b_terms_total_occur[b_term]
        if absolute_profanity_value == 0:
            break
        row_list = [b_term,absolute_profanity_value]
        for i in range(len(b_terms_mem[b_term])):
            row_list.append(b_terms_mem[b_term][i])
        csv_file.writerow(row_list)
    output_file.close()

def write_to_csv(docs_info_lst,output_dir):

    # a general csv with all the foundings
    output_file = open(output_dir+"/atomic_output.csv","w",newline='')
    csv_file = csv.writer(output_file,delimiter=',')
    csv_file.writerow(["Doc ID","Profane terms","Position (#line,start,end)", "Quote","Line"])
    b_terms_mem = {}
    b_terms_total_occur = {}
    profane_amount_in_docs = {}

    for doc in docs_info_lst:
        if not doc:
            continue
        csv_file.writerow([])
        doc_name = list(doc.values())[0][0][0]
        profane_amount_in_docs[doc_name] = 0 
        for b_term in doc:

            list_of_occurrences_in_doc = doc[b_term]
            # list of (current_doc_path,words_to_add,line_num,start,line_text)
            occurrences = len(list_of_occurrences_in_doc)
            
            if b_term not in b_terms_total_occur:
                b_terms_total_occur[b_term] = 0 
                b_terms_mem[b_term] = []
            b_terms_total_occur[b_term] += occurrences
            profane_amount_in_docs[doc_name] += occurrences

            for occurrence in list_of_occurrences_in_doc:
                quote = occurrence[1]
                line_num = occurrence[2]
                start = occurrence[3]
                line_text = occurrence[4]
                end = start + len(quote) - 1
                csv_file.writerow([doc_name,b_term,(line_num,start,end),quote,line_text])
                if line_text not in b_terms_mem[b_term]:
                    if len(b_terms_mem[b_term]) < 5:
                        b_terms_mem[b_term].append(line_text)
                    else:
                        probability = 0.054321
                        if random.random() < probability:
                            random_index = random.randrange(5)
                            b_terms_mem[random_index] = line_text

    output_file.close()
    # b_terms_total_occur has counted the occurences of all the profane words.
    # profane_amount_in_docs has counted the profanity amounts for each doc.
    # b_terms_mem has gethered 5 sentences (if exsist) for any profanity phrase.
    extra_csvs(b_terms_total_occur, profane_amount_in_docs,b_terms_mem,output_dir)


    docs_info_lst.clear()



def unobscured_sequence_match(phrase_words,closest_pword,current_mail_text,current_doc_path,docs_info_lst):
    # this is a check for an unobscured sequence of words:
    fuzz_ratio = fuzz.ratio(phrase_words, closest_pword)
    print("fuzz.ratio("+phrase_words+","+closest_pword+") = ", fuzz_ratio)
    if fuzz_ratio >= 70:
        if not word_diff(phrase_words,closest_pword):
            print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF word_diff")
            return
        # word =_diff is fine.
        if fuzz_ratio <= 90:
            if is_valid_english(phrase_words):
                print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF is_valid_english")
                return

            else:
                word_found(phrase_words, closest_pword,current_mail_text,current_doc_path,docs_info_lst)
                return                                  # this is for accepting a word that:
                                                        # (1) is between 70% and 90% similar to a profane word, (and)
                                                        # (2) the distance between them is not greater than 4, (and)
                                                        # (3) is not a valid English one.
                                                        # maybe later we would want to put them into more tests, to assure that they perform profanity.
        word_found(phrase_words, closest_pword,current_mail_text,current_doc_path,docs_info_lst)
        #  accepting words that scored a fuzz_ratio of 90% or above with their closest profane-word.
        return
    else:
        print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF fuzz.ratio")
    return


# NEEDS AT LEAST A TUNE OF THE DIFF. AND DIST.
def partially_obscured_sequence_match(phrase_words,closest_pword,letters_ratio,current_mail_text,current_doc_path,docs_info_lst):
    print(phrase_words,closest_pword,letters_ratio)
    fuzz_ratio = fuzz.ratio(phrase_words, closest_pword)
    print("fuzz.ratio("+phrase_words+","+closest_pword+") = ", fuzz_ratio)
    if fuzz_ratio > (69 * letters_ratio):
        if not word_diff(phrase_words,closest_pword):
            print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF word_diff")
            return
        if fuzz_ratio < (91 * letters_ratio):
            if is_valid_english(phrase_words):
                print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF is_valid_english")
                return
            else:
                word_found(phrase_words, closest_pword,current_mail_text,current_doc_path,docs_info_lst)
                return                                          # this is for accepting a word that:
                                                                # (1) is between 70% and 90% similar to a profane word, (and)
                                                                # (2) the distance between them is not greater than 4, (and)
                                                                # (3) is not a valid English one.
                                                             # maybe later we would want to put them into more tests, to assure that they perform profanity.
        else:
            word_found(phrase_words, closest_pword,current_mail_text,current_doc_path,docs_info_lst) #  accepting words that scored a fuzz_ratio of 90% or above with their closest profane-word.
            return 
    else:
        print("'",phrase_words, "' IS NOT A PROFANE PHRASE, BECAUSE OF fuzz.ratio")
    return



def individual_check(phrase_words,closest_pword,score,current_mail_text,current_doc_path,docs_info_lst):
    # checking if the given phrases are close enough to say that they are actually the same.
    # fisrt we want to see what is the condition of the text (how reveal is it)


    print("#######################\n THE PHRASE IS: '"+phrase_words+"'.")
    print("  THE CLOSEST BAD-PHRASE IS: '"+closest_pword+"'.")
    ABC_count = sum(phrase_words.count(letter) for letter in the_ABC)
    spaces_count = phrase_words.count(" ")
    print("The amount of real letters in the word '"+phrase_words+"' is - ",ABC_count,".")
    letters_ratio = ABC_count/(len(phrase_words)-spaces_count)
    print('This word have ',100*letters_ratio,'%','of actual ABC-letters')
    if not isinstance(phrase_words, str):
        return 
    if letters_ratio == 0:
        if len(phrase_words) < 3:
            return
        print('it is a total GRAWLIX!')
        print('add it.. no need for thinking')
        closest_pword = "*%@&^/*"
        word_found(phrase_words,closest_pword,current_mail_text,current_doc_path,docs_info_lst)
    elif letters_ratio == 1:
        print('no grawlix at all.')
        print("check it regularly")
        unobscured_sequence_match(phrase_words,closest_pword,current_mail_text,current_doc_path,docs_info_lst)

    else: 
        print("it is partially obscured. check the length and the revealed letters (OR add it?!)")
        #partially_obscured_sequence_match(phrase_words,closest_pword,letters_ratio,current_mail_text,current_doc_path,docs_info_lst)
    return 




def add_to_caught_words(b_term,words_to_add,line_text,line_num,start,current_doc_path,docs_info_lst):
    doc_id = -1 # we are always talking about the last one in the docs_info_lst, and that's the one we are working on
    if b_term not in docs_info_lst[doc_id]:
        docs_info_lst[doc_id][b_term] = []
    docs_info_lst[doc_id][b_term].append((current_doc_path,words_to_add,line_num,start,line_text))
    print("ADDED ",(current_doc_path,words_to_add,line_num,start),"from the sentence:",line_text,".\nto the key: ",b_term)


def get_text_position(text,current_mail_text):
    # COULD BE A PROBLEM IF WE THROW THE STOPWORDS AND THEY WERE WITHIN THIS TEXT.
    # MAYBE WE SHOULD GET THEM OUT OF THE current_mail_text TOO?
    flag = False
    str_borders = ' ,.()[]-+=_\n\t'
    for i, line_text in enumerate(current_mail_text.split('\n')): # add 1 to the enum to start from 1 and not from 0.
        start_at = line_text.find(text)
        if not start_at == -1:
            if (start_at == 0 and current_mail_text[len(text)] in str_borders) or (start_at == len(line_text)-len(text) and current_mail_text[start_at-1] in str_borders):
                flag = True
            if (current_mail_text[start_at-1] in str_borders) and (current_mail_text[start_at+len(text)] in str_borders):
                flag = True
        if flag == True:
            line = line_text
            line_num = i
            return line, line_num, start_at
    print("ADD AN EXCEPTION HERE!!!!!!!!!!!") # cannot find the text back in the current mail. check for lost stopwords
    return -1,-1,-1

def word_found(text, closest_phrase,current_mail_text,current_doc_path,docs_info_lst):
    line, line_num, start = get_text_position(text,current_mail_text)
    if (line, line_num, start) == (-1,-1,-1):
        return
    add_to_caught_words(closest_phrase,text,line,line_num,start,current_doc_path,docs_info_lst)


def sequence_check(words_sequence, prof_dict_by_size,max_words_in_phrase,current_mail_text,current_doc_path,docs_info_lst):
    max_available_words_lest = min(max_words_in_phrase, len(words_sequence))
    for phrase_size in range(max_available_words_lest): # checking phrases with an increasing amount of words.
        if phrase_size not in prof_dict_by_size: 
            continue # the wierdest scenario where there exist a profane phrase with M words, but there isn't one with some k<M words.
        phrase_words_to_check = " ".join(words_sequence[:phrase_size])

        p_phrases_to_check = []
        for fixed_size_phrase_lst in prof_dict_by_size[phrase_size]:
            fixed_size_phrase = ""
            for i in range(phrase_size):
                if not fixed_size_phrase == "":
                    fixed_size_phrase += " "
                fixed_size_phrase += fixed_size_phrase_lst[i]
            p_phrases_to_check.append(fixed_size_phrase)

        closest_pword, score = process.extractOne(phrase_words_to_check, p_phrases_to_check, scorer = fuzz.ratio)
        #we can change the metric by adding a scorer parameter: process.extractOne(word,profane_words,scorer=fuzz.some_optional_sort)
        individual_check(phrase_words_to_check,closest_pword,score,current_mail_text,current_doc_path,docs_info_lst)


def preprocess(email_body): # \S*[^. \-\]\)\?,!~`$]
    email_body = email_body.lower()
    tokens = regexp_tokenize(email_body, pattern='^\S+^|\S*[^. -\]\)\?,!~`$]')
    filtered_words = [w for w in tokens if not w in stopwords.words('english')]
    return filtered_words


def is_valid_english(phrase):
    for word in phrase.split(" "):
        if not is_english_word(word):
            return False
    return True

def is_english_word(word):
    # return word in words.words() # @aviv
    return spell_checker.check(word)

def word_diff(word,profane_word):
    diff = difflib.SequenceMatcher(None,word,profane_word)
    word_len = len(word)
    prof_len = len(profane_word)
    FIRST_CH_IDX = 0
    diff_size = diff.find_longest_match(FIRST_CH_IDX,word_len,FIRST_CH_IDX,prof_len).size
    # diff_size is the size of the biggest shared substring  (between word[1ST_CH_IDX:word_len] profane_word[1ST_CH_IDX:prof_len])
    max_len = max(word_len, prof_len)
    diff_of_words = abs(diff_size - max_len)
    print("diff_of_words(("+word+","+profane_word+")) = ", diff_of_words)
    if diff_of_words>4:
        return False
    return True

# not in use anymore
def get_input():
    if len(sys.argv) == 1:
        print("No document recieved.\n\tYou can also give filename as a command line argument")
        dir_name = input("Enter directory name: ")
    else:
        dir_name = sys.argv[1]
    return dir_name
    # input_file = open(file_name,"r")
    # return input_file.read()


def check_profanity_in_mail(mail_file_name,input_dir,prof_dict_by_size,max_words_in_phrase,docs_info_lst):
    # 'utf-8-sig', 'utf-16','utf-16-le','utf-16-be','utf-32','latin-1'
    with open(input_dir+'/'+mail_file_name,'r') as input_file: #, encoding = "utf-16"
        mail_text = input_file.read()
        mail_words_list = preprocess(mail_text)
    input_file.close()
    current_doc_path = mail_file_name
    current_mail_text = mail_text
    for word_idx in range(len(mail_words_list)):
        max_available = max(word_idx+max_words_in_phrase,len(mail_words_list)-1)
        sequence_to_check = mail_words_list[word_idx:max_available]
        if None in sequence_to_check:
            print("HELPPPPPPPPP")
        # sequence_to_check.append(mail_words_list[word_idx+i])
        # for example, if the sentence is "I am having so much fun", and word_idx=2, and there is no profane phrase with more than 2 words,
        # so the sequence is words 2-3, i.e. ["having","so"]
        sequence_check(sequence_to_check,prof_dict_by_size,max_words_in_phrase,current_mail_text,current_doc_path,docs_info_lst) 

    

def the_lookup(emails_paths, input_dir ,output_dir):

    # stats = open(output_dir + "/statistics.tsv", 'w',encoding=en)
    longest_phrase_length, prof_dict_by_size = create_bow("bad_words.txt")
    docs_info_lst = []
    for mail_path in emails_paths:
        docs_info_lst.append({})
        check_profanity_in_mail(mail_path,input_dir,prof_dict_by_size,longest_phrase_length,docs_info_lst)
    write_to_csv(docs_info_lst,output_dir)
    return



if __name__ == '__main__':
    data_dir = get_input()
    emails_files = [f for f in os.listdir(data_dir) if os.path.isfile(os.path.join(data_dir, f))]
    out_dir = os.path.join("outputs", time.strftime("%Y-%m-%d_%H-%M"))
    if not os.path.isdir(out_dir):
                os.makedirs(out_dir)
                os.makedirs(os.path.join(out_dir, "csv's"))
    the_lookup(emails_files,data_dir,out_dir)
