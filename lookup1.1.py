import enchant
import json
import string
import nltk
import random
from nltk.tokenize import regexp_tokenize
from nltk.corpus import stopwords, words
import re
from fuzzywuzzy import fuzz,process
import difflib
import time
import csv
import os
import sys
from collections import Counter

from collections import defaultdict
from os import listdir, path
from itertools import combinations, product
# from fuzzywuzzy import fuzz
#import spacy
#from gensim.summarization.textcleaner import get_sentences


#nlp = spacy.load('en_core_web_sm')
spell_checker = enchant.Dict("en_US")
suffix_punct = ",.?!:'\"\\)]}|`/-_"
prefix_punct = ",.?!:'\"\\([{|`/-_"


def create_bow(bow_path):
    file = open(bow_path,"r", encoding='utf-8') # @aviv
    profane_phrases = file.read().replace('\r\n','\n').split('\n') # getting the BOW
    #return profane_phrases

    # check only 1-word profane terms (for now)
    one_word_profs = list()
    for p_term in profane_phrases:
        if len(p_term.split()) == 1: 
            one_word_profs.append(p_term)
    return one_word_profs

def write_to_csv(docs_info_lst,output_dir):
    return


def contains_alpha(word):
    return any(char.isalpha() for char in word)

# reduce aplha letters repetitions to 2 max.
def reduce_repetitions(word):
    rep_counter = 0
    last_letter = ""
    clean_word = ""
    for letter in word:
        if letter.isalpha():
            if letter == last_letter:
                rep_counter += 1
            else:
                rep_counter = 0
            if rep_counter < 2:
                clean_word += letter
        else:
            clean_word += letter
        last_letter = letter
    return clean_word


def preprocess(data_dir): # \S*[^. \-\]\)\?,!~`$]
    index = defaultdict(list)
    docs = []
    counter = 0
    limit_amount_of_mails = 1000
    ignore_list = ["from uli","CMS 475 files only"] # "south_park_01.txt"

    for file in listdir(data_dir)[:limit_amount_of_mails]:

        if file in ignore_list:
            continue 

        with open(data_dir +'/'+ file, 'r', encoding='utf-16') as email_body:
            splitted_body = email_body.read().lower().split()
            words_amount = len(splitted_body)

            for word_idx in range(words_amount):
                if contains_alpha(splitted_body[word_idx]):
                    splitted_body[word_idx] = clean_word(splitted_body[word_idx])
                token = splitted_body[word_idx]
                index[token].append((counter, word_idx))
            docs.append(splitted_body)
            counter += 1

    return docs, index 


def clean_suffix(word):
    last_char = -1

    while word[last_char] in suffix_punct: # getting rid of suffix punctuations
        word = word[:last_char]
    return word


def clean_prefix(word):
    first_char = 0
    second_char = first_char + 1

    while word[first_char] in prefix_punct: # getting rid of prefix punctuations
        word = word[second_char:]
    return word

def clean_word(word):
    return reduce_repetitions(clean_prefix(clean_suffix(word)))

def is_valid_english(phrase):
    for word in phrase.split(" "):
        if not is_english_word(word):
            return False
    return True

def is_english_word(word):
    # return word in words.words() # @aviv
    return spell_checker.check(word)


# remains the same
def get_input():
    if len(sys.argv) == 1:
        print("No document recieved.\n\tYou can also give filename as a command line argument")
        dir_name = input("Enter directory name: ")
    else:
        dir_name = sys.argv[1]
    return dir_name

def symbol_to_char(symbol):
    if symbol == "@":
        return "a"
    elif symbol == "$":
        return "s"
    elif symbol == "#":
        return "h"
    elif symbol == "+":
        return "t`"
    elif symbol == "!":
        return "i" # or "l"
    elif symbol == "0":
        return "o"
    elif symbol == "1":
        return "l" # or "i"
    elif symbol == "2":
        return "z"
    elif symbol == "3":
        return "e"
    elif symbol == "5":
        return "s"
    elif symbol == "6" or symbol == "8":
        return "b"
    elif symbol == "7":
        return "t"
    elif symbol == "9":
        return "g"

def john_trans(word):
    converted = ''
    for char in word:
        if char.isalpha():
            converted += char
        else:
            conv_char = symbol_to_char(char)
            if conv_char:
                converted += conv_char
    return converted

# returns the word's case:
# 0 = totally-revealed
# 1 = partially-obscured
# 2 = totally-obscured
def check_word_case(word):
    if contains_alpha(word):
        if word.isalpha():
            return 0
        return 1
    return 2

def share_rev_letters(word1,word2):
    if len(word1) is not len(word1):
        return False
    for idx in range(len(word1)):
        if word1[idx].isalpha() and word2[idx].isalpha():
            if word1[idx] is not word2[idx]:
                return False

def similar_under_john_transformation(word1,word2):
    if not share_rev_letters(word1,word2):
        return 0
    ratio = fuzz.ratio(john_trans(word1),john_trans(word2))
    if ratio <= 75:
        ratio = 0
    return ratio



def check_revealed_words(word1,word2):
    ratio = 0
    if len(word1) == len(word2):
        ratio = fuzz.ratio(word1, word2)
        if ratio <= 75:
            ratio = 0
        else:
            if ratio < 100 and is_valid_english(word1):
                ratio = 0

    return ratio

# check similarity between 2 totally-obscured words
def check_obscured_words(word1,word2):
    ratio = 0
    if len(word1) == len(word2):
        ratio = fuzz.ratio(word1, word2)
        if ratio <= 75:
            ratio = 0
    # maybe we want to check soundex too (if the words differ by size, but not that much phonetically)
    return ratio

# check similarity between 2 partially-obscured words
def check_pobs_words(word1,word2):
    ratio = 0
    if len(word1) == len(word2):
        if similar_under_john_transformation(word1,word2):
            ratio = 101
    return ratio

# check similarity between 2 totally-obscured words
def check_tobs_words(word1,word2):
    ratio = 0
    if len(word1) == len(word2):
        ratio = 102
    return ratio

# check similarity between a revealed word and a totally-obscured word
def check_rev_and_tobs(word1,word2):
    ratio = 0
    return ratio

# check similarity between a partially-obscured word and a totally-obscured word
def check_pobs_and_tobs(word1,word2):
    ratio = 0
    return ratio



def check_similarity(word1,word2):
    # CHECK CASES::::::
    word1_case = check_word_case(word1) # 0 = totally-revealed
    word2_case = check_word_case(word2) # 1 = partially-obscured
                                        # 2 = totally-obscured
    # pick the correct check:
    if (word1_case == 0 and word2_case == 0):
        ratio = check_revealed_words(word1,word2) # V V
    elif (word1_case == 2 and word2_case == 0)  or (word1_case == 0 and word2_case == 2):
        ratio = check_rev_and_tobs(word1,word2)
    elif (word1_case == 1 and word2_case == 0)  or (word1_case == 0 and word2_case == 1) or (word1_case == 1 and word2_case == 1):
        ratio = check_pobs_words(word1,word2) 
    elif (word1_case == 2 and word2_case == 1)  or (word1_case == 1 and word2_case == 2):
        ratio = check_pobs_and_tobs(word1,word2)
    else: # == if (word1_case == 2 and word2_case == 2):
        ratio = check_tobs_words(word1,word2)
    return ratio

# in proccess
def check_similarities(words):
    similar_words = list()
    for word1, word2 in combinations(words, 2):
        ratio = check_similarity(word1,word2)
        if ratio:
            similar_words.append((word1, word2, ratio))
    return similar_words


def find_bad_words_in_index(index,bow):
    similar = list()
    for pair in product(index, bow):
        word = pair[0]
        p_term = pair[1].split()
        first_in_p = p_term[0]
        is_similar_lst = check_similarities([word,first_in_p])
        if len(is_similar_lst) > 0:
            similar.append((word, first_in_p,is_similar_lst[0][2]))
        if len(p_term) > 1:
            print("p_term is more than 1 word. what shall I do with it?!")
    return similar


def the_lookup(input_dir ,output_dir):

    bow = create_bow("bad_words.txt") # collecting only 1-word p_terms for now.    V
    print("bow = ",bow)
    print("\n\n")

    docs, index = preprocess(input_dir) # V V
    print("docs = ",docs)
    print("\n\n\n")
    print("index = ",index)

    words = list(index.keys())
    similarities = check_similarities(words) # V
    print("\n\n\n")
    print("similarities = ",similarities)
    
    similarities.sort(key=lambda x: x[2], reverse=True) # V V
    print("\n\n\n")
    print("sorted similarities = ",similarities)

    bad_words_in_index = find_bad_words_in_index(index,bow)
    print("\n\nbad_words_in_index = ",bad_words_in_index)

    bad_words_in_index.sort(key=lambda x: x[2], reverse=True)
    print("\n\n\n")
    print("sorted bad_words_in_index = ",bad_words_in_index)
    return


#checked
if __name__ == '__main__':
    input_dir = get_input()
    #emails_files = [f for f in listdir(input_dir) if path.isfile(path.join(input_dir, f))]
    out_dir = path.join("outputs", time.strftime("%Y-%m-%d_%H-%M"))
    if not path.isdir(out_dir):
                os.makedirs(out_dir)
                os.makedirs(os.path.join(out_dir, "csv's"))
    the_lookup(input_dir,out_dir)
